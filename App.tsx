/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * Generated with the TypeScript template
 * https://github.com/react-native-community/react-native-template-typescript
 *
 * @format
 */
import 'react-native-gesture-handler';
import React from 'react';
import { SafeAreaProvider } from 'react-native-safe-area-context';
import { Router } from './src/router';
import { AppContextProvider } from './src/context/app/app-context';
import { AppThemeProvider } from './src/theme/theme-provider';
import { ErrorHandlingProvider } from './src/context/error/error-handling-provider';
import { LoadingStateContextProvider } from './src/context/loading-state/loading-state-context';

const App = () => {
  return (
    <SafeAreaProvider>
      <LoadingStateContextProvider>
        <AppContextProvider>
          <AppThemeProvider>
            <ErrorHandlingProvider>
              <Router />
            </ErrorHandlingProvider>
          </AppThemeProvider>
        </AppContextProvider>
      </LoadingStateContextProvider>
    </SafeAreaProvider>
  );
};

export default App;
