import { validateAccessToken } from '../../src/services/bolt/bolt.service';
import RNSecureKeyStore from 'react-native-secure-key-store';

describe.skip('Bolt service', () => {
  let getSpy: any;
  let setSpy: any;

  beforeEach(() => {
    getSpy = jest
      .spyOn(RNSecureKeyStore, 'get')
      .mockResolvedValueOnce(
        JSON.stringify({
          created_at: 123,
          expires_in: 123,
          refresh_token: '123456',
        }),
      );
    setSpy = jest
      .spyOn(RNSecureKeyStore, 'set')
      .mockImplementation();
  });

  afterEach(() => {
    (getSpy as jest.Mock).mockReset();
    (setSpy as jest.Mock).mockReset();
  });

  describe('validateAccessToken given access token exists', () => {
    it('fetches new token and stores it', async () => {
      await validateAccessToken();
      expect(getSpy).toHaveBeenCalled();
      expect(setSpy).toHaveBeenCalledWith(
        'ACCESS_TOKEN',
        JSON.stringify({
          created_at: 123,
          expires_in: 123,
          refresh_token: '123456',
        }),
        { accessible: 'always' },
      );
    });
  });

  describe('validateAccessToken given access token does not exist', () => {
    it('skips token refresh', async () => {
      getSpy.mockReset();
      setSpy.mockReset();
      getSpy = jest
        .spyOn(RNSecureKeyStore, 'get')
        .mockResolvedValueOnce(undefined);
      setSpy = jest
        .spyOn(RNSecureKeyStore, 'set')
        .mockImplementation();
      await validateAccessToken();
      expect(getSpy).toHaveBeenCalled();
      expect(setSpy).not.toHaveBeenCalledWith();
    });
  });
});
