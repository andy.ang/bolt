import {
  determineBatteryLevel,
  truncateNumberToX,
  deriveImagePath,
  accessData,
  getPaddingFromHideGutter,
  getHoursFromMins,
} from '../../src/lib/util';

describe('truncateNumberToX', () => {
  it('should truncate number to precision', () => {
    const truncated = truncateNumberToX(123.12123124, 2);
    expect(truncated).toBe(123.12);
  });
});

describe('determineBatteryLevel', () => {
  it('should return correct value once', () => {
    const level = determineBatteryLevel(
      1.92165,
      54,
      13,
      80,
      191.41,
    );
    expect(level).toBe(76);
  });

  it('should return correct value twice', () => {
    const level = determineBatteryLevel(
      26.57,
      54,
      177,
      80,
      191.41,
    );
    expect(level).toBe(31);
  });

  it('should return correct value with battery degradation', () => {
    const level = determineBatteryLevel(
      26.57,
      54,
      177,
      80,
      191.41,
      0.1, // 10 percent degradation
    );
    expect(level).toBe(25);
  });
});

describe('deriveImageUrl', () => {
  it('returns url', () => {
    const optionCodes =
      'AD15,MDL3,PBSB,RENA,BT37,ID3W,RF3G,S3PB,DRLH,DV2W,W39B,APF0,COUS,BC3B,CH07,PC30,FC3P,FG31,GLFR,HL31,HM31,IL31,LTPB,MR31,FM3B,RS3H,SA3P,STCP,SC04,SU3C,T3CA,TW00,TM00,UT3P,WR00,AU3P,APH3,AF00,ZCST,MI00,CDM0';
    const url = deriveImagePath(optionCodes);
    expect(url).toBe('tm3_black');
  });
});

describe('accessData', () => {
  it('returns correct data based on accessor (1)', () => {
    const accessor = 'drive_state.latitude';
    const data = {
      drive_state: {
        latitude: 100,
      },
      other_state: {
        car_type: 'model3',
      },
    };
    const results = accessData(accessor, data);
    expect(results).toBe(100);
  });

  it('returns correct data based on accessor (2)', () => {
    const accessor = 'drive_state.coordinates.longitude';
    const data = {
      drive_state: {
        coordinates: {
          latitde: 200,
          longitude: 300,
        },
      },
      other_state: {
        car_type: 'model3',
      },
    };
    const results = accessData(accessor, data);
    expect(results).toBe(300);
  });

  it('returns undefined if data not found (1)', () => {
    const accessor = 'drive_state.coordinates.altitude';
    const data = {
      drive_state: {
        coordinates: {
          latitde: 200,
          longitude: 300,
        },
      },
      other_state: {
        car_type: 'model3',
      },
    };
    const results = accessData(accessor, data);
    expect(results).toBe(undefined);
  });

  it('returns undefined if data not found (2)', () => {
    const accessor = 'charge_state.coordinates.altitude';
    const data = {
      drive_state: {
        coordinates: {
          latitde: 200,
          longitude: 300,
        },
      },
      other_state: {
        car_type: 'model3',
      },
    };
    const results = accessData(accessor, data);
    expect(results).toBe(undefined);
  });
});

describe('getPaddingFromHideGutter', () => {
  it('return correct padding if hideGutter is true', () => {
    const result = getPaddingFromHideGutter(true, 'md');
    expect(result).toBe('0px 0px 0px 0px');
  });

  it('return correct padding if hideGutter is true', () => {
    const result = getPaddingFromHideGutter(false, 'md');
    expect(result).toBe('20px 20px 20px 20px');
  });

  it('return correct padding if hideGutter is horizontal', () => {
    const result = getPaddingFromHideGutter(
      'horizontal',
      'md',
    );
    expect(result).toBe('20px 0 20px 0');
  });

  it('return correct padding if hideGutter is vertical', () => {
    const result = getPaddingFromHideGutter(
      'vertical',
      'md',
    );
    expect(result).toBe('0 20px 0 20px');
  });

  it('return correct padding if hideGutter is left', () => {
    const result = getPaddingFromHideGutter('left', 'md');
    expect(result).toBe('20px 20px 20px 0');
  });

  it('return correct padding if hideGutter is right', () => {
    const result = getPaddingFromHideGutter('right', 'md');
    expect(result).toBe('20px 0 20px 20px');
  });

  it('return correct padding if hideGutter is top', () => {
    const result = getPaddingFromHideGutter('top', 'md');
    expect(result).toBe('0 20px 20px 20px');
  });

  it('return correct padding if hideGutter is bottom', () => {
    const result = getPaddingFromHideGutter('bottom', 'md');
    expect(result).toBe('20px 20px 0 20px');
  });

  it('return correct padding if hideGutter is top and left', () => {
    const result = getPaddingFromHideGutter(
      ['left', 'top'],
      'md',
    );
    expect(result).toBe('0px 20px 20px 0px');
  });

  it('return correct padding if hideGutter is vertical and left', () => {
    const result = getPaddingFromHideGutter(
      ['left', 'vertical'],
      'md',
    );
    expect(result).toBe('0px 20px 0px 0px');
  });

  it('return correct padding if hideGutter is true and left', () => {
    const result = getPaddingFromHideGutter(
      ['left', true],
      'md',
    );
    expect(result).toBe('0px 0px 0px 0px');
  });

  it('return correct padding if hideGutter is false and left', () => {
    const result = getPaddingFromHideGutter(
      ['left', false],
      'md',
    );
    expect(result).toBe('20px 20px 20px 0px');
  });

  it('return correct padding if hideGutter is top, righht, bottom and left', () => {
    const result = getPaddingFromHideGutter(
      ['left', 'right', 'top', 'bottom'],
      'md',
    );
    expect(result).toBe('0px 0px 0px 0px');
  });
});

describe('getHoursFromMins', () => {
  it('return 0 hours 15 mins if timeInMins is 15', () => {
    const time = getHoursFromMins(15);
    expect(time).toEqual([0, 15]);
  });

  it('return 0 hours 45 mins if timeInMins is 45', () => {
    const time = getHoursFromMins(45);
    expect(time).toEqual([0, 45]);
  });

  it('return 2 hours if timeInMins is 120', () => {
    const time = getHoursFromMins(120);
    expect(time).toEqual([2, 0]);
  });

  it('return 2 hours 10 mins if timeInMins is 130', () => {
    const time = getHoursFromMins(130);
    expect(time).toEqual([2, 10]);
  });

  it('return 2 hours 55 mina if timeInMins is 175', () => {
    const time = getHoursFromMins(175);
    expect(time).toEqual([2, 55]);
  });

  it('return 3 hours if timeInMins is 180', () => {
    const time = getHoursFromMins(180);
    expect(time).toEqual([3, 0]);
  });
});
