import React from 'react';
import renderer from 'react-test-renderer';
import { Router } from '../src/router';
import { AppThemeProvider } from '../src/theme/theme-provider';
import * as AppContext from '../src/context/app/app-context';

import 'jest-styled-components';
import { currentVehicle } from './mock-data/mock-data';
import { ControlPanelItems } from '../src/config/control-panel-list';

describe('Router', () => {
  it('renders home page correctly', () => {
    jest
      .spyOn(AppContext, 'useAppContext')
      .mockReturnValueOnce({
        authentication: 'authenticated',
        currentVehicle,
        vehicles: [currentVehicle],
        appStatus: 'active',
        controlPanelItems: ControlPanelItems,
        remoteConfig: { experiment: false },
        gradient: undefined,
        mode: 'dark',
        location: { latitude: 0, longitude: 0 },
        setColorMode: jest.fn(),
        setControlPanelItems: jest.fn(),
        setCurrentVehicle: jest.fn(),
        setGradient: jest.fn(),
        setVehicles: jest.fn(),
      });

    const rendered = renderer.create(
      <AppThemeProvider>
        <Router />
      </AppThemeProvider>,
    );
    expect(rendered).toMatchSnapshot();
  });
});
