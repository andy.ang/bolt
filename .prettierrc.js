module.exports = {
  bracketSpacing: true,
  jsxBracketSameLine: true,
  singleQuote: true,
  doubleQuotes: false,
  trailingComma: 'all',
  printWidth: 60
};
