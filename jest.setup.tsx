import React from 'react';
import 'react-native-gesture-handler/jestSetup';
//@ts-ignore
import mockAsyncStorage from '@react-native-async-storage/async-storage/jest/async-storage-mock';

jest.useFakeTimers();

jest.mock('react-native-reanimated', () => {
  const Reanimated = require('react-native-reanimated/mock');

  // The mock for `call` immediately calls the callback which is incorrect
  // So we override it with a no-op
  Reanimated.default.call = () => {};

  return Reanimated;
});

// Silence the warning: Animated: `useNativeDriver` is not supported because the native animated module is missing
jest.mock(
  'react-native/Libraries/Animated/src/NativeAnimatedHelper',
);

jest.mock(
  '@react-native-async-storage/async-storage',
  () => mockAsyncStorage,
);
jest.mock(
  'react-native/Libraries/EventEmitter/NativeEventEmitter',
);

jest.mock('@react-native-community/geolocation', () => ({
  addListener: jest.fn(),
  getCurrentPosition: jest.fn(),
  removeListeners: jest.fn(),
  requestAuthorization: jest.fn(),
  setConfiguration: jest.fn(),
  startObserving: jest.fn(),
  stopObserving: jest.fn(),
}));

jest.mock('react-native-maps', () => {
  const { View } = require('react-native');
  const MockView = (props: any) => {
    return <View>{props.children}</View>;
  };
  return {
    __esModule: true,
    default: MockView,
    Marker: MockView,
    Callout: MockView,
  };
});

jest.mock('react-native-secure-key-store', () => {
  const mockStorage: Record<string, string> = {};
  return {
    get: (key: string) => mockStorage[key],
    set: (key: string, value: string) => {
      mockStorage[key] = value;
    },
    ACCESSIBLE: {
      ALWAYS: 'always',
    },
  };
});

jest.mock('react-native-haptic-feedback');
jest.mock(
  '@react-native-community/segmented-control',
  () => {
    return {
      default: <></>,
    };
  },
);
