// import original module declarations
import 'styled-components';
import { ITheme } from './src/typings/theme.types';

// and extend them!
declare module 'styled-components' {
  export interface DefaultTheme extends ITheme {}
}
