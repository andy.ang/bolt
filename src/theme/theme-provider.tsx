import React from 'react';
import { ThemeProvider } from 'styled-components/native';
import { useAppContext } from '../context/app/app-context';
import { Dark, Light } from './themes';

interface IAppThemeProvider {
  children: React.ReactNode;
}

export const AppThemeProvider = ({
  children,
}: IAppThemeProvider) => {
  const { mode } = useAppContext();
  const appTheme = mode === 'dark' ? Dark : Light;

  return (
    <ThemeProvider theme={appTheme}>
      {children}
    </ThemeProvider>
  );
};
