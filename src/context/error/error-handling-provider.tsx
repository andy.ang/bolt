import React, { useEffect, useState } from 'react';
import { SimpleModal } from '../../components';
import { EventManager } from '../../lib/event';
import {
  ErrorTitle,
  IError,
} from '../../typings/error.types';
import { Events } from '../../typings/event.types';

interface IErrorHandlingProviderProps {
  children: React.ReactNode;
}

export const ErrorHandlingProvider = (
  props: IErrorHandlingProviderProps,
) => {
  const { children } = props;
  const [error, setError] = useState<IError>();
  const {
    title = ErrorTitle.GENERIC,
    body,
    footer,
    buttonLabel = 'OK',
    buttonOnPress = () => setError(undefined),
  } = error || {};

  useEffect(() => {
    const listener = EventManager.listen(
      Events.ERROR,
      (event) => {
        setError(event);
      },
    );
    return () => {
      EventManager.remove(listener);
    };
  }, []);

  return (
    <>
      {children}
      <SimpleModal
        visible={!!error}
        title={title}
        body={body}
        footer={footer}
        buttonLabel={buttonLabel}
        buttonOnPress={buttonOnPress}
      />
    </>
  );
};
