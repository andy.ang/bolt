import { LatLng } from 'react-native-maps';
import { AuthenticationStatus } from '../../typings/auth.types';
import { IControlPanelItem } from '../../typings/vehicle-command.types';
import { Gradient } from '../../typings/theme.types';
import { IVehicleData } from '../../typings/vehicle-data.types';
import { AppStateStatus } from 'react-native';

export interface IRemoteConfig {
  experiment: boolean;
}

export type ColorMode = 'light' | 'dark' | undefined;

export interface IAppContext {
  appStatus: AppStateStatus;
  authentication: AuthenticationStatus;
  controlPanelItems: IControlPanelItem[];
  currentVehicle: IVehicleData | null;
  gradient: Gradient | undefined;
  location: LatLng | undefined;
  mode: ColorMode;
  remoteConfig: IRemoteConfig;
  setColorMode: (data?: ColorMode) => void;
  setGradient: (gd?: Gradient) => void;
  setVehicles: (vehicles: IVehicleData[]) => void;
  setCurrentVehicle: (vehicleId: IVehicleData) => void;
  setControlPanelItems: (
    controls: IControlPanelItem[],
  ) => void;
  vehicles: IVehicleData[];
}

export enum ActionTypes {
  SET_APP_STATUS = 'setAppStatus',
  SET_AUTHENTICATION = 'setAuthentication',
  SET_COLOR_MODE = 'setColorMode',
  SET_CONTROL_PANEL_ITEMS = 'setControlPanelItems',
  SET_CURRENT_VEHICLE = 'setCurrentVehicle',
  SET_GRADIENT = 'setGradient',
  SET_LOCATION = 'setLocation',
  SET_VEHICLES = 'setVehicles',
  STATE = 'state',
  UPDATE_REMOTE_CONFIG = 'updateRemoteConfig',
}

export interface ISetAuthenticated {
  type: ActionTypes.SET_AUTHENTICATION;
  payload: AuthenticationStatus;
}

export interface ISetLocation {
  type: ActionTypes.SET_LOCATION;
  payload: LatLng;
}

export interface ISetColorMode {
  type: ActionTypes.SET_COLOR_MODE;
  payload: {
    mode: ColorMode;
  };
}

export interface ISetControlPanelItems {
  type: ActionTypes.SET_CONTROL_PANEL_ITEMS;
  payload: IControlPanelItem[];
}

export interface IUpdateRemoteConfig {
  type: ActionTypes.UPDATE_REMOTE_CONFIG;
  payload: IRemoteConfig;
}

export interface ISetGradient {
  type: ActionTypes.SET_GRADIENT;
  payload?: Gradient;
}

export interface IState {
  type: ActionTypes.STATE;
  payload?: Partial<IAppContext>;
}

export interface ISetVehicles {
  type: ActionTypes.SET_VEHICLES;
  payload: IVehicleData[];
}

export interface ISetCurrentVehicle {
  type: ActionTypes.SET_CURRENT_VEHICLE;
  payload: IVehicleData;
}

export interface ISetAppStatus {
  type: ActionTypes.SET_APP_STATUS;
  payload: AppStateStatus;
}

export type Actions =
  | ISetAppStatus
  | ISetAuthenticated
  | ISetColorMode
  | ISetControlPanelItems
  | ISetCurrentVehicle
  | ISetGradient
  | ISetLocation
  | ISetVehicles
  | IState
  | IUpdateRemoteConfig;
