import React, {
  createContext,
  useContext,
  useEffect,
  useReducer,
  useRef,
} from 'react';
import {
  Appearance,
  AppState,
  AppStateStatus,
} from 'react-native';
import Geolocation from '@react-native-community/geolocation';
import { getMultipleData } from '../../lib/storage';
import { fetchRemoteConfig } from '../../services/remote-config/remote-config.service';
import { Gradient } from '../../typings/theme.types';
import { eventDispatcher } from './app-action';
import { reducer } from './app-reducer';
import {
  IAppContext,
  IRemoteConfig,
} from './app-context.types';
import { StorageKeys } from '../../typings/storage.types';
import { validateAccessToken } from '../../services/bolt/bolt.service';
import { useAuthentiation } from '../../hooks/auth';
import { IVehicleData } from '../../typings/vehicle-data.types';
import { ControlPanelItems } from '../../config/control-panel-list';
import { useVehicles } from '../../hooks/vehicle';

interface IAppContextProvider {
  children: React.ReactNode;
}
const initialVehicle: IVehicleData = {
  display_name: 'Your tesla',
  id: 0,
  vehicle_id: 0,
  vin: '',
  option_codes: '',
  color: '',
  access_type: 'OWNER',
  tokens: [],
  state: 'online',
  in_service: false,
  id_s: '',
  calendar_enabled: false,
  api_version: 1,
  backseat_token: null,
  backseat_token_updated_at: null,
};

const initialConfig: IRemoteConfig = {
  experiment: false,
};

const initialState: IAppContext = {
  appStatus: 'active',
  mode: 'light',
  authentication: 'initializing',
  controlPanelItems: ControlPanelItems,
  currentVehicle: initialVehicle,
  vehicles: [],
  gradient: Gradient.DEFAULT,
  remoteConfig: initialConfig,
  location: undefined,
  setColorMode: () => {},
  setGradient: () => {},
  setVehicles: () => {},
  setCurrentVehicle: () => {},
  setControlPanelItems: () => {},
};

export const AppContext = createContext<IAppContext>(
  initialState,
);

export const useAppContext = (): IAppContext =>
  useContext(AppContext);

export const AppContextProvider = ({
  children,
}: IAppContextProvider): JSX.Element => {
  const [state, dispatch] = useReducer(
    reducer,
    initialState,
  );
  const appState = useRef(AppState.currentState);
  const dispatcher = eventDispatcher(dispatch);
  const [authentication] = useAuthentiation();
  const [vehicles, selectedVehicle] = useVehicles();

  const _handleAppStateChange = (
    nextAppState: AppStateStatus,
  ) => {
    if (
      appState.current.match(/inactive|background/) &&
      nextAppState === 'active'
    ) {
      console.log('App has come to the foreground!');
    }
    appState.current = nextAppState;
    dispatcher.setAppStatus(appState.current);
  };

  useEffect(() => {
    getMultipleData([
      StorageKeys.REMOTE_CONFIG,
      StorageKeys.GRADIENT,
      StorageKeys.MODE,
      StorageKeys.LOCATION,
      StorageKeys.VEHICLES,
      StorageKeys.CURRENT_VEHICLE,
      StorageKeys.CONTROL_PANEL_LAYOUT,
    ]).then((data) => {
      const [
        config,
        grad,
        color,
        location,
        vehiclesData = [],
        currentVehicle,
        controlPanelItems = ControlPanelItems,
      ] = data || [];
      const systemColor = Appearance.getColorScheme();
      dispatcher.setState({
        remoteConfig: config,
        gradient: grad,
        mode: color ? color : systemColor,
        location: location ? location : undefined,
        vehicles: vehiclesData,
        controlPanelItems,
        currentVehicle: currentVehicle
          ? currentVehicle
          : vehiclesData && vehiclesData.length > 0
          ? vehiclesData[0]
          : null,
      });
    });
  }, []);

  useEffect(() => {
    fetchRemoteConfig(initialConfig).then(
      (fetchedConfig) => {
        dispatcher.updateRemoteConfig(fetchedConfig);
      },
    );
  }, []);

  useEffect(() => {
    validateAccessToken().catch(() => {});
  }, []);

  useEffect(() => {
    Geolocation.getCurrentPosition(
      ({ coords: { latitude, longitude } }) => {
        dispatcher.setLocation({
          latitude,
          longitude,
        });
      },
    );
  }, []);

  useEffect(() => {
    AppState.addEventListener(
      'change',
      _handleAppStateChange,
    );

    return () => {
      AppState.removeEventListener(
        'change',
        _handleAppStateChange,
      );
    };
  }, []);

  useEffect(() => {
    if (
      selectedVehicle &&
      vehicles &&
      vehicles.length > 0
    ) {
      dispatcher.setVehicles(vehicles);
      dispatcher.setCurrentVehicle(selectedVehicle);
    }
  }, [vehicles, selectedVehicle]);

  const context: IAppContext = {
    ...state,
    authentication,
    setColorMode: (colorMode) =>
      dispatcher.setColorMode({ mode: colorMode }),
    setGradient: (gd) => dispatcher.setGradient(gd),
    setVehicles: (vs) => dispatcher.setVehicles(vs),
    setCurrentVehicle: (vehicle) =>
      dispatcher.setCurrentVehicle(vehicle),
    setControlPanelItems: (controls) =>
      dispatcher.setControlPanelItems(controls),
  };

  return (
    <AppContext.Provider value={context}>
      {children}
    </AppContext.Provider>
  );
};
