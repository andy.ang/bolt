import {
  ActionTypes,
  Actions,
  IAppContext,
} from './app-context.types';

export type Reducer = (
  state: IAppContext,
  action: Actions,
) => IAppContext;
export const reducer: Reducer = (
  state,
  action,
): IAppContext => {
  switch (action.type) {
    case ActionTypes.SET_COLOR_MODE:
      return {
        ...state,
        ...action.payload,
      };
    case ActionTypes.UPDATE_REMOTE_CONFIG:
      return {
        ...state,
        ...action.payload,
      };
    case ActionTypes.SET_GRADIENT:
      return {
        ...state,
        gradient: action.payload,
      };
    case ActionTypes.STATE:
      return {
        ...state,
        ...action.payload,
      };
    case ActionTypes.SET_LOCATION:
      return {
        ...state,
        location: action.payload,
      };
    case ActionTypes.SET_AUTHENTICATION:
      return {
        ...state,
        authentication: action.payload,
      };
    case ActionTypes.SET_VEHICLES:
      return {
        ...state,
        vehicles: action.payload,
      };
    case ActionTypes.SET_CURRENT_VEHICLE:
      return {
        ...state,
        currentVehicle: action.payload,
      };
    case ActionTypes.SET_APP_STATUS:
      return {
        ...state,
        appStatus: action.payload,
      };
    default:
      return {
        ...state,
      };
  }
};
