import { storeData } from '../../lib/storage';
import { StorageKeys } from '../../typings/storage.types';
import {
  ActionTypes,
  ISetColorMode,
  IUpdateRemoteConfig,
  ISetGradient,
  Actions,
  IState,
  ISetLocation,
  ISetAuthenticated,
  ISetVehicles,
  ISetCurrentVehicle,
  ISetControlPanelItems,
  ISetAppStatus,
} from './app-context.types';

const updateRemoteConfig = (
  dispatch: React.Dispatch<Actions>,
) => (data: IUpdateRemoteConfig['payload']) => {
  storeData(StorageKeys.REMOTE_CONFIG, data);
  dispatch({
    type: ActionTypes.UPDATE_REMOTE_CONFIG,
    payload: data,
  });
};

const setColorMode = (
  dispatch: React.Dispatch<Actions>,
) => (data: ISetColorMode['payload']) => {
  storeData(StorageKeys.MODE, data.mode);
  dispatch({
    type: ActionTypes.SET_COLOR_MODE,
    payload: data,
  });
};

const setGradient = (dispatch: React.Dispatch<Actions>) => (
  data: ISetGradient['payload'],
) => {
  storeData(StorageKeys.GRADIENT, data);
  dispatch({
    type: ActionTypes.SET_GRADIENT,
    payload: data,
  });
};

const setState = (dispatch: React.Dispatch<Actions>) => (
  data: IState['payload'],
) => {
  dispatch({
    type: ActionTypes.STATE,
    payload: data,
  });
};

const setLocation = (dispatch: React.Dispatch<Actions>) => (
  data: ISetLocation['payload'],
) => {
  storeData(StorageKeys.LOCATION, data);
  dispatch({
    type: ActionTypes.SET_LOCATION,
    payload: data,
  });
};

const setAuthnetication = (
  dispatch: React.Dispatch<Actions>,
) => (data: ISetAuthenticated['payload']) => {
  dispatch({
    type: ActionTypes.SET_AUTHENTICATION,
    payload: data,
  });
};

const setVehicles = (dispatch: React.Dispatch<Actions>) => (
  data: ISetVehicles['payload'],
) => {
  storeData(StorageKeys.VEHICLES, data);
  dispatch({
    type: ActionTypes.SET_VEHICLES,
    payload: data,
  });
};

const setCurrentVehicle = (
  dispatch: React.Dispatch<Actions>,
) => (data: ISetCurrentVehicle['payload']) => {
  storeData(StorageKeys.CURRENT_VEHICLE, data);
  dispatch({
    type: ActionTypes.SET_CURRENT_VEHICLE,
    payload: data,
  });
};

const setControlPanelItems = (
  dispatch: React.Dispatch<Actions>,
) => (data: ISetControlPanelItems['payload']) => {
  storeData(StorageKeys.CONTROL_PANEL_LAYOUT, data);
  dispatch({
    type: ActionTypes.SET_CONTROL_PANEL_ITEMS,
    payload: data,
  });
};

const setAppStatus = (
  dispatch: React.Dispatch<Actions>,
) => (data: ISetAppStatus['payload']) => {
  dispatch({
    type: ActionTypes.SET_APP_STATUS,
    payload: data,
  });
};

export const eventDispatcher = (
  dispatch: React.Dispatch<Actions>,
) => ({
  updateRemoteConfig: updateRemoteConfig(dispatch),
  setColorMode: setColorMode(dispatch),
  setGradient: setGradient(dispatch),
  setState: setState(dispatch),
  setLocation: setLocation(dispatch),
  setAuthneticated: setAuthnetication(dispatch),
  setVehicles: setVehicles(dispatch),
  setCurrentVehicle: setCurrentVehicle(dispatch),
  setControlPanelItems: setControlPanelItems(dispatch),
  setAppStatus: setAppStatus(dispatch),
});
