import React, {
  createContext,
  useContext,
  useReducer,
} from 'react';
import { loadingStateEventDispatcher } from './loading-state-actions';
import { ILoadingStateContext } from './loading-state-context.types';
import { reducer } from './loading-state-reducer';

interface ILoadingStateProvider {
  children: React.ReactNode;
}

const initialState: ILoadingStateContext = {
  DELETE_SCHEDULED_CHARGING: false,
  FLASH_LIGHTS: false,
  HONK_HORN: false,
  LOCK: false,
  MAX_CHARGING: false,
  OPEN_FRUNK: false,
  OPEN_TRUNK: false,
  PLAY_NEXT_TRACK: false,
  PLAY_PREVIOUS_TRACK: false,
  REMOTE_SEAT_HEATER_REQUEST: false,
  RESET_SPEED_LIMIT_PIN: false,
  RESET_VALET_PIN: false,
  SCHEDULE_CHARGING: false,
  SET_CHARGE_LIMIT: false,
  SET_SENTRY_MODE: false,
  SET_SPEED_LIMIT: false,
  SET_SPEED_LIMIT_MODE: false,
  SET_TEMPS: false,
  SET_VALET_MODE: false,
  STANDARD_CHARGING: false,
  START_AC: false,
  START_CHARGING: false,
  START_REMOTE_DRIVE: false,
  STOP_AC: false,
  STOP_CHARGING: false,
  TOGGLE_MAX_DEFROST: false,
  TOGGLE_MEDIA_PLAYBACK: false,
  TOGGLE_SUNROOF: false,
  TOGGLE_WINDOW: false,
  TRIGGER_HOME_LINK: false,
  TURN_VOLUME_DOWN: false,
  TURN_VOLUME_UP: false,
  UNLOCK: false,
  WAKE: false,
  setStateToInitiateLoading: () => {},
  setStateToCompleteLoading: () => {},
  resetLoadingState: () => {},
};

export const LoadingStateContext = createContext(
  initialState,
);

export const useLoadingStateContext = (): ILoadingStateContext =>
  useContext(LoadingStateContext);

export const LoadingStateContextProvider = ({
  children,
}: ILoadingStateProvider) => {
  const [state, dispatch] = useReducer(
    reducer,
    initialState,
  );
  const dispatcher = loadingStateEventDispatcher(
    state,
    dispatch,
  );

  const context: ILoadingStateContext = {
    ...state,
    setStateToInitiateLoading:
      dispatcher.setStateToInitiateLoading,
    setStateToCompleteLoading:
      dispatcher.setStateToCompleteLoading,
    resetLoadingState: dispatcher.resetLoadingState,
  };

  return (
    <LoadingStateContext.Provider value={context}>
      {children}
    </LoadingStateContext.Provider>
  );
};
