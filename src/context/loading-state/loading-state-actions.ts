import { Control } from '../../typings/vehicle-command.types';
import {
  ILoadingStateContext,
  LoadingStateActions,
} from './loading-state-context.types';

const setStateToInitiateLoading = (
  state: ILoadingStateContext,
  dispatch: React.Dispatch<LoadingStateActions>,
) => (data: Control) => {
  dispatch({
    type: data,
    payload: true,
  });
};

const setStateToCompleteLoading = (
  state: ILoadingStateContext,
  dispatch: React.Dispatch<LoadingStateActions>,
) => (data: Control) => {
  dispatch({
    type: data,
    payload: false,
  });
};

const resetLoadingState = (
  state: ILoadingStateContext,
  dispatch: React.Dispatch<LoadingStateActions>,
) => () => {
  Object.keys(state).forEach((loadingState) => {
    if (state[loadingState as Control]) {
      dispatch({
        type: loadingState as Control,
        payload: false,
      });
    }
  });
};

export const loadingStateEventDispatcher = (
  state: ILoadingStateContext,
  dispatch: React.Dispatch<LoadingStateActions>,
) => ({
  setStateToInitiateLoading: setStateToInitiateLoading(
    state,
    dispatch,
  ),
  setStateToCompleteLoading: setStateToCompleteLoading(
    state,
    dispatch,
  ),
  resetLoadingState: resetLoadingState(state, dispatch),
});
