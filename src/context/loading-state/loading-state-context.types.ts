import { Control } from '../../typings/vehicle-command.types';

type PartialContext = {
  [key in Control]: boolean;
};

export interface ILoadingStateContext
  extends PartialContext {
  setStateToInitiateLoading: (state: Control) => void;
  setStateToCompleteLoading: (state: Control) => void;
  resetLoadingState: () => void;
}

export type LoadingStateActionTypes = Control;

export interface LoadingStateActions {
  type: LoadingStateActionTypes;
  payload: boolean;
}
