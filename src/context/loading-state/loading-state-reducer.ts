import {
  ILoadingStateContext,
  LoadingStateActions,
} from './loading-state-context.types';

export type LoadingStateReducer = (
  state: ILoadingStateContext,
  action: LoadingStateActions,
) => ILoadingStateContext;
export const reducer: LoadingStateReducer = (
  state,
  action,
): ILoadingStateContext => {
  return {
    ...state,
    [action.type]: action.payload,
  };
};
