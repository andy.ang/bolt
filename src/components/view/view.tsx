import React from 'react';
import {
  Keyboard,
  KeyboardAvoidingView,
  ScrollView,
  StatusBar,
} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import { useSafeAreaInsets } from 'react-native-safe-area-context';
import { useTheme } from 'styled-components/native';
import { BOTTOM_BAR } from '../../assets/images';
import { ColorMode } from '../../context/app/app-context.types';
import { Gradient } from '../../typings/theme.types';
import { Placeholder } from '../placeholder/placeholder';
import {
  StandardView,
  DismissableView,
  BottomTabMask,
  BottomTabMaskContainer,
} from './view.styles';

export interface IViewProps {
  alignX?: 'left' | 'center' | 'right';
  children: React.ReactNode;
  avoidKeyboard?: boolean;
  testID: string;
  mode?: ColorMode;
  type?: 'scroll' | 'normal';
  gradient?: boolean;
  gradientStyle?: Gradient;
  gradientStopPoint?: number;
  topInset?: boolean;
  bottomInset?: boolean;
  shadow?: boolean;
  scrollEnabled?: boolean;
  stickyIndex?: number[];
}

export const View = ({
  avoidKeyboard = true,
  children,
  testID,
  mode = 'light',
  type = 'normal',
  gradient,
  gradientStyle = Gradient.DEFAULT,
  gradientStopPoint = 0.5,
  alignX = 'center',
  topInset = true,
  bottomInset = false,
  shadow = false,
  scrollEnabled = true,
  stickyIndex,
}: IViewProps) => {
  const statusBarStyle =
    mode === 'dark' ? 'light-content' : 'dark-content';
  const theme = useTheme();
  const { primaryBackground } = theme;
  const { top, bottom } = useSafeAreaInsets();
  const [start, end] = gradient
    ? theme[gradientStyle]
    : [primaryBackground, primaryBackground];
  const horizontalAlign =
    alignX === 'left'
      ? 'flex-start'
      : alignX === 'right'
      ? 'flex-end'
      : 'center';

  return (
    <LinearGradient
      style={{
        flex: 1,
        width: '100%',
      }}
      colors={[start, end]}
      locations={[gradientStopPoint, 1]}>
      <KeyboardAvoidingView
        style={{ flex: 1 }}
        enabled={avoidKeyboard}
        behavior={'padding'}>
        <StatusBar
          barStyle={statusBarStyle}
          translucent
          backgroundColor="transparent"
        />
        {type === 'scroll' && (
          <ScrollView
            testID={testID}
            style={{
              flex: 1,
              width: '100%',
              marginBottom: -2 * bottom,
            }}
            stickyHeaderIndices={stickyIndex}
            scrollEnabled={scrollEnabled}
            showsVerticalScrollIndicator={false}
            keyboardShouldPersistTaps="handled"
            contentContainerStyle={{
              marginTop: topInset ? top : 0,
              alignItems: horizontalAlign,
            }}>
            {children}
            <Placeholder height={200} />
          </ScrollView>
        )}
        {type === 'normal' && (
          <DismissableView
            onPress={() => Keyboard.dismiss()}>
            <StandardView
              style={{
                paddingTop: topInset ? top : 0,
                paddingBottom: bottomInset ? bottom : 0,
              }}>
              {children}
            </StandardView>
          </DismissableView>
        )}
      </KeyboardAvoidingView>
      {shadow && (
        <BottomTabMaskContainer pointerEvents="box-only">
          <BottomTabMask source={BOTTOM_BAR} />
        </BottomTabMaskContainer>
      )}
    </LinearGradient>
  );
};
