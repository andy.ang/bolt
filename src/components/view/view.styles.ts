import styled from 'styled-components/native';

export const StandardView = styled.View`
  height: 100%;
  width: 100%;
`;

export const DismissableView = styled.TouchableWithoutFeedback`
  height: 100%;
  width: 100%;
`;

export const BottomTabMask = styled.Image`
  position: absolute;
  bottom: 0;
  opacity: 0.6;
`;

export const BottomTabMaskContainer = styled.View``;
