import React from 'react';
import { View } from 'react-native';

interface IPlaceholder {
  height: number;
  width?: number;
  color?: string;
}

export const Placeholder = (props: IPlaceholder) => {
  const { height, color = 'transparent', width } = props;

  return (
    <View
      style={{
        height,
        width: width ? width : '100%',
        backgroundColor: color,
      }}
    />
  );
};
