import React from 'react';
import { Dimensions } from 'react-native';
import { useTheme } from 'styled-components/native';
import {
  VictoryArea,
  VictoryAxis,
  VictoryChart,
  VictoryLabel,
} from 'victory-native';
import { convertDateToHumanReadable } from '../../lib/util';
import { IUserVehicleData } from '../../typings/vehicle-data.types';
import {
  AreaStyle,
  ChartContainer,
  ChartStyle,
  //   DayAxisStyle,
  LabelStyle,
  XAxisStyle,
  YAxisStyle,
} from './chart.styles';

interface IChart {
  data: IUserVehicleData[];
  x: keyof IUserVehicleData;
  y: keyof IUserVehicleData;
  yRange?: [number, number] | [Date, Date];
  xRange?: [number, number] | [Date, Date];
  xLabel: string;
  yLabel: string;
  title: string;
}

interface IPoint {
  _x: number;
  _y: number;
}

const { width } = Dimensions.get('screen');

export const Chart = (props: IChart) => {
  const {
    data,
    x,
    y,
    xRange,
    yRange,
    xLabel,
    yLabel,
    title,
  } = props;
  const theme = useTheme();
  const domain =
    xRange && yRange
      ? { x: xRange, y: yRange }
      : xRange
      ? { x: xRange }
      : { y: yRange };

  return (
    <ChartContainer>
      <VictoryChart
        height={250}
        width={width - 40}
        style={ChartStyle(theme)}>
        <VictoryLabel
          text={title}
          x={(width * 0.9) / 2}
          y={20}
          textAnchor="middle"
          style={LabelStyle(theme)}
        />
        <VictoryArea
          data={data}
          interpolation="basis"
          x={x}
          y={y}
          //@ts-ignore
          domain={domain}
          animate={{ duration: 1000 }}
          style={AreaStyle(theme)}
        />
        <VictoryAxis
          style={XAxisStyle(theme)}
          label={xLabel}
          fixLabelOverlap={true}
          tickFormat={(t) => {
            return convertDateToHumanReadable(t, 'HH:mm');
          }}
        />
        <VictoryAxis
          dependentAxis
          label={yLabel}
          style={YAxisStyle(theme)}
        />
      </VictoryChart>
    </ChartContainer>
  );
};
