import styled, {
  DefaultTheme,
} from 'styled-components/native';

export const ChartContainer = styled.View`
  height: 250px;
  justify-content: center;
  align-items: center;
  background-color: transparent;
  width: 100%;
`;

export const ChartStyle = ({
  chartBackground,
}: DefaultTheme) => ({
  parent: {
    borderRadius: 5,
    backgroundColor: chartBackground,
  },
  background: {
    fill: 'transparent',
  },
});

export const DayAxisStyle = ({
  primaryTextColor,
}: DefaultTheme) => ({
  grid: {
    fill: primaryTextColor,
    stroke: primaryTextColor,
    strokeWidth: 0.2,
    strokeOpacity: 0.7,
  },
  axisLabel: {
    fill: primaryTextColor,
    padding: 35,
    fontSize: 10,
  },
  axis: {
    fill: primaryTextColor,
  },
  tickLabels: {
    fill: primaryTextColor,
    padding: -20,
  },
  ticks: {
    fill: primaryTextColor,
  },
});

export const YAxisStyle = ({
  primaryTextColor,
}: DefaultTheme) => ({
  grid: {
    stroke: 'none',
    fill: primaryTextColor,
  },
  axisLabel: {
    fill: primaryTextColor,
    padding: 35,
    fontSize: 10,
  },
  axis: {
    fill: primaryTextColor,
  },
  tickLabels: {
    fill: primaryTextColor,
  },
  ticks: {
    fill: primaryTextColor,
  },
});

export const XAxisStyle = ({
  primaryTextColor,
}: DefaultTheme) => ({
  grid: {
    stroke: 'none',
  },
  axisLabel: {
    fill: primaryTextColor,
    padding: 30,
    fontSize: 10,
  },
  axis: {
    fill: primaryTextColor,
  },
  tickLabels: {
    fill: primaryTextColor,
  },
});

export const AreaStyle = ({
  graphAreaColor,
  primaryTextColor,
}: DefaultTheme) => ({
  data: {
    fill: graphAreaColor,
  },
  labels: {
    fill: primaryTextColor,
  },
});

export const LabelStyle = ({
  chartTitleColor,
}: DefaultTheme) => ({
  fill: chartTitleColor,
});
