import React from 'react';
import * as Progress from 'react-native-progress';
import { useTheme } from 'styled-components/native';
import RadialGradient from 'react-native-radial-gradient';
import { Text } from '../text/text';
import {
  RingContainer,
  RingTextContainer,
  RingTextLayout,
} from './charging-ring.styles';
import { TimingFade } from '../animation/timing-fade';
import { getHoursFromMins, hex2rgba } from '../../lib/util';

interface IChargingRingProps {
  batteryLevel?: number;
  ringSize?: number;
  charging?: boolean;
  minsToFull?: number;
}

const DEFAULT_RING_SIZE = 300;
const DEFAULT_RING_THICKNESS = 30;

export const ChargingRing = (props: IChargingRingProps) => {
  const {
    batteryLevel = 0,
    ringSize = DEFAULT_RING_SIZE,
    charging = false,
    minsToFull = 0,
  } = props;
  const {
    primaryBackground,
    chargingRingColor,
    chargingRingGradientColor,
  } = useTheme();
  const [hours, minutes] = getHoursFromMins(minsToFull);
  return (
    <RingContainer>
      <Progress.Circle
        animated
        size={ringSize}
        progress={batteryLevel / 100}
        color={chargingRingColor}
        thickness={DEFAULT_RING_THICKNESS}
        borderWidth={0}
      />
      <RingTextContainer>
        <TimingFade
          loop={charging}
          disabled={!charging}
          otherViewProps={{
            height: '100%',
            position: 'absolute',
          }}>
          <RadialGradient
            style={{
              height: '100%',
              width: '100%',
            }}
            radius={DEFAULT_RING_SIZE / 3}
            colors={[
              chargingRingGradientColor,
              hex2rgba(primaryBackground, 0),
            ]}
          />
        </TimingFade>
        <RingTextLayout>
          <Text bold="bold" variant="large" alignX="center">
            {batteryLevel.toString() + '%'}
          </Text>
          {charging && (
            <Text alignX="center" variant="xxsmall">
              {hours
                ? `${hours} ${
                    hours > 1 ? 'hours' : 'hour'
                  } ${minutes} mins remaining`
                : `${minutes} mins remaining`}
            </Text>
          )}
        </RingTextLayout>
      </RingTextContainer>
    </RingContainer>
  );
};
