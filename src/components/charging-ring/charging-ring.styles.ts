import styled from 'styled-components/native';

export const RingContainer = styled.View`
  background-color: transparent;
  padding: 50px;
  border-radius: 500px;
`;

export const RingTextContainer = styled.View`
  position: absolute;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
`;

export const RingTextLayout = styled.View`
  display: flex;
  flex: 1;
  flex-direction: column;
  justify-content: center;
  align-items: center;
`;
