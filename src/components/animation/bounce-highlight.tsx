import React, { useEffect, useRef } from 'react';
import { Animated, Easing } from 'react-native';

interface IBounceHighlight {
  children: React.ReactNode;
  duration?: number;
  on?: boolean;
}

export const BounceHighlight = (
  props: IBounceHighlight,
) => {
  const { children, duration = 1000, on } = props;
  const opacity = useRef(new Animated.Value(0));
  const scale = useRef(new Animated.Value(0));

  useEffect(() => {
    Animated.parallel([
      Animated.timing(opacity.current, {
        toValue: on ? 1 : 0,
        duration,
        easing: Easing.bounce,
        useNativeDriver: true,
      }),
      Animated.timing(scale.current, {
        toValue: on ? 1 : 0,
        duration,
        easing: Easing.bounce,
        useNativeDriver: true,
      }),
    ]).start();
  }, [on, duration]);

  return (
    <Animated.View
      style={{
        opacity: opacity.current,
        width: '100%',
        transform: [
          {
            scale: scale.current,
          },
        ],
      }}>
      {children}
    </Animated.View>
  );
};
