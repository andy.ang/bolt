import React, { useEffect, useRef } from 'react';
import { Animated, Easing } from 'react-native';

interface IRotate {
  children: React.ReactNode;
  duration?: number;
  loading?: boolean;
}

export const Rotate = (props: IRotate) => {
  const {
    children,
    duration = 1000,
    loading = false,
  } = props;
  const spin = useRef(new Animated.Value(0));

  useEffect(() => {
    const spinAnimation = Animated.loop(
      Animated.timing(spin.current, {
        toValue: 1,
        duration,
        easing: Easing.linear,
        useNativeDriver: true,
      }),
    );
    if (loading) {
      spinAnimation.start();
    } else {
      spinAnimation.stop();
      spin.current.setValue(0);
    }
  }, [loading, duration]);

  return (
    <Animated.View
      style={{
        transform: [
          {
            rotate: spin.current.interpolate({
              inputRange: [0, 1],
              outputRange: ['0deg', '360deg'],
            }),
          },
        ],
      }}>
      {children}
    </Animated.View>
  );
};
