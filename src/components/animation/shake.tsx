import React, { useEffect, useRef } from 'react';
import { Animated, Easing } from 'react-native';

interface IShakeProps {
  children: React.ReactNode;
  duration?: number;
  animate?: boolean;
}

export const Shake = (props: IShakeProps) => {
  const {
    children,
    duration = 500,
    animate = false,
  } = props;
  const movementX = useRef(new Animated.Value(0));

  useEffect(() => {
    const shakingAnimation = Animated.sequence([
      Animated.timing(movementX.current, {
        toValue: 10,
        duration: 100,
        easing: Easing.linear,
        useNativeDriver: true,
      }),
      Animated.timing(movementX.current, {
        toValue: 0,
        duration,
        easing: Easing.bounce,
        useNativeDriver: true,
      }),
    ]);

    if (animate) {
      shakingAnimation.start();
    } else {
      shakingAnimation.reset();
    }
  }, [duration, animate]);

  return (
    <Animated.View
      style={{
        width: '100%',
        transform: [
          {
            translateX: movementX.current,
          },
        ],
      }}>
      {children}
    </Animated.View>
  );
};
