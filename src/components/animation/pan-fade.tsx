import React from 'react';
import { Animated } from 'react-native';

interface IPanFade {
  children: React.ReactNode;
  fadeSpeed: 'fast' | 'slow';
  animatedValue: Animated.Value;
}

const SLOW_FADE = {
  inputRange: [0, 0.2, 0.4, 0.6, 0.8, 1],
  outputRange: [0, 0.1, 0.2, 0.5, 0.8, 1],
};

const FAST_FADE = {
  inputRange: [0, 0.2, 0.4, 0.6, 0.8, 1],
  outputRange: [0, 0.1, 0.1, 0.2, 0.4, 1],
};

const SPEED = {
  fast: FAST_FADE,
  slow: SLOW_FADE,
};

export const PanFade = (props: IPanFade) => {
  const { children, fadeSpeed, animatedValue } = props;

  return (
    <Animated.View
      style={{
        opacity: animatedValue.interpolate(
          SPEED[fadeSpeed],
        ),
      }}>
      {children}
    </Animated.View>
  );
};
