import React, { useEffect, useRef } from 'react';
import { Animated } from 'react-native';

interface ITimingFade {
  children: React.ReactNode;
  duration?: number;
  loop?: boolean;
  disabled?: boolean;
  otherViewProps?: Record<string, any>;
}

export const TimingFade = (props: ITimingFade) => {
  const {
    children,
    duration = 1000,
    loop = false,
    disabled = false,
    otherViewProps = {},
  } = props;
  const opacity = useRef(new Animated.Value(0));

  useEffect(() => {
    if (loop && !disabled) {
      Animated.loop(
        Animated.sequence([
          Animated.timing(opacity.current, {
            toValue: 1,
            duration,
            useNativeDriver: true,
          }),
          Animated.timing(opacity.current, {
            toValue: 0,
            duration,
            useNativeDriver: true,
          }),
        ]),
        { iterations: loop ? -1 : 1 },
      ).start();
    } else if (!disabled) {
      Animated.timing(opacity.current, {
        toValue: 1,
        duration,
        useNativeDriver: true,
      }).start();
    } else {
      Animated.timing(opacity.current, {
        toValue: 0,
        duration,
        useNativeDriver: true,
      }).start();
    }
  }, [loop, disabled, duration]);

  return (
    <Animated.View
      style={{
        opacity: opacity.current,
        width: '100%',
        ...otherViewProps,
      }}>
      {children}
    </Animated.View>
  );
};
