import React, { useEffect, useState } from 'react';
import {
  ActivityIndicator,
  Switch as RNSwitch,
} from 'react-native';
import { CustomButton } from '../button/custom-button';
import { Column } from '../layout/Column';
import { Text } from '../text/text';
import { SwitchContainer } from './switch.styles';

interface ISwitchProps {
  label: string;
  value: boolean;
  onValueChange: (val: boolean) => void;
  disabled?: boolean;
  loading?: boolean;
  showButton?: boolean;
  buttonLabel?: string;
  buttonOnPress?: () => void;
  buttonDisabled?: boolean;
}

const ACTIVITY_INDICATOR_SIZE = 30;
export const Switch = (props: ISwitchProps) => {
  const {
    label,
    value,
    onValueChange,
    disabled = false,
    loading = false,
    showButton = false,
    buttonLabel = '',
    buttonOnPress,
    buttonDisabled = false,
  } = props;

  const [switchValue, setSwitchValue] = useState<boolean>(
    value,
  );

  useEffect(() => {
    setSwitchValue(value);
  }, [value]);

  return (
    <Column alignX="space-between" hideGutter>
      <Text>{label}</Text>
      <SwitchContainer>
        <Column hideGutter alignX="right">
          {showButton && (
            <CustomButton
              height={30}
              width={80}
              label={buttonLabel}
              onPress={buttonOnPress}
              disabled={buttonDisabled}
            />
          )}
          {loading && (
            <ActivityIndicator
              size={ACTIVITY_INDICATOR_SIZE}
            />
          )}
          <RNSwitch
            value={switchValue}
            disabled={disabled || loading}
            onValueChange={(val) => {
              setSwitchValue(val);
              onValueChange(val);
            }}
          />
        </Column>
      </SwitchContainer>
    </Column>
  );
};
