import { useTheme } from 'styled-components/native';
import React from 'react';
import {
  GooglePlacesAutocomplete,
  GooglePlaceData,
  GooglePlaceDetail,
} from 'react-native-google-places-autocomplete';
import { config } from '../../config/config';
import {
  InputLayout,
  shadow,
} from './address-input.styles';
import { AddressRow } from './address-row';
import { LatLng } from 'react-native-maps';
import { Fonts } from '../../typings/theme.types';

interface IAddressInput {
  location: LatLng;
  onResultSelected: (
    data: GooglePlaceData,
    details: GooglePlaceDetail | null,
  ) => void;
}

export const AddressInput = (props: IAddressInput) => {
  const { onResultSelected, location } = props;
  const {
    primaryBackground,
    primaryTextColor,
    placeholderTextColor,
    cursorColor,
  } = useTheme();

  return (
    <InputLayout>
      <GooglePlacesAutocomplete
        placeholder="Search"
        fetchDetails={true}
        suppressDefaultStyles
        onPress={onResultSelected}
        GooglePlacesDetailsQuery={{
          fields: 'geometry,photos,formatted_address',
          location: `${location.latitude},${location.longitude}`,
        }}
        //   onFail={(error) => console.error(error)}
        query={{
          key: config.GOOGLE_API_KEY,
          language: 'en',
        }}
        debounce={200}
        enablePoweredByContainer={false}
        listUnderlayColor="transparent"
        disableScroll
        isRowScrollable={false}
        renderRow={(data) => <AddressRow {...data} />}
        textInputProps={{
          placeholderTextColor: placeholderTextColor,
          backgroundColor: primaryBackground,
          color: primaryTextColor,
          selectionColor: cursorColor,
          style: { fontFamily: Fonts.OXANIUM },
          borderRadius: 100,
          paddingLeft: 20,
          height: 40,
          marginBottom: 10,
          ...shadow,
        }}
      />
    </InputLayout>
  );
};
