import styled from 'styled-components/native';
import { Dimensions } from 'react-native';

const { width } = Dimensions.get('screen');

export const InputLayout = styled.View`
  width: 100%;
  padding: 10px;
`;

export const shadow = {
  shadowOffset: { width: 0, height: 0 },
  shadowColor: 'black',
  shadowOpacity: 0.3,
  shadowRadius: 5,
};

export const AddressRowContainer = styled.View`
  background: ${(props) => props.theme.primaryBackground};
  border-radius: 50px;
  width: ${width - 30}px;
  height: 35px;
  justify-content: center;
  padding-horizontal: 10px;
  margin-left: 5px;
  margin-top: 3px;
  margin-bottom: 3px;
`;
