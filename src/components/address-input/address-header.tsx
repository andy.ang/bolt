import React from 'react';
import { GooglePlaceData } from 'react-native-google-places-autocomplete';
import { Text } from '../text/text';
import { AddressRowContainer } from './address-input.styles';

export const AddressRow = (props: GooglePlaceData) => {
  return (
    <AddressRowContainer>
      <Text>{props.description}</Text>
    </AddressRowContainer>
  );
};
