import React from 'react';
import { HeaderButton } from '../button/header-button';
import { Icons } from '../icon/icon.types';
import { Text } from '../text/text';
import {
  CenterLayout,
  HeaderLayout,
  LeftLayout,
  RightLayout,
} from './header.styles';

interface IButton {
  icon?: Icons;
  label?: string;
  onPress: () => void;
  disabled?: boolean;
  animating?: boolean;
}
interface IHeader {
  title: string;
  leftButton?: IButton;
  rightButton?: IButton;
}

export const Header = (props: IHeader) => {
  const { title, leftButton, rightButton } = props;

  return (
    <HeaderLayout>
      <LeftLayout>
        {leftButton && (
          <HeaderButton
            icon={leftButton.icon}
            onPress={leftButton.onPress}
            disabled={leftButton.disabled}
            animating={leftButton.animating}
          />
        )}
      </LeftLayout>
      <CenterLayout>
        <Text
          variant="big"
          bold="extra-bold"
          alignX="center">
          {title}
        </Text>
      </CenterLayout>
      <RightLayout>
        {rightButton && (
          <HeaderButton
            icon={rightButton.icon}
            label={rightButton.label}
            onPress={rightButton.onPress}
            disabled={rightButton.disabled}
            animating={rightButton.animating}
          />
        )}
      </RightLayout>
    </HeaderLayout>
  );
};
