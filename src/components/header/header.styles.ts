import styled from 'styled-components/native';

export const HeaderLayout = styled.View`
  height: 60px;
  width: 100%;
  background-color: transparent;
  flex-direction: row;
  justify-content: center;
  align-items: center;
`;

export const LeftLayout = styled.View`
  flex: 1;
  justify-content: center;
  align-items: flex-start;
  padding-left: 10px;
`;

export const RightLayout = styled.View`
  flex: 1;
  justify-content: center;
  align-items: flex-end;
  padding-right: 10px;
`;

export const CenterLayout = styled.View`
  flex: 2;
  justify-content: center;
  align-items: center;
`;
