import React, { forwardRef } from 'react';
import RNPickerSelect from 'react-native-picker-select';
import { useTheme } from 'styled-components/native';
import { Fonts } from '../../typings/theme.types';

interface IItem {
  label: string;
  value: string | number;
}
interface IDropDown {
  items: IItem[];
  placeholder: string;
  onItemPicked: (value: IItem['value']) => void;
  state?: 'open' | 'close';
  value: IItem['value'];
}

export const DropDown = forwardRef(
  (
    props: IDropDown,
    ref: React.ForwardedRef<RNPickerSelect>,
  ) => {
    const {
      items,
      onItemPicked,
      placeholder,
      value,
    } = props;

    const {
      primaryBackground,
      primaryTextColor,
    } = useTheme();

    return (
      <RNPickerSelect
        ref={ref}
        items={items}
        itemKey="label"
        value={value}
        onValueChange={onItemPicked}
        placeholder={{ label: placeholder, value: null }}
        pickerProps={{
          style: {
            backgroundColor: primaryBackground,
          },
          itemStyle: {
            color: primaryTextColor,
            fontFamily: Fonts.OXANIUM,
          },
        }}
        style={{
          placeholder: {
            color: primaryTextColor,
            backgroundColor: primaryBackground,
            fontFamily: Fonts.OXANIUM,
          },
          inputIOS: {
            fontFamily: Fonts.OXANIUM,
          },
          chevronContainer: {
            backgroundColor: primaryBackground,
          },
          viewContainer: {
            backgroundColor: primaryBackground,
          },
          modalViewMiddle: {
            backgroundColor: primaryBackground,
          },
          done: {
            color: primaryTextColor,
          },
        }}
      />
    );
  },
);
