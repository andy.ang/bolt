import styled from 'styled-components/native';

export const TextLinkContainer = styled.TouchableOpacity`
  background: transparent;
  border-bottom-width: 1px;
  border-bottom-color: ${(props) =>
    props.theme.primaryTextColor};
`;
