import React from 'react';
import { Linking } from 'react-native';
import { Text } from '../text/text';
import { TextLinkContainer } from './text-link.styles';

interface ITextLink {
  message: string;
  onPress?: () => void;
  url?: string;
  disabled?: boolean;
}
export const TextLink = (props: ITextLink) => {
  const { message, onPress, url, disabled = false } = props;

  const navigateToUrl = () => {
    if (url) {
      Linking.openURL(url);
    }
  };

  return (
    <TextLinkContainer
      disabled={disabled}
      style={{ opacity: disabled ? 0.2 : 1 }}
      onPress={url ? navigateToUrl : onPress}>
      <Text variant="xsmall" bold="extra-light">
        {message}
      </Text>
    </TextLinkContainer>
  );
};
