import React from 'react';
import { DragSortableView } from 'react-native-drag-sort';
import { IconSize } from '../icon/icon.types';
import { ControlButton } from './control-button';
import {
  ControlButtonContainer,
  ControlButtonLayout,
} from './control-panel.styles';

type IControlPanelProps<T> = {
  width: number;
  data: T[];
  onDragStart: (fromIndex: number) => void;
  onDragEnd: (fromIndex: number, toIndex: number) => void;
  onChange: (data: T[]) => void;
  onPressItem?: (data: T[], item: T, index: number) => void;
  grid?: 3 | 4 | 5;
};

const MAX_WIDTH = 600;
const MAX_CHILDREN_DIMENSIONS = 100;

export const ControlPanelEditable = <T extends any>(
  props: IControlPanelProps<T>,
) => {
  const {
    width,
    data,
    onDragEnd,
    onDragStart,
    onPressItem,
    onChange,
    grid = 3,
  } = props;
  const useOriginalWidth = width < MAX_WIDTH;
  const panelWidth = useOriginalWidth ? width : MAX_WIDTH;
  const childWidth = useOriginalWidth
    ? panelWidth / grid
    : MAX_CHILDREN_DIMENSIONS;

  return (
    <DragSortableView
      dataSource={data}
      parentWidth={panelWidth}
      isDragFreely
      childrenWidth={childWidth}
      childrenHeight={childWidth}
      onDragStart={onDragStart}
      onDragEnd={onDragEnd}
      onClickItem={onPressItem}
      onDataChange={onChange}
      renderItem={(item) => (
        <ControlButtonLayout
          style={{
            width: childWidth,
            height: childWidth,
          }}>
          <ControlButtonContainer>
            <ControlButton
              icon={item.inactive.icon}
              size={
                item.inactive.label
                  ? IconSize.big
                  : IconSize.large
              }
              label={item.inactive.label}
            />
          </ControlButtonContainer>
        </ControlButtonLayout>
      )}
    />
  );
};
