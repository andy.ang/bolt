import React from 'react';
import { Colors } from '../../typings/theme.types';
import { Icons, IconSize } from '../icon/icon.types';
import { ControlButton } from './control-button';
import {
  ControlPanelLayout,
  ControlPanelItemContainer,
  ControlPanelItemView,
} from './control-panel.styles';

interface IControlPanelProps {
  children: (JSX.Element | undefined)[];
}

interface IControlPanelItemProps {
  active?: boolean;
  icon: Icons;
  label?: string;
  loading?: boolean;
  disabled?: boolean;
  color?: Colors;
  onPress?: () => void;
}

export const ControlPanel = (props: IControlPanelProps) => {
  const { children } = props;

  return (
    <ControlPanelLayout>{children}</ControlPanelLayout>
  );
};

const ControlPanelitem = ({
  icon,
  label,
  loading = false,
  disabled = false,
  onPress,
  active = false,
}: IControlPanelItemProps) => {
  return (
    <ControlPanelItemContainer>
      <ControlPanelItemView
        style={{
          opacity: disabled ? 0.3 : 1,
        }}
        disabled={loading || disabled}
        onPress={onPress}
        active={active}>
        <ControlButton
          icon={icon}
          label={label}
          loading={loading}
          size={label ? IconSize.medium : IconSize.big}
        />
      </ControlPanelItemView>
    </ControlPanelItemContainer>
  );
};

ControlPanel.Item = ControlPanelitem;
