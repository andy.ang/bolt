import styled from 'styled-components/native';

interface IControlPanelItemViewProps {
  active?: boolean;
}

export const StyledControlButton = styled.View`
  background: transparent;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  width: 100%;
  height: 100%;
`;

export const ControlButtonLayout = styled.View`
  background-color: transparent;
  flex-direction: row;
  justify-content: center;
  align-items: center;
`;

export const ControlButtonContainer = styled.View`
  border-radius: 5px;
  background-color: ${(props) =>
    props.theme.primaryBackground};
  border: 1px solid ${(props) => props.theme.borderColor};
  width: 80px;
  height: 80px;
`;

export const ControlPanelLayout = styled.View`
  width: 100%;
  max-width: 600px;
  flex: 1;
  flex-direction: row;
  flex-flow: row wrap;
  justify-content: flex-start;
  align-items: center;
`;

export const ControlPanelItemContainer = styled.View`
  height: 85px;
  width: 25%;
  min-width: 80px;
  max-width: 100px;
`;

export const ControlPanelItemView = styled.TouchableOpacity<
  IControlPanelItemViewProps
>`
  border-radius: 5px;
  background: ${(props) =>
    props.active
      ? props.theme.controlItemActiveButtonColor
      : props.theme.controlItemInactiveButtonColor};
  margin: 7px;
`;

export const IconContainer = styled.View`
  height: 35px;
`;
