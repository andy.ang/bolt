import React from 'react';
import { ActivityIndicator } from 'react-native';
import { useTheme } from 'styled-components/native';
import { Icon } from '../icon/icon';
import { Icons, IconSize } from '../icon/icon.types';
import { Text } from '../text/text';
import {
  IconContainer,
  StyledControlButton,
} from './control-panel.styles';

interface IControlButtonProps {
  icon: Icons;
  size: IconSize;
  label?: string;
  loading?: boolean;
}

export const ControlButton = (
  props: IControlButtonProps,
) => {
  const { primaryTextColor } = useTheme();
  const { icon, size, label, loading = false } = props;
  return (
    <StyledControlButton>
      {!loading && (
        <IconContainer>
          <Icon
            name={icon}
            size={size}
            color={primaryTextColor}
          />
        </IconContainer>
      )}
      {!loading && label && (
        <Text variant="xxsmall" alignX="center">
          {label}
        </Text>
      )}
      {loading && <ActivityIndicator />}
    </StyledControlButton>
  );
};
