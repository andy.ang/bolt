import React from 'react';
import { useTheme } from 'styled-components/native';
import { Fonts } from '../../typings/theme.types';
import { Text } from '../text/text';
import {
  StyledTextInput,
  TextContainer,
  TextInputLayout,
} from './text-input.styles';

interface iTextInput {
  value: string;
  placeholder: string;
  maskInput?: boolean;
  onChangeText: (text: string) => void;
  prefix?: string;
}

export const TextInput = (props: iTextInput) => {
  const {
    value,
    placeholder,
    maskInput = false,
    onChangeText,
    prefix,
  } = props;
  const { cursorColor, placeholderTextColor } = useTheme();

  return (
    <TextInputLayout>
      {prefix !== undefined && (
        <TextContainer>
          <Text variant="xxsmall" alignX="right">
            {prefix}
          </Text>
        </TextContainer>
      )}
      <StyledTextInput
        value={value}
        placeholder={placeholder}
        autoCapitalize={'none'}
        placeholderTextColor={placeholderTextColor}
        selectionColor={cursorColor}
        spellCheck={false}
        secureTextEntry={maskInput}
        selectTextOnFocus={true}
        onChangeText={onChangeText}
        style={{
          textAlign:
            prefix !== undefined ? 'left' : 'center',
          fontFamily: Fonts.OXANIUM,
        }}
      />
    </TextInputLayout>
  );
};
