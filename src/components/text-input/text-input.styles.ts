import styled from 'styled-components/native';

export const StyledTextInput = styled.TextInput`
  height: 40px;
  flex: 6;
  max-width: 400px;
  background: ${(props) => props.theme.textInputBackground};
  padding-horizontal: 10px;
  color: ${(props) => props.theme.primaryTextColor};
`;

export const TextInputLayout = styled.View`
  width: 100%;
  flex-direction: row;
  justify-content: center;
  align-items: center;
`;

export const TextContainer = styled.View`
  height: 40px;
  flex: 1;
  background: ${(props) => props.theme.textInputBackground};
  justify-content: center;
  align-items: flex-end;
`;
