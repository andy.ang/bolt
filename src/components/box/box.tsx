import React from 'react';
import { BoxContainer } from './box.styles';

interface IBox {
  children: React.ReactNode;
  size?: 'xxsmall' | 'xsmall' | 'small' | 'medium' | 'big';
}

const Height = {
  xxsmall: 50,
  xsmall: 110,
  small: 150,
  medium: 200,
  big: 300,
};

export const Box = (props: IBox) => {
  const { children, size = 'medium' } = props;
  const height = Height[size];

  return (
    <BoxContainer
      style={{
        height,
      }}>
      {children}
    </BoxContainer>
  );
};
