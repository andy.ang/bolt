import styled from 'styled-components/native';

export const BoxContainer = styled.View`
  background: ${(props) => props.theme.primaryBackground};
  height: 200px;
  width: 100%;
  max-width: 600px;
  border-radius: 5px;
  overflow: hidden;
`;
