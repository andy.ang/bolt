import React from 'react';
import SegmentedControl from '@react-native-community/segmented-control';
import { useTheme } from 'styled-components/native';

type Value = {
  label: string;
  value: string | number;
};

interface ITabPickerProps {
  values: Value[];
  onItemSelected: (item: Value) => void;
  defaultItem?: Value;
}

export const TabPicker = (props: ITabPickerProps) => {
  const { values, onItemSelected, defaultItem } = props;
  const {
    primaryBackground,
    primaryTextColor,
    activeTabPickerColor,
  } = useTheme();
  const defaultIndex = values.findIndex(
    (val) => val.value === defaultItem?.value,
  );

  return (
    <SegmentedControl
      style={{
        borderWidth: 1,
        borderRadius: 5,
      }}
      values={values.map((val) => val.label)}
      selectedIndex={defaultIndex > -1 ? defaultIndex : 0}
      backgroundColor={primaryBackground}
      tintColor={activeTabPickerColor}
      fontStyle={{ color: primaryTextColor }}
      activeFontStyle={{ color: primaryTextColor }}
      onChange={(event) => {
        onItemSelected(
          values[event.nativeEvent.selectedSegmentIndex],
        );
      }}
    />
  );
};
