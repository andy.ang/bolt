import React from 'react';
import Ionicons from 'react-native-vector-icons/Ionicons';
import Fontisto from 'react-native-vector-icons/Fontisto';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import { createIconSetFromIcoMoon } from 'react-native-vector-icons';
import icoMoonConfig from '../../assets/selection.json';
import {
  Icons,
  CustomIcons,
  FontisoIcons,
  MaterialIcons,
} from './icon.types';
import { useTheme } from 'styled-components/native';

const CustomIcon = createIconSetFromIcoMoon(icoMoonConfig);
interface IIcon {
  name: Icons;
  size: number;
  color?: string;
  scale?: number;
}

export const Icon = (props: IIcon) => {
  const { primaryTextColor } = useTheme();
  const {
    name,
    size,
    color = primaryTextColor,
    scale = 1,
  } = props;
  const isFontisoIcons = Object.values(
    FontisoIcons,
  ).includes((name as unknown) as FontisoIcons);

  const isCustomIcons = Object.values(CustomIcons).includes(
    (name as unknown) as CustomIcons,
  );

  const isMaterialIcons = Object.values(
    MaterialIcons,
  ).includes((name as unknown) as MaterialIcons);

  if (isCustomIcons) {
    return (
      <CustomIcon
        name={name}
        size={size * scale}
        color={color}
        style={{ marginTop: 6, width: size * scale }}
      />
    );
  }

  if (isMaterialIcons) {
    return (
      <MaterialCommunityIcons
        name={name}
        size={size}
        color={color}
      />
    );
  }

  if (isFontisoIcons) {
    return (
      <Fontisto name={name} size={size} color={color} />
    );
  }
  return <Ionicons name={name} size={size} color={color} />;
};
