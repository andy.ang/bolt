import React from 'react';
import SwitchSelector, {
  ISwitchSelectorProps,
} from 'react-native-switch-selector';
import { useTheme } from 'styled-components/native';

interface ICustomSelector {
  disabled?: boolean;
  onSelect: ISwitchSelectorProps['onPress'];
  options: ISwitchSelectorProps['options'];
  height?: Height;
  width?: number;
  initial?: number;
}

type Height = 'xlarge' | 'large' | 'medium' | 'small';

const HeightMap = {
  xlarge: 32,
  large: 28,
  medium: 24,
  small: 16,
};

export const CustomSelector = ({
  disabled,
  onSelect,
  options,
  height = 'medium',
  width,
  initial = 0,
}: ICustomSelector) => {
  const {
    basic,
    primaryTextColor,
    secondaryTextColor,
    borderColor,
    buttonBackground,
  } = useTheme();
  const selectorWidth = width ? width : options.length * 60;
  return (
    <SwitchSelector
      initial={initial > -1 ? initial : 0}
      height={HeightMap[height]}
      disabled={disabled}
      hasPadding
      backgroundColor={basic}
      selectedColor={primaryTextColor}
      textColor={secondaryTextColor}
      borderColor={borderColor}
      buttonColor={buttonBackground}
      style={{ width: selectorWidth }}
      options={options}
      onPress={onSelect}
    />
  );
};
