import { TouchableOpacity, View } from 'react-native';
import styled from 'styled-components/native';

export const ListLayout = styled(View)`
  height: 100%;
  width: 100%;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
  padding-horizontal: 15px;
  background: ${(props) => props.theme.listColor};
  border-top-width: 0.5px;
  border-top-color: ${(props) => props.theme.borderColor};
  border-bottom-width: 0.5px;
  border-bottom-color: ${(props) =>
    props.theme.borderColor};
`;

export const TouchableList = styled(TouchableOpacity)`
  height: 70px;
  width: 100%;
`;
