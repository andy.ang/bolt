import React from 'react';
import { GestureResponderEvent } from 'react-native';
import { Colors } from '../../typings/theme.types';
import { Icon } from '../icon/icon';
import { Icons, IconSize } from '../icon/icon.types';
import { Text } from '../text/text';
import { ListLayout, TouchableList } from './list.styles';

interface IList {
  label: string;
  icon?: Icons;
  iconColor?: Colors;
  onPress: (event: GestureResponderEvent) => void;
}

export const List = (props: IList) => {
  const {
    label,
    icon,
    iconColor = Colors.PURPLE_NAVY,
    onPress,
  } = props;
  return (
    <TouchableList onPress={onPress}>
      <ListLayout>
        {icon && (
          <Icon
            name={icon}
            size={IconSize.small}
            color={iconColor}
          />
        )}
        <Text alignX="center">{label}</Text>
      </ListLayout>
    </TouchableList>
  );
};
