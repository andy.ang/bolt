import React from 'react';
import Pulse from 'react-native-pulse';
import { useTheme } from 'styled-components/native';
import { CUSTOM_MARKER } from '../../assets/images';
import {
  CustomMarkerContainer,
  CustomMarkerImageStyles,
} from './map.styles';

export const CustomMarker = () => {
  const { markerColor } = useTheme();
  return (
    <CustomMarkerContainer>
      <Pulse
        color={markerColor}
        image={{
          source: CUSTOM_MARKER,
          style: CustomMarkerImageStyles,
        }}
        numPulses={2}
        diameter={60}
        speed={40}
        duration={2000}
      />
    </CustomMarkerContainer>
  );
};
