import React, { forwardRef } from 'react';
import MapView, {
  LatLng,
  Marker,
  Polyline,
  PROVIDER_GOOGLE,
} from 'react-native-maps';
import nightMap from './night-map.json';
import dayMap from './day-map.json';
import { CustomMarker } from './custom-marker';
import { useTheme } from 'styled-components/native';

interface IMap {
  locations: LatLng[];
  darkMode?: boolean;
  routes: LatLng[];
  onMapPressed?: () => void;
  onMarkerPressed?: () => void;
}

const ROUTE_WIDTH = 6;

export const Map = forwardRef(
  (props: IMap, ref: React.ForwardedRef<MapView>) => {
    const {
      locations,
      darkMode = false,
      routes,
      onMapPressed,
      onMarkerPressed,
    } = props;
    const { routeColor } = useTheme();
    const [origin, destination] = locations;
    const { latitude, longitude } = origin;

    return (
      <MapView
        ref={ref}
        cacheEnabled
        style={{
          height: '100%',
          width: '100%',
        }}
        provider={PROVIDER_GOOGLE}
        onPress={onMapPressed}
        customMapStyle={darkMode ? nightMap : dayMap}
        initialCamera={{
          center: {
            latitude,
            longitude,
          },
          pitch: 10,
          heading: 10,
          altitude: 100,
          zoom: 16,
        }}>
        <Marker
          onPress={onMarkerPressed}
          coordinate={{
            latitude,
            longitude,
          }}>
          <CustomMarker />
        </Marker>
        {destination && (
          <Marker
            onPress={onMarkerPressed}
            coordinate={{
              latitude: destination.latitude,
              longitude: destination.longitude,
            }}
          />
        )}
        {routes.length > 0 && (
          <Polyline
            coordinates={routes}
            strokeColor={routeColor} // fallback for when `strokeColors` is not supported by the map-provider
            strokeWidth={ROUTE_WIDTH}
          />
        )}
      </MapView>
    );
  },
);
