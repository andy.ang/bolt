import styled from 'styled-components/native';

export const CustomMarkerContainer = styled.View`
  height: 60px;
  width: 60px;
`;

export const CustomMarkerImageStyles = {
  width: 40,
  height: 40,
  marginLeft: 10,
  marginTop: 22,
};
