import styled from 'styled-components/native';

export const BlurViewContainer = styled.SafeAreaView`
  height: 100%;
  width: 100%;
`;

export const NumberContainer = styled.View`
  border: 1px solid
    ${(props) => props.theme.primaryTextColor};
  border-radius: 100px;
  background: transparent;
  overflow: hidden;
`;

export const InputContainer = styled.View`
  width: 100%;
  height: 30px;
`;
