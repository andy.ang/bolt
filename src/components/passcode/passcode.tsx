import React, {
  useCallback,
  useEffect,
  useState,
} from 'react';
import { BlurView } from '@react-native-community/blur';
import ReactNativeHapticFeedback from 'react-native-haptic-feedback';
import { StyleSheet, Modal, Vibration } from 'react-native';
import { Text } from '../text/text';
import {
  BlurViewContainer,
  InputContainer,
  NumberContainer,
} from './passcode.styles';
import { Stack } from '../layout/Stack';
import { useTheme } from 'styled-components/native';
import { Column } from '../layout/Column';
import { CustomButton } from '../button/custom-button';
import { Placeholder } from '../placeholder/placeholder';
import { Icons } from '../icon/icon.types';
import { Shake } from '../animation/shake';
import { Header } from '../header/header';

interface IPasscodeProps {
  mode?: 'light' | 'dark';
  title?: string;
  visible?: boolean;
  passcodeLength?: number;
  requireVerification?: boolean;
  onSuccess?: (passcode: string) => void;
  onCancel: () => void;
}

const NUMBER_BUTTON_DIMENSION = 80;
const VERIFY_SUBTITLE = 'Please confirm your PIN';
export const Passcode = (props: IPasscodeProps) => {
  const { primaryBackground } = useTheme();
  const {
    mode = 'light',
    title = 'Security',
    visible = false,
    passcodeLength = 4,
    onSuccess,
    onCancel,
    requireVerification = true,
  } = props;
  const [code, setCode] = useState<string>('');
  const [confirmCode, setConfirmCode] = useState<string>(
    '',
  );
  const [verfication, setVerification] = useState<boolean>(
    false,
  );
  const [masked, setMasked] = useState<boolean>(true);
  const [error, setError] = useState<boolean>(false);
  const SUBTITLE = `Please enter your ${passcodeLength}-digit PIN `;
  const renderNumberButton = useCallback(
    (number: number) => (
      <NumberContainer>
        <CustomButton
          height={NUMBER_BUTTON_DIMENSION}
          width={NUMBER_BUTTON_DIMENSION}
          label={number.toString()}
          labelSize="big"
          inactiveColor="transparent"
          underlayColor="#ffffff"
          onPress={() => {
            ReactNativeHapticFeedback.trigger(
              'keyboardTap',
            );
            if (verfication) {
              setConfirmCode(
                (prevCode) => prevCode + number,
              );
            } else {
              setCode((prevCode) => prevCode + number);
            }
          }}
        />
      </NumberContainer>
    ),
    [setCode, setConfirmCode, verfication],
  );

  const onError = useCallback(() => {
    ReactNativeHapticFeedback.trigger('impactHeavy');
    Vibration.vibrate();
    setError(true);
    setCode('');
    setConfirmCode('');
    setVerification(false);
    setTimeout(() => {
      setError(false);
    }, 1000);
  }, []);

  useEffect(() => {
    if (visible && code) {
      if (
        !confirmCode &&
        code.length === passcodeLength &&
        !verfication &&
        requireVerification
      ) {
        setVerification(true);
      } else if (
        code.length === passcodeLength &&
        !requireVerification &&
        onSuccess
      ) {
        onSuccess(code);
        setCode('');
        setConfirmCode('');
      } else if (
        confirmCode &&
        verfication &&
        code.length === passcodeLength &&
        confirmCode.length === passcodeLength &&
        requireVerification &&
        onSuccess
      ) {
        if (confirmCode === code) {
          onSuccess(confirmCode);
          setCode('');
          setConfirmCode('');
          setVerification(false);
        } else {
          onError();
        }
      }
    }
  }, [
    code,
    confirmCode,
    verfication,
    onSuccess,
    onError,
    passcodeLength,
    requireVerification,
    visible,
  ]);

  const getValueForDisplay = useCallback(
    (value: string) => {
      if (masked) {
        return Array(value.length).fill('\u2022').join('');
      }
      return value;
    },
    [masked],
  );

  return (
    <Modal
      visible={visible}
      animationType="fade"
      transparent
      statusBarTranslucent>
      <BlurView
        style={styles.absolute}
        blurType={mode}
        blurAmount={10}
        reducedTransparencyFallbackColor={primaryBackground}
      />
      <BlurViewContainer>
        <Header
          title={title}
          rightButton={{
            icon: Icons.CLOSE,
            onPress: onCancel,
          }}
        />
        <Stack space="big">
          <Shake animate={error}>
            <Text alignX="center">
              {verfication ? VERIFY_SUBTITLE : SUBTITLE}
            </Text>
          </Shake>
          <InputContainer>
            <Text alignX="center" variant="large">
              {!verfication
                ? getValueForDisplay(code)
                : getValueForDisplay(confirmCode)}
            </Text>
          </InputContainer>
          <Placeholder height={0} />
          <Column space="big" hideGutter="bottom">
            {renderNumberButton(1)}
            {renderNumberButton(2)}
            {renderNumberButton(3)}
          </Column>
          <Column space="big" hideGutter="bottom">
            {renderNumberButton(4)}
            {renderNumberButton(5)}
            {renderNumberButton(6)}
          </Column>
          <Column space="big" hideGutter="bottom">
            {renderNumberButton(7)}
            {renderNumberButton(8)}
            {renderNumberButton(9)}
          </Column>
          <Column space="large">
            <CustomButton
              icon={masked ? Icons.EYE : Icons.EYE_OFF}
              inactiveColor="transparent"
              onPress={() => {
                setMasked((prevMasked) => !prevMasked);
              }}
            />
            {renderNumberButton(0)}
            <CustomButton
              icon={Icons.BACKSPACE}
              inactiveColor="transparent"
              onLongPress={() => setCode('')}
              onPress={() => {
                if (verfication) {
                  setConfirmCode((prevCode) =>
                    prevCode.slice(0, -1),
                  );
                } else {
                  setCode((prevCode) =>
                    prevCode.slice(0, -1),
                  );
                }
              }}
            />
          </Column>
        </Stack>
      </BlurViewContainer>
    </Modal>
  );
};

const styles = StyleSheet.create({
  absolute: {
    position: 'absolute',
    top: 0,
    left: 0,
    bottom: 0,
    right: 0,
  },
});
