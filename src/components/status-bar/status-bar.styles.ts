import styled from 'styled-components/native';

export const TextIconContainer = styled.View`
  background: ${(props) => props.theme.primaryBackground};
  padding-vertical: 4px;
  padding-horizontal: 10px;
  border-radius: 10px;
  margin-horizontal: 2px;
`;

interface IIconContainer {
  background: string;
}

export const IconContainer = styled.View<IIconContainer>`
  background: ${(props) => props.background};
  padding-vertical: 4px;
  padding-horizontal: 5px;
  border-radius: 20px;
  margin-horizontal: 2px;
`;
