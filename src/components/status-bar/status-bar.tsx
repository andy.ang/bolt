import React from 'react';
import { useTheme } from 'styled-components/native';
import { BounceHighlight } from '../animation/bounce-highlight';
import { Icon } from '../icon/icon';
import { Icons, IconSize } from '../icon/icon.types';
import { Column } from '../layout/Column';
import { TextIcon } from '../text/text-icon';
import {
  IconContainer,
  TextIconContainer,
} from './status-bar.styles';

interface IStatusBarProps {
  locked: boolean;
  hvacOn: boolean;
  charging: boolean;
}

export const StatusBar = (props: IStatusBarProps) => {
  const { locked, hvacOn, charging } = props;
  const {
    climateStatusColor,
    chargingStatusColor,
  } = useTheme();

  return (
    <Column hideGutter space="none">
      <BounceHighlight on={charging}>
        <IconContainer background={chargingStatusColor}>
          <Icon
            size={IconSize.xxsmall}
            name={Icons.FLASH_FIILED}
          />
        </IconContainer>
      </BounceHighlight>
      <TextIconContainer>
        <TextIcon
          fontSize="xsmall"
          iconSize={IconSize.xxsmall}
          alignY="top"
          icon={locked ? Icons.LOCK : Icons.UNLOCK}>
          {locked ? 'LOCKED' : 'UNLOCKED'}
        </TextIcon>
      </TextIconContainer>
      <BounceHighlight on={hvacOn}>
        <IconContainer background={climateStatusColor}>
          <Icon size={IconSize.xxsmall} name={Icons.FAN} />
        </IconContainer>
      </BounceHighlight>
    </Column>
  );
};
