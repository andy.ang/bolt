import styled from 'styled-components/native';

export const SeatHeatersControlContainer = styled.View`
  height: 100%;
  width: 100%;
`;

export const SeatHeaterButtonContainer = styled.TouchableOpacity`
  width: 100%;
  height: 40px;
  background: ${(props) =>
    props.theme.controlItemInactiveButtonColor};
  border-radius: 5px;
  align-items: center;
`;

export const ButtonLayout = styled.View`
  height: 100%;
  width: 100%;
  max-width: 120px;
  flex-direction: row;
  justify-content: space-around;
  align-items: center;
`;

export const LevelContainer = styled.View`
  height: 100%;
  width: 20px;
  background: transparent;
  flex-direction: row;
  justify-content: space-around;
  align-items: center;
`;

interface ILevelIndicatorProps {
  active?: boolean;
}

export const LevelIndicator = styled.View<
  ILevelIndicatorProps
>`
  height: 40%;
  width: 5px;
  background: ${(props) =>
    props.active
      ? props.theme.seatHeaterIndicatorActiveColor
      : props.theme.seatHeaterIndicatorInactiveColor};
  border-radius: 5px;
`;
