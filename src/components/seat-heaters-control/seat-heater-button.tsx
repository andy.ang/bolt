import React, { useEffect, useState } from 'react';
import ReactNativeHapticFeedback from 'react-native-haptic-feedback';
import { Text } from '../text/text';
import {
  ButtonLayout,
  LevelContainer,
  LevelIndicator,
  SeatHeaterButtonContainer,
} from './seat-heaters-control.styles';

interface ISeatHeaterButtonProps {
  label: string;
  disabled?: boolean;
  loading?: boolean;
  onPress?: (id: number, level: number) => void;
  level?: number;
  id: number;
}

const MAX_HEAT_LEVEL = 3;
export const SeatHeaterButton = (
  props: ISeatHeaterButtonProps,
) => {
  const {
    disabled = false,
    loading = false,
    onPress,
    label,
    level = 0,
    id,
  } = props;
  const [heatLevel, setHeatLevel] = useState<number>(level);
  const onButtonPress = (seatId: number) => {
    return () => {
      if (onPress) {
        ReactNativeHapticFeedback.trigger('impactLight');
        const newHeatLevel =
          heatLevel === MAX_HEAT_LEVEL ? 0 : heatLevel + 1;
        setHeatLevel(newHeatLevel);
        onPress(seatId, newHeatLevel);
      }
    };
  };

  useEffect(() => {
    setHeatLevel(level);
  }, [level]);

  return (
    <SeatHeaterButtonContainer
      style={{ opacity: disabled ? 0.3 : 1 }}
      onPress={onButtonPress(id)}
      disabled={disabled || loading}>
      <ButtonLayout>
        <Text alignX="center">{label}</Text>
        <LevelContainer>
          <LevelIndicator active={heatLevel > 0} />
          <LevelIndicator active={heatLevel > 1} />
          <LevelIndicator active={heatLevel > 2} />
        </LevelContainer>
      </ButtonLayout>
    </SeatHeaterButtonContainer>
  );
};
