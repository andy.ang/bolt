import React from 'react';
import { Column } from '../layout/Column';
import { Stack } from '../layout/Stack';
import { Text } from '../text/text';
import { SeatHeaterButton } from './seat-heater-button';
import { SeatHeatersControlContainer } from './seat-heaters-control.styles';

interface ISeatHeatersControlProps {
  leftHandDrive?: boolean;
  hasRearSeatHeaters?: boolean;
  frontLeft?: number;
  frontRight?: number;
  rearLeft?: number;
  rearCenter?: number;
  rearRight?: number;
  disabled?: boolean;
  onPress?: (seatId: number, heatLevel: number) => void;
}

export const SeatHeatersControl = (
  props: ISeatHeatersControlProps,
) => {
  const {
    leftHandDrive,
    hasRearSeatHeaters = false,
    onPress,
    disabled = false,
    frontLeft = 0,
    frontRight = 0,
    rearCenter = 0,
    rearLeft = 0,
    rearRight = 0,
  } = props;

  return (
    <SeatHeatersControlContainer>
      <Stack>
        <Text alignX="center">FRONT</Text>
        <Column hideGutter flexChildren>
          <SeatHeaterButton
            key={0}
            label={leftHandDrive ? 'driver' : 'passenger'}
            level={frontLeft}
            id={0}
            onPress={onPress}
            disabled={disabled}
          />
          <SeatHeaterButton
            key={1}
            label={leftHandDrive ? 'passenger' : 'driver'}
            level={frontRight}
            id={1}
            onPress={onPress}
            disabled={disabled}
          />
        </Column>
        <Text alignX="center">REAR</Text>
        <Column hideGutter flexChildren>
          <SeatHeaterButton
            id={2}
            label="left"
            level={rearLeft}
            disabled={!hasRearSeatHeaters || disabled}
            onPress={onPress}
          />
          <SeatHeaterButton
            id={4}
            label="center"
            level={rearCenter}
            disabled={!hasRearSeatHeaters || disabled}
            onPress={onPress}
          />
          <SeatHeaterButton
            id={5}
            label="right"
            level={rearRight}
            disabled={!hasRearSeatHeaters || disabled}
            onPress={onPress}
          />
        </Column>
      </Stack>
    </SeatHeatersControlContainer>
  );
};
