import React, { forwardRef } from 'react';
import { Pages } from 'react-native-pages';
import { useTheme } from 'styled-components/native';

interface ICarouselProps {
  children: React.ReactNode;
  size?: 'xsmall' | 'small' | 'medium' | 'big';
}

const Height = {
  xsmall: 100,
  small: 150,
  medium: 200,
  big: 300,
};

const INDICATOR_HEIGHT = 25;

export const Carousel = forwardRef(
  (
    props: ICarouselProps,
    ref: React.ForwardedRef<typeof Pages>,
  ) => {
    const { children, size = 'medium' } = props;
    const height = Height[size] + INDICATOR_HEIGHT;
    const { primaryBackground } = useTheme();

    return (
      <Pages
        ref={ref}
        containerStyle={{
          height,
          width: '100%',
        }}
        bounces={false}
        indicatorColor={primaryBackground}>
        {children}
      </Pages>
    );
  },
);
