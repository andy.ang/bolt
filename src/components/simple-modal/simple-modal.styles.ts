import styled from 'styled-components/native';

export const ModalContainer = styled.View`
  height: 200px;
  width: 90%;
  background: ${(props) => props.theme.modalBackground};
  align-self: center;
`;

export const ButtonContainer = styled.View`
  width: 100%;
  align-items: center;
  padding-bottom: 15px;
  padding-horizontal: 15px;
`;
