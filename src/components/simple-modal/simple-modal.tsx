import React from 'react';
import Modal from 'react-native-modal';
import { Button } from '../button/button';
import { Stack } from '../layout/Stack';
import { Text } from '../text/text';
import {
  ModalContainer,
  ButtonContainer,
} from './simple-modal.styles';

interface IModal {
  visible?: boolean;
  title?: string;
  body?: string;
  footer?: string;
  buttonLabel?: string;
  buttonOnPress?: () => void;
}

export const SimpleModal = (props: IModal) => {
  const {
    title = '',
    body = '',
    footer,
    buttonLabel = 'OK',
    buttonOnPress,
    visible = false,
  } = props;
  return (
    <Modal isVisible={visible}>
      <ModalContainer>
        <Stack>
          <Text variant="medium" bold="extra-bold">
            {title}
          </Text>
          <Text variant="small" numberOfLines={3}>
            {body}
          </Text>
          {footer && <Text variant="xsmall">{footer}</Text>}
        </Stack>
        <ButtonContainer>
          <Button
            label={buttonLabel}
            onPress={buttonOnPress}
          />
        </ButtonContainer>
      </ModalContainer>
    </Modal>
  );
};
