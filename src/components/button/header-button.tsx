import { useTheme } from 'styled-components/native';
import React from 'react';
import { Icon } from '../icon/icon';
import { Text } from '../text/text';
import { StyledHeaderButton } from './button.styles';
import { Icons, IconSize } from '../icon/icon.types';
import { Rotate } from '../animation/rotate';

interface IButton {
  label?: string;
  icon?: Icons;
  iconSize?: IconSize;
  onPress: () => void;
  disabled?: boolean;
  animating?: boolean;
}

export const HeaderButton = (props: IButton) => {
  const {
    label,
    icon,
    iconSize = IconSize.small,
    onPress,
    disabled = false,
    animating = false,
  } = props;
  const { primaryTextColor } = useTheme();

  return (
    <StyledHeaderButton
      onPress={onPress}
      disabled={disabled}
      style={{ opacity: disabled ? 0.3 : 1 }}>
      {icon && (
        <Rotate loading={animating}>
          <Icon
            name={icon}
            size={iconSize}
            color={primaryTextColor}
          />
        </Rotate>
      )}
      {label && <Text>{label}</Text>}
    </StyledHeaderButton>
  );
};
