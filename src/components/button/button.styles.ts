import styled from 'styled-components/native';

export const StyledHeaderButton = styled.TouchableOpacity`
  background: transparent;
  width: 50px;
  flex-direction: row;
  justify-content: center;
  align-items: center;
`;

export const StyledButton = styled.TouchableOpacity`
  width: 100%;
  max-width: 400px;
  background: ${(props) => props.theme.buttonBackground};
  height: 40px;
  flex-direction: row;
  justify-content: center;
  align-items: center;
  z-index: -1;
`;

interface ICustomButtonProps {
  active?: boolean;
  customActiveColor?: string;
  customInactiveColor?: string;
}

export const StyledCustomButton = styled.TouchableHighlight<
  ICustomButtonProps
>`
  background-color: ${(props) =>
    props.active
      ? props.customActiveColor
        ? props.customActiveColor
        : props.theme.controlItemActiveButtonColor
      : props.customInactiveColor
      ? props.customInactiveColor
      : props.theme.controlItemInactiveButtonColor};
  justify-content: center;
  align-items: center;
  border-radius: 5px;
`;
