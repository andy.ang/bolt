import React from 'react';
import { ActivityIndicator, Keyboard } from 'react-native';
import { Icons } from '../icon/icon.types';
import { TextIcon } from '../text/text-icon';
import { StyledButton } from './button.styles';

interface IButton {
  label: string;
  icon?: Icons;
  onPress?: () => void;
  textColor?: string;
  disabled?: boolean;
  loading?: boolean;
}

export const Button = (props: IButton) => {
  const {
    label,
    icon,
    onPress,
    textColor,
    disabled = false,
    loading = false,
  } = props;

  const onButtonPress = () => {
    Keyboard.dismiss();
    if (onPress) {
      return onPress();
    }
  };

  return (
    <StyledButton
      style={{ opacity: disabled ? 0.3 : 1 }}
      onPress={onButtonPress}
      disabled={disabled || loading}>
      {!loading ? (
        <TextIcon
          stretch={false}
          textColor={textColor}
          iconSize={20}
          icon={icon}>
          {label}
        </TextIcon>
      ) : (
        <ActivityIndicator />
      )}
    </StyledButton>
  );
};
