import React, { useCallback } from 'react';
import { ActivityIndicator } from 'react-native';
import ReactNativeHapticFeedback from 'react-native-haptic-feedback';
import { useTheme } from 'styled-components/native';
import { hex2rgba } from '../../lib/util';
import { Icon } from '../icon/icon';
import { Icons, IconSize } from '../icon/icon.types';
import { Text } from '../text/text';
import { StyledCustomButton } from './button.styles';

interface ICustomButton {
  height?: number;
  width?: number | 'full-width';
  icon?: Icons;
  iconSize?: number;
  onPress?: () => void;
  onLongPress?: () => void;
  disabled?: boolean;
  loading?: boolean;
  active?: boolean;
  activeColor?: string;
  inactiveColor?: string;
  underlayColor?: string;
  label?: string;
  labelSize?:
    | 'xxlarge'
    | 'xlarge'
    | 'large'
    | 'big'
    | 'medium'
    | 'small'
    | 'xsmall'
    | 'xxsmall';
}

const DEFAULT_DIMENSION = 70;

export const CustomButton = (props: ICustomButton) => {
  const {
    height = DEFAULT_DIMENSION,
    width = DEFAULT_DIMENSION,
    icon,
    iconSize = IconSize.medium,
    onPress,
    disabled = false,
    loading = false,
    active = false,
    activeColor,
    inactiveColor,
    underlayColor,
    label = '',
    labelSize,
    onLongPress,
  } = props;
  const { controlItemInactiveButtonColor } = useTheme();
  const underlayingColor = underlayColor
    ? hex2rgba(underlayColor, 0.3)
    : hex2rgba(controlItemInactiveButtonColor, 0.3);

  const onButtonPressed = useCallback(() => {
    return () => {
      if (onPress) {
        ReactNativeHapticFeedback.trigger('impactLight');
        onPress();
      }
    };
  }, [onPress]);

  return (
    <StyledCustomButton
      active={active && !loading}
      customActiveColor={activeColor}
      customInactiveColor={inactiveColor}
      onPress={onButtonPressed()}
      onLongPress={onLongPress}
      activeOpacity={0.3}
      underlayColor={underlayingColor}
      disabled={disabled || loading}
      style={{
        height,
        width: width === 'full-width' ? '100%' : width,
        opacity: disabled ? 0.3 : 1,
      }}>
      {!loading ? (
        icon ? (
          <Icon name={icon} size={iconSize} />
        ) : (
          <Text alignX="center" variant={labelSize}>
            {label}
          </Text>
        )
      ) : (
        <ActivityIndicator />
      )}
    </StyledCustomButton>
  );
};
