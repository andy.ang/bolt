export const shadow = {
  shadowColor: 'black',
  shadowOffset: { height: 0, width: 0 },
  shadowOpacity: 0.3,
  shadowRadius: 3,
};
