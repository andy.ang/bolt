import React, { forwardRef, useRef } from 'react';
import { Animated } from 'react-native';
import { Modalize } from 'react-native-modalize';
import { useTheme } from 'styled-components/native';
import { PanFade } from '../animation/pan-fade';
import { Button } from '../button/button';
import { Icons } from '../icon/icon.types';
import { Column } from '../layout/Column';
import { Stack } from '../layout/Stack';
import { Text } from '../text/text';
import { TextIcon } from '../text/text-icon';
import { shadow } from './travel-modal.styles';

const SNAP_POINT = [150, 285];
interface ITravelModal {
  details?: {
    arrivalTime: string;
    batteryConsumptionInkWh: number;
    departureTime: string;
    lengthInMeters: number;
    trafficDelayInSeconds: number;
    travelTimeInSeconds: number;
  };
  origin: string;
  destination: string;
  batteryLevel: number;
  onSend: () => void;
}

export const TravelModal = forwardRef(
  (
    props: ITravelModal,
    ref: React.ForwardedRef<Modalize>,
  ) => {
    const {
      modalBackground,
      modalHandleColor,
    } = useTheme();
    const {
      batteryLevel = 100,
      origin,
      destination,
      details,
      onSend,
    } = props;

    const { travelTimeInSeconds = 0, lengthInMeters = 0 } =
      details || {};
    const animatedOpacity = useRef<Animated.Value>(
      new Animated.Value(0),
    );

    const batteryIcon =
      batteryLevel > 90
        ? Icons.BATTERY_FULL
        : batteryLevel > 60
        ? Icons.BATTERY_THREE_QUARTER
        : batteryLevel > 40
        ? Icons.BATTERY_HALF
        : batteryLevel > 10
        ? Icons.BATTERY_QUARTER
        : Icons.BATTERY_DEAD;

    return (
      <Modalize
        ref={ref}
        velocity={999}
        modalHeight={SNAP_POINT[1]}
        snapPoint={SNAP_POINT[0]}
        alwaysOpen={SNAP_POINT[0]}
        panGestureAnimatedValue={animatedOpacity.current}
        handlePosition="inside"
        withOverlay={false}
        closeSnapPointStraightEnabled={false}
        // Weird behavior of keyboard offset
        avoidKeyboardLikeIOS={false}
        childrenStyle={{
          flex: 1,
          backgroundColor: modalBackground,
          borderTopLeftRadius: 10,
          borderTopRightRadius: 10,
          ...shadow,
        }}
        handleStyle={{
          backgroundColor: modalHandleColor,
        }}
        scrollViewProps={{ scrollEnabled: false }}>
        <Stack space="small">
          <Text
            kind="primary"
            variant="medium"
            alignX="left">
            {origin}
          </Text>
          <PanFade
            animatedValue={animatedOpacity.current}
            fadeSpeed="slow">
            <TextIcon
              alignX="center"
              stretch
              hideIcon={!destination}
              icon={Icons.NAVIGATE_RIGHT}>
              {destination}
            </TextIcon>
          </PanFade>
          <PanFade
            animatedValue={animatedOpacity.current}
            fadeSpeed="fast">
            <Column hideGutter space="none" flexChildren>
              <TextIcon icon={Icons.CAR}>
                {`${Math.round(
                  travelTimeInSeconds / 60,
                )} min`}
              </TextIcon>
              <TextIcon icon={Icons.NAVIGATE}>
                {`${Math.round(lengthInMeters / 1000)} km`}
              </TextIcon>
              <TextIcon icon={batteryIcon}>
                {`${batteryLevel}% left`}
              </TextIcon>
            </Column>
          </PanFade>
          <PanFade
            animatedValue={animatedOpacity.current}
            fadeSpeed="fast">
            <Column hideGutter flexChildren>
              <Button
                disabled={!destination}
                label="Send to Tesla"
                onPress={onSend}
              />
            </Column>
          </PanFade>
        </Stack>
      </Modalize>
    );
  },
);
