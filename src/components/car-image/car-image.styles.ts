import styled from 'styled-components/native';

export const StyledImage = styled.Image`
  height: 100%;
  width: 100%;
`;

export const ImageContainer = styled.View`
  background: transparent;
  align-items: center;
  justify-content: center;
  width: 100%;
`;
