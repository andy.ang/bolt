import React from 'react';
import {
  Dimensions,
  ImageSourcePropType,
} from 'react-native';
import {
  ImageContainer,
  StyledImage,
} from './car-image.styles';
import { Car, Model } from './car-image.types';

interface IIMage {
  url: string;
  size?: 'normal' | 'small' | 'big';
  color?: string;
  model?: Model;
}

const { width, height } = Dimensions.get('screen');
export const CarImage = (props: IIMage) => {
  const { url, size = 'normal', model } = props;
  const carSize =
    size === 'normal'
      ? height / 4
      : size === 'big'
      ? height / 3
      : height / 5;

  const imageSource: ImageSourcePropType =
    model && Car[model]
      ? Car[model]
      : { uri: url, cache: 'force-cache' };

  return (
    <ImageContainer>
      <StyledImage
        style={{ height: carSize, width }}
        resizeMethod="scale"
        source={imageSource}
      />
    </ImageContainer>
  );
};
