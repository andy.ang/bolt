import { ImageSourcePropType } from 'react-native';
import { ROADSTER } from '../../assets/images';

type CarType = Record<
  keyof typeof Model,
  ImageSourcePropType
>;

export const Car: CarType = {
  TESLA_ROADSTER: ROADSTER,
};

export enum Model {
  TESLA_ROADSTER = 'TESLA_ROADSTER',
}
