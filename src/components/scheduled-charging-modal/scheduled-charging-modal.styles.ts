import styled from 'styled-components/native';

export const shadow = {
  shadowColor: 'black',
  shadowOffset: { height: 0, width: 0 },
  shadowOpacity: 0.3,
  shadowRadius: 3,
};

export const DateTimePickerContainer = styled.View`
  background: transparent;
  width: 100%;
`;
