import React, { forwardRef, useRef } from 'react';
import { Animated } from 'react-native';
import DateTimePicker from '@react-native-community/datetimepicker';
import { Modalize } from 'react-native-modalize';
import { DateTime } from 'luxon';
import { useTheme } from 'styled-components/native';
import { Stack } from '../layout/Stack';
import { Text } from '../text/text';
import {
  DateTimePickerContainer,
  shadow,
} from './scheduled-charging-modal.styles';
import { Button } from '../button/button';
import { Column } from '../layout/Column';
import { CustomSelector } from '../custom-selector/custom-selector';
import { Icon } from '../icon/icon';
import { Icons, IconSize } from '../icon/icon.types';
import { IScheduledTask } from '../../typings/vehicle-data.types';

const SNAP_POINT = 270;
interface IScheduledChargingModal {
  chargingTime: Date;
  onDateTimeSelected: (
    event: Event,
    date?: Date | undefined,
  ) => void;
  chargingFrequency: IScheduledTask['frequency'];
  onChargingFrequencySelected: (value: string) => void;
  onConfirm?: () => void;
  onCancel?: () => void;
  loading?: boolean;
  mode?: 'datetime' | 'time';
}

const OPTIONS = [
  {
    label: '',
    value: 'once',
    customIcon: (
      <Icon name={Icons.REPEAT} size={IconSize.xsmall} />
    ),
  },
  {
    label: '',
    value: 'daily',
    customIcon: (
      <Icon name={Icons.CALENDAR} size={IconSize.xsmall} />
    ),
  },
];

export const ScheduledChargingModal = forwardRef(
  (
    props: IScheduledChargingModal,
    ref: React.ForwardedRef<Modalize>,
  ) => {
    const {
      modalBackground,
      modalHandleColor,
      primaryTextColor,
    } = useTheme();
    const {
      chargingTime,
      chargingFrequency,
      onDateTimeSelected,
      onChargingFrequencySelected,
      onConfirm,
      onCancel,
      loading = false,
      mode = 'datetime',
    } = props;

    const animatedOpacity = useRef<Animated.Value>(
      new Animated.Value(0),
    );
    const initialFrequency = OPTIONS.findIndex(
      (freq) => chargingFrequency === freq.value,
    );

    return (
      <Modalize
        ref={ref}
        velocity={999}
        modalHeight={SNAP_POINT}
        snapPoint={SNAP_POINT}
        panGestureAnimatedValue={animatedOpacity.current}
        handlePosition="inside"
        closeOnOverlayTap={!loading}
        closeSnapPointStraightEnabled={false}
        // Weird behavior of keyboard offset
        avoidKeyboardLikeIOS={false}
        childrenStyle={{
          flex: 1,
          backgroundColor: modalBackground,
          borderTopLeftRadius: 10,
          borderTopRightRadius: 10,
          ...shadow,
        }}
        handleStyle={{
          backgroundColor: modalHandleColor,
        }}
        panGestureEnabled={!loading}
        withReactModal
        useNativeDriver
        onOverlayPress={onCancel}
        scrollViewProps={{ scrollEnabled: false }}>
        <Stack space="small">
          <Column hideGutter alignX="space-between">
            <Text variant="large" bold="bold">
              Start charging at
            </Text>
            <CustomSelector
              height="large"
              width={100}
              initial={initialFrequency}
              disabled={loading}
              options={OPTIONS}
              onSelect={onChargingFrequencySelected}
            />
          </Column>
          <DateTimePickerContainer
            pointerEvents={loading ? 'none' : 'auto'}>
            <DateTimePicker
              style={{ width: '100%', height: 100 }}
              textColor={primaryTextColor}
              testID="dateTimePicker"
              value={chargingTime}
              display="spinner"
              mode={mode}
              minimumDate={
                chargingFrequency === 'once'
                  ? new Date()
                  : undefined
              }
              maximumDate={DateTime.fromJSDate(new Date())
                .plus({ years: 1 })
                .toJSDate()}
              onChange={onDateTimeSelected}
            />
          </DateTimePickerContainer>
          <Button
            label="Confirm"
            onPress={onConfirm}
            loading={loading}
          />
        </Stack>
      </Modalize>
    );
  },
);
