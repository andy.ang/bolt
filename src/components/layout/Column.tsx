import React from 'react';
import { getPaddingFromHideGutter } from '../../lib/util';
import {
  Gutter,
  Padding,
  Space,
  SPACES,
} from '../../typings/layout.types';
import {
  ColumnLayout,
  ColumnContainer,
} from './layout.styles';

interface IColumn {
  children: React.ReactNode | React.ReactNode[];
  space?: Space;
  hideGutter?: Gutter | Gutter[];
  paddingSize?: Padding;
  alignX?:
    | 'left'
    | 'center'
    | 'right'
    | 'space-around'
    | 'space-between';
  flexChildren?: boolean;
  color?: string;
  wrap?: boolean;
}

export const Column = (props: IColumn) => {
  const {
    children,
    space = 'xsmall',
    hideGutter = false,
    alignX = 'center',
    flexChildren = false,
    color = 'transparent',
    wrap = false,
    paddingSize = 'md',
  } = props;

  const gap = SPACES[space];
  const justifyContent =
    alignX === 'center'
      ? 'center'
      : alignX === 'left'
      ? 'flex-start'
      : alignX === 'right'
      ? 'flex-end'
      : alignX;

  const padding = getPaddingFromHideGutter(
    hideGutter,
    paddingSize,
  );
  return (
    <ColumnLayout
      paddings={padding}
      style={{
        justifyContent,
        backgroundColor: color,
        flexWrap: wrap ? 'wrap' : 'nowrap',
      }}>
      {Array.isArray(children) ? (
        children.map((node, i) => (
          <ColumnContainer
            key={i}
            space={gap}
            wrap={wrap}
            style={{
              flex: flexChildren ? 1 : undefined,
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            {node}
          </ColumnContainer>
        ))
      ) : (
        <ColumnContainer
          space={gap}
          style={{
            flex: flexChildren ? 1 : undefined,
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          {children}
        </ColumnContainer>
      )}
    </ColumnLayout>
  );
};
