import React from 'react';
import { getPaddingFromHideGutter } from '../../lib/util';
import {
  Gutter,
  Padding,
  Space,
  SPACES,
} from '../../typings/layout.types';
import {
  StackLayout,
  StackContainer,
} from './layout.styles';

interface IStack {
  children: React.ReactNode | React.ReactNode[];
  space?: Space;
  hideGutter?: Gutter | Gutter[];
  paddingSize?: Padding;
  alignX?: 'left' | 'center' | 'right';
  alignY?: 'top' | 'center' | 'bottom';
  zIndexes?: 'equal' | 'top-down' | 'bottom-up' | number[];
}

export const Stack = (props: IStack) => {
  const {
    children,
    space = 'xsmall',
    hideGutter = false,
    alignX = 'left',
    alignY = 'top',
    zIndexes = 'top-down',
    paddingSize = 'md',
  } = props;
  const gap = SPACES[space];
  const alignItems =
    alignX === 'center'
      ? 'center'
      : alignX === 'left'
      ? 'flex-start'
      : 'flex-end';

  const justifyContent =
    alignY === 'center'
      ? 'center'
      : alignY === 'top'
      ? 'flex-start'
      : 'flex-end';

  const padding = getPaddingFromHideGutter(
    hideGutter,
    paddingSize,
  );

  return (
    <StackLayout
      paddings={padding}
      style={{
        justifyContent,
      }}>
      {Array.isArray(children) ? (
        children.map((node, i) => (
          <StackContainer
            key={i}
            space={gap}
            style={{
              alignItems,
              zIndex:
                zIndexes === 'top-down'
                  ? children.length - i
                  : zIndexes === 'bottom-up'
                  ? i
                  : zIndexes === 'equal'
                  ? 1
                  : zIndexes[i],
            }}>
            {node}
          </StackContainer>
        ))
      ) : (
        <StackContainer
          space={gap}
          style={{
            alignItems,
          }}>
          {children}
        </StackContainer>
      )}
    </StackLayout>
  );
};
