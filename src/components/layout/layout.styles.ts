import styled from 'styled-components/native';

interface IContainer {
  space: number;
  wrap?: boolean;
}

interface IStackLayout {
  paddings: string;
}

export const StackLayout = styled.View<IStackLayout>`
  background: transparent;
  padding: ${(props) => props.paddings};
  flex-direction: column;
  align-items: center;
  width: 100%;
`;

export const ColumnLayout = styled.View<IStackLayout>`
  background: transparent;
  padding: ${(props) => props.paddings};
  flex-direction: row;
  width: 100%;
`;

export const StackContainer = styled.View<IContainer>`
  margin-vertical: ${(props) => props.space}px;
  background: transparent;
  width: 100%;
  max-width: 600px;
`;

export const ColumnContainer = styled.View<IContainer>`
  background: transparent;
  padding-horizontal: ${(props) => props.space}px;
  padding-vertical: ${(props) =>
    props.wrap ? props.space : 0}px;
`;
