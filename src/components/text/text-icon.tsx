import React from 'react';
import { View } from 'react-native';
import { useTheme } from 'styled-components/native';
import { Icon } from '../icon/icon';
import { Icons } from '../icon/icon.types';
import { Text } from './text';
import { TextIconLayout } from './text.styles';

interface ITextIcon {
  children: string;
  icon?: Icons;
  iconColor?: string;
  textColor?: string;
  hideIcon?: boolean;
  iconSize?: number;
  fontSize?: 'xsmall' | 'small' | 'big' | 'medium';
  alignX?: 'left' | 'center' | 'right';
  alignY?: 'top' | 'center' | 'bottom';
  stretch?: boolean;
}

export const TextIcon = (props: ITextIcon) => {
  const {
    children,
    icon,
    hideIcon = false,
    iconSize = 30,
    fontSize,
    alignX = 'center',
    alignY = 'center',
    stretch = false,
    iconColor,
    textColor,
  } = props;
  const { primaryTextColor } = useTheme();
  const textStyle = stretch
    ? {
        width: '100%',
        color: textColor || primaryTextColor,
      }
    : {
        color: textColor || primaryTextColor,
      };
  const textContainerStyle = stretch
    ? {
        width: '90%',
        paddingLeft: 5,
      }
    : {
        paddingLeft: 5,
      };
  const justifyContent =
    alignX === 'center'
      ? 'center'
      : alignX === 'left'
      ? 'flex-start'
      : 'flex-end';

  const alignItems =
    alignY === 'center'
      ? 'center'
      : alignY === 'top'
      ? 'flex-start'
      : 'flex-end';

  return (
    <TextIconLayout style={{ justifyContent, alignItems }}>
      {icon && !hideIcon && (
        <Icon
          name={icon}
          color={iconColor || primaryTextColor}
          size={iconSize}
        />
      )}
      <View style={textContainerStyle}>
        <Text
          variant={fontSize}
          bold="bold"
          otherProps={{ style: textStyle }}>
          {children}
        </Text>
      </View>
    </TextIconLayout>
  );
};
