import React from 'react';
import { TextProps } from 'react-native';
import { Fonts } from '../../typings/theme.types';
import { StyledText } from './text.styles';

interface IText {
  variant?:
    | 'xxlarge'
    | 'xlarge'
    | 'large'
    | 'big'
    | 'medium'
    | 'small'
    | 'xsmall'
    | 'xxsmall';
  kind?: 'primary' | 'secondary';
  bold?:
    | 'extra-light'
    | 'light'
    | 'regular'
    | 'semi-bold'
    | 'bold'
    | 'extra-bold';
  children: string;
  numberOfLines?: number;
  ellipsizeMode?: 'tail';
  otherProps?: TextProps;
  alignX?: 'left' | 'center' | 'right';
}

const LINE_HEIGHT_SPACE = 5;
export const Text = (props: IText) => {
  const {
    variant = 'small',
    kind = 'primary',
    children,
    bold = 'regular',
    numberOfLines = 1,
    ellipsizeMode,
    otherProps,
    alignX = 'left',
  } = props;

  const fontFamily =
    bold === 'regular'
      ? Fonts.OXANIUM
      : bold === 'semi-bold'
      ? Fonts.OXANIUM_SEMIBOLD
      : bold === 'bold'
      ? Fonts.OXANIUM_BOLD
      : bold === 'extra-bold'
      ? Fonts.OXANIUM_EXTRABOLD
      : bold === 'light'
      ? Fonts.OXANIUM_LIGHT
      : bold === 'extra-light'
      ? Fonts.OXANIUM_EXTRALIGHT
      : Fonts.OXANIUM;

  const alignSelf =
    alignX === 'left'
      ? 'flex-start'
      : alignX === 'center'
      ? 'center'
      : 'flex-end';

  const getTextSize = (textVariant: IText['variant']) => {
    switch (textVariant) {
      case 'xxlarge':
        return 30;
      case 'xlarge':
        return 26;
      case 'large':
        return 22;
      case 'big':
        return 20;
      case 'medium':
        return 18;
      case 'small':
        return 16;
      case 'xsmall':
        return 14;
      case 'xxsmall':
        return 12;
      default:
        return 14;
    }
  };

  return (
    <StyledText
      fontSize={getTextSize(variant)}
      kind={kind}
      ellipsizeMode={ellipsizeMode}
      numberOfLines={numberOfLines}
      {...otherProps}
      style={{
        fontFamily,
        lineHeight:
          getTextSize(variant) + LINE_HEIGHT_SPACE,
        alignSelf,
        ...(otherProps?.style as object),
      }}>
      {children}
    </StyledText>
  );
};
