import styled from 'styled-components/native';

interface IStyledText {
  fontSize: number;
  kind: 'primary' | 'secondary';
}

export const StyledText = styled.Text<IStyledText>`
  font-family: Oxanium-Regular;
  font-size: ${(props) => props.fontSize}px;
  color: ${(props) =>
    props.kind === 'primary'
      ? props.theme.primaryTextColor
      : props.theme.secondaryTextColor};
`;

export const TextIconLayout = styled.View`
  flex-direction: row;
  align-items: center;
`;
