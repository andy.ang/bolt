import React from 'react';
import { CustomButton } from '../button/custom-button';
import { Icons, IconSize } from '../icon/icon.types';
import { Column } from '../layout/Column';
import { MediaControlLayout } from './media-controls.styles';

interface IMediaControlProps {
  onNextPressed: () => void;
  onPrevPressed: () => void;
  onPlayPausePressed: () => void;
}

export const MediaControls = ({
  onNextPressed,
  onPlayPausePressed,
  onPrevPressed,
}: IMediaControlProps) => {
  return (
    <MediaControlLayout>
      <Column
        flexChildren
        alignX="center"
        hideGutter="horizontal">
        <CustomButton
          icon={Icons.PLAY_BACK}
          onPress={onPrevPressed}
          inactiveColor="transparent"
        />
        <CustomButton
          icon={Icons.PLAY_PAUSE}
          iconSize={IconSize.big}
          onPress={onPlayPausePressed}
          inactiveColor="transparent"
        />
        <CustomButton
          icon={Icons.PLAY_FORWARD}
          onPress={onNextPressed}
          inactiveColor="transparent"
        />
      </Column>
    </MediaControlLayout>
  );
};
