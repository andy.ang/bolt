import styled from 'styled-components/native';
import { hex2rgba } from '../../lib/util';

export const StyledSlideableButton = styled.TouchableOpacity`
  background: ${(props) =>
    props.theme.controlItemInactiveButtonColor};
`;

export const SliderBackgroundView = styled.View`
  background-color: ${(props) =>
    hex2rgba(props.theme.sliderBackground, 0.5)};
  border-radius: 10px;
  width: 100%;
  flex-direction: row;
  align-items: flex-end;
  overflow: hidden;
`;

export const TextContainer = styled.View`
  width: 100%;
  height: 100%;
  position: absolute;
  align-items: center;
  justify-content: space-between;
`;
