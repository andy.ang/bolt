import React, { useEffect, useRef, useState } from 'react';
import ReactNativeHapticFeedback from 'react-native-haptic-feedback';
import {
  PanResponder,
  View,
  Animated,
  ActivityIndicator,
} from 'react-native';
import { useTheme } from 'styled-components/native';
import {
  SliderBackgroundView,
  TextContainer,
} from './slider.styles';
import { Text } from '../text/text';
import { TouchableWithoutFeedback } from 'react-native-gesture-handler';
import { Icon } from '../icon/icon';
import { Icons } from '../icon/icon.types';

interface ISlideButton {
  height: number;
  width: number;
  disabled?: boolean;
  loading?: boolean;
  slideLength?: number;
  position?: 'vertical' | 'horizontal';
  max: number;
  min: number;
  current: number;
  icon: Icons;
  onSlideReleased: (value: number) => void;
  suffix?: string;
}

export const Slider = ({
  height,
  width,
  position = 'vertical',
  disabled = false,
  loading = false,
  max,
  min,
  icon,
  current,
  suffix = '',
  onSlideReleased,
}: ISlideButton) => {
  const { sliderTintColor } = useTheme();
  const scale =
    position === 'horizontal'
      ? width / (max - min)
      : height / (max - min);
  const initialLength = Math.floor(scale * current);
  const longPressedRef = useRef(false);
  const disabledRef = useRef(disabled);
  const length = useRef(new Animated.Value(0));
  const minLength = min * scale;
  const updatedInitialLength = useRef(
    initialLength - minLength,
  );
  const [addedLength, setAddedLength] = useState<number>();
  const [value, setValue] = useState<number>(current);

  useEffect(() => {
    length.current.setValue(initialLength - minLength);
    updatedInitialLength.current =
      initialLength - minLength;
    setValue(current);
  }, [current, initialLength, minLength]);

  useEffect(() => {
    disabledRef.current = disabled;
  }, [disabled]);

  useEffect(() => {
    if (addedLength !== undefined) {
      const newLength = Math.round(
        (initialLength + addedLength) / scale,
      );
      if (newLength > max) {
        onSlideReleased(max);
      } else if (newLength < min) {
        onSlideReleased(min);
      } else {
        onSlideReleased(newLength);
      }
      setAddedLength(undefined);
    }
  }, [
    addedLength,
    initialLength,
    onSlideReleased,
    max,
    min,
    scale,
  ]);

  const panResponder = React.useRef(
    PanResponder.create({
      onStartShouldSetPanResponder: () => false,
      onStartShouldSetPanResponderCapture: () => false,
      onMoveShouldSetPanResponder: () => {
        if (disabledRef.current) {
          return false;
        }
        return longPressedRef.current;
      },
      onMoveShouldSetPanResponderCapture: () => {
        if (disabledRef.current) {
          return false;
        }
        return longPressedRef.current;
      },
      onPanResponderGrant: () => {
        // ReactNativeHapticFeedback.trigger('impactLight');
      },
      onPanResponderEnd: () => {
        ReactNativeHapticFeedback.trigger('impactLight');
        longPressedRef.current = false;
      },
      onPanResponderMove: (_, gestureState) => {
        const { dx, dy } = gestureState;
        const delta =
          position === 'horizontal' ? dx : -1 * dy;
        length.current.setValue(
          updatedInitialLength.current + delta,
        );
        const updatedValue = Math.round(
          (updatedInitialLength.current + delta) / scale +
            min,
        );
        setValue(
          updatedValue > max
            ? max
            : updatedValue < min
            ? min
            : updatedValue,
        );
      },
      onPanResponderTerminationRequest: () => true,
      onPanResponderRelease: (_, gestureState) => {
        const { dx, dy } = gestureState;
        const newPosition =
          position === 'horizontal' ? dx : -1 * dy;
        setAddedLength(Math.round(newPosition));
      },
      onShouldBlockNativeResponder: () => true,
    }),
  ).current;

  const sliderStyle = useRef(
    position === 'vertical'
      ? {
          width: '100%',
          height: length.current.interpolate({
            inputRange: [-height, height],
            outputRange: [-height, height],
            extrapolate: 'clamp',
          }),
        }
      : {
          height: '100%',
          width: length.current.interpolate({
            inputRange: [-width, width],
            outputRange: [-width, width],
            extrapolate: 'clamp',
          }),
        },
  );

  const animationRange =
    position === 'horizontal' ? width : height;

  return (
    <View {...panResponder.panHandlers}>
      <TouchableWithoutFeedback
        disabled={disabled}
        delayLongPress={200}
        onLongPress={() => {
          ReactNativeHapticFeedback.trigger('impactHeavy');
          longPressedRef.current = true;
        }}>
        <SliderBackgroundView
          style={{
            height,
            width,
          }}>
          <Animated.View
            style={{
              backgroundColor: sliderTintColor,
              ...sliderStyle.current,
            }}
          />
          <TextContainer
            style={{
              flexDirection:
                position === 'horizontal'
                  ? 'row-reverse'
                  : 'column',
              paddingBottom:
                position === 'horizontal' ? 0 : 10,
              paddingTop:
                position === 'horizontal' ? 0 : 20,
              paddingHorizontal:
                position === 'horizontal' ? 10 : 0,
            }}>
            <Animated.View
              style={{
                opacity: length.current.interpolate({
                  inputRange: [
                    -animationRange,
                    0,
                    animationRange,
                  ],
                  outputRange: [0, 0.1, 1],
                  extrapolate: 'clamp',
                }),
                transform: [
                  {
                    scale: length.current.interpolate({
                      inputRange: [
                        -animationRange,
                        0,
                        animationRange,
                      ],
                      outputRange: [0.5, 0.8, 1.2],
                      extrapolate: 'clamp',
                    }),
                  },
                ],
              }}>
              <Icon name={icon} size={30} />
            </Animated.View>
            {!loading ? (
              <Text alignX="center" variant="big">
                {`${value.toFixed(0)}${suffix}`}
              </Text>
            ) : (
              <ActivityIndicator />
            )}
          </TextContainer>
        </SliderBackgroundView>
      </TouchableWithoutFeedback>
    </View>
  );
};
