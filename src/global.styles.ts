import styled from 'styled-components/native';

export const CenteredView = styled.View`
  flex-direction: column;
  align-items: center;
  justify-content: center;
`;
