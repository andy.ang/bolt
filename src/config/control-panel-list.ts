import { Icons } from '../components';
import {
  Controls,
  IControlPanelItem,
} from '../typings/vehicle-command.types';

export const ControlPanelItems: IControlPanelItem[] = [
  {
    index: 0,
    accessor: 'vehicle_state.locked',
    disabled: '',
    disabledIf: true,
    active: {
      state: false,
      label: 'lock',
      command: Controls.LOCK,
      icon: Icons.UNLOCK,
    },
    inactive: {
      state: true,
      label: 'unlock',
      command: Controls.UNLOCK,
      icon: Icons.LOCK,
    },
  },
  {
    index: 1,
    accessor: '',
    disabled: '',
    disabledIf: true,
    active: {
      state: true,
      icon: Icons.FRUNK,
      label: 'frunk',
      command: Controls.OPEN_FRUNK,
    },
    inactive: {
      state: false,
      icon: Icons.FRUNK,
      label: 'frunk',
      command: Controls.OPEN_FRUNK,
    },
  },
  {
    index: 2,
    accessor: '',
    disabled: '',
    disabledIf: true,
    active: {
      state: true,
      icon: Icons.TRUNK,
      label: 'trunk',
      command: Controls.OPEN_TRUNK,
    },
    inactive: {
      state: true,
      icon: Icons.TRUNK,
      label: 'trunk',
      command: Controls.OPEN_TRUNK,
    },
  },
  {
    index: 3,
    accessor: 'climate_state.is_climate_on',
    disabled: '',
    disabledIf: true,
    active: {
      state: true,
      icon: Icons.FAN,
      label: 'stop ac',
      command: Controls.STOP_AC,
    },
    inactive: {
      state: true,
      icon: Icons.FAN_OFF,
      label: 'start ac',
      command: Controls.START_AC,
    },
  },
  {
    index: 4,
    accessor: '',
    disabled: '',
    disabledIf: true,
    active: {
      state: true,
      icon: Icons.SUNNY,
      label: 'wake',
      command: Controls.WAKE,
    },
    inactive: {
      state: false,
      icon: Icons.SUNNY,
      label: 'wake',
      command: Controls.WAKE,
    },
  },
  {
    index: 5,
    accessor: '',
    disabled: 'vehicle_state.remote_start_enabled',
    disabledIf: false,
    active: {
      state: true,
      icon: Icons.KEY,
      label: 'start',
      command: Controls.START_REMOTE_DRIVE,
    },
    inactive: {
      state: false,
      icon: Icons.KEY,
      label: 'start',
      command: Controls.START_REMOTE_DRIVE,
    },
  },
  {
    index: 6,
    accessor: '',
    disabled: '',
    disabledIf: true,
    active: {
      state: true,
      icon: Icons.FLASH_LIGHT,
      label: 'flash',
      command: Controls.FLASH_LIGHTS,
    },
    inactive: {
      state: false,
      icon: Icons.FLASH_LIGHT,
      label: 'flash',
      command: Controls.FLASH_LIGHTS,
    },
  },
  {
    index: 7,
    accessor: '',
    disabled: '',
    disabledIf: true,
    active: {
      state: true,
      icon: Icons.HORN,
      label: 'honk',
      command: Controls.HONK_HORN,
    },
    inactive: {
      state: false,
      icon: Icons.HORN,
      label: 'honk',
      command: Controls.HONK_HORN,
    },
  },
];
