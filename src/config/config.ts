import Config from 'react-native-config';

interface IConfig {
  TOM_TOM_API_KEY: string;
  TOM_TOM_API_URL: string;
  GOOGLE_API_KEY: string;
  GOOGLE_MAPS_URL: string;
  IPSTACK_API_KEY: string;
  IPSTACK_API_URL: string;
  FIREBASE_HOSTING_URL: string;
}

export const config: IConfig = {
  TOM_TOM_API_KEY: Config.TOM_TOM_API_KEY,
  TOM_TOM_API_URL: Config.TOM_TOM_API_URL,
  GOOGLE_API_KEY: Config.GOOGLE_API_KEY,
  GOOGLE_MAPS_URL: Config.GOOGLE_MAPS_URL,
  IPSTACK_API_KEY: Config.IPSTACK_API_KEY,
  IPSTACK_API_URL: Config.IPSTACK_API_URL,
  FIREBASE_HOSTING_URL: Config.FIREBASE_HOSTING_URL,
};
