import React from 'react';
import { View } from '../../components';
import { useAppContext } from '../../context/app/app-context';

export const SplashScreen = () => {
  const { mode } = useAppContext();
  return (
    <View testID="splash" mode={mode}>
      <></>
    </View>
  );
};
