import React, {
  useCallback,
  useEffect,
  useState,
} from 'react';
import {
  Chart,
  Header,
  Icons,
  Placeholder,
  Stack,
  TabPicker,
  Text,
  View,
} from '../../components';
import { useAppContext } from '../../context/app/app-context';
import { EventManager } from '../../lib/event';
import { getStatsData } from '../../services/vehicle/vehicle-data.service';
import { Error } from '../../typings/error.types';
import { IUserVehicleData } from '../../typings/vehicle-data.types';
import { TabPickerContainer } from './statistics.styles';

const PERIOD = [
  {
    label: '3 hours',
    value: 3,
  },
  {
    label: '6 hours',
    value: 6,
  },
  {
    label: '12 hours',
    value: 12,
  },
  {
    label: '24 hours',
    value: 24,
  },
];

export const StatisticsScreen = () => {
  const {
    mode,
    gradient,
    currentVehicle,
  } = useAppContext();
  const { id_s = '' } = currentVehicle || {};
  const [data, setData] = useState<IUserVehicleData[]>([]);
  const [refreshing, setRefreshing] = useState<boolean>(
    false,
  );
  const [periodInHours, setPeriodInHours] = useState<
    number
  >(3);
  const currentTimestamp = new Date().getTime();
  const xAxisRange: [number, number] = [
    currentTimestamp - periodInHours * 0.9 * 60 * 60 * 1000,
    currentTimestamp,
  ];

  const onPullToRefresh = useCallback(async () => {
    setRefreshing(true);
    try {
      if (id_s) {
        const dataset = await getStatsData(
          id_s,
          periodInHours,
        );
        setData(dataset);
        setRefreshing(false);
      }
    } catch (error) {
      EventManager.emitError({ body: Error.GENERIC });
      setRefreshing(false);
    }
  }, [id_s, periodInHours, setRefreshing]);

  useEffect(() => {
    onPullToRefresh();
  }, [periodInHours, onPullToRefresh]);

  return (
    <View
      testID="statistics"
      mode={mode}
      shadow
      gradient
      gradientStyle={gradient}
      type="scroll">
      <Header
        title="Your data"
        rightButton={{
          icon: Icons.REFRESH,
          animating: refreshing,
          onPress: () => {
            onPullToRefresh();
          },
        }}
      />
      <Stack hideGutter="vertical">
        <TabPickerContainer>
          <TabPicker
            values={PERIOD}
            onItemSelected={(item) => {
              setPeriodInHours(item.value as number);
            }}
          />
        </TabPickerContainer>
      </Stack>
      <Stack space="big" alignX="center">
        <Chart
          data={data}
          x="timestamp"
          y="battery_level"
          yRange={[0, 100]}
          xRange={xAxisRange}
          xLabel={`Last ${periodInHours} hours`}
          yLabel="Percentage %"
          title="Battery level (%)"
        />
        <Text
          alignX="left"
          numberOfLines={
            4
          }>{`In the last ${periodInHours} hours, you have used 10% battery. Based on your battery capacity, that is approximately 5kWh.`}</Text>
        <Chart
          data={data}
          x="timestamp"
          y="est_battery_range"
          yRange={[0, 300]}
          xRange={xAxisRange}
          xLabel={`Last ${periodInHours} hours`}
          yLabel="Range in miles"
          title="Estimated range (miles)"
        />
        <Text alignX="left" numberOfLines={5}>
          {
            'Based on your current battery level and estimated range, we project that your maximum range to be roughly 400km. '
          }
        </Text>
        <Chart
          data={data}
          x="timestamp"
          y="charge_rate"
          yRange={[0, 20]}
          xRange={xAxisRange}
          xLabel={`Last ${periodInHours} hours`}
          yLabel="A"
          title="Charging rate (A)"
        />
        <Text alignX="left" numberOfLines={5}>
          {`This is the summary in the last ${periodInHours} hours. `}
        </Text>
        <Placeholder height={150} color="black" />
      </Stack>
    </View>
  );
};
