import ReactNativeHapticFeedback from 'react-native-haptic-feedback';
import React, {
  useCallback,
  useEffect,
  useMemo,
  useRef,
  useState,
} from 'react';
import {
  Header,
  CarImage,
  Text,
  Stack,
  View,
  ControlPanel,
  Column,
  Icons,
  Slider,
  MediaControls,
  StatusBar,
  CustomButton,
  Carousel,
  SeatHeatersControl,
  Passcode,
  Switch,
} from '../../components/';
import { Box } from '../../components/box/box';
import { useAppContext } from '../../context/app/app-context';
import { useLoadingStateContext } from '../../context/loading-state/loading-state-context';
import {
  accessData,
  getCarNameFromCarType,
  getKMFromMiles,
  getMilesFromKM,
} from '../../lib/util';
import { sendCommand } from '../../services/vehicle/vehicle-command.service';
import { getCarImage } from '../../services/vehicle/vehicle-data.service';
import {
  Controls,
  ControlType,
  IControlPanelItem,
} from '../../typings/vehicle-command.types';
import { EventManager } from '../../lib/event';
import { Events } from '../../typings/event.types';
import { Error } from '../../typings/error.types';
import { useVehicleRefresh } from '../../hooks/vehicle-refresh';
import { ClimateControlContainer } from './home.styles';
import { StorageKeys } from '../../typings/storage.types';
import {
  deleteData,
  getMultipleData,
  storeData,
} from '../../lib/storage';

export const HomeScreen = () => {
  const {
    appStatus,
    mode,
    gradient,
    currentVehicle,
    controlPanelItems,
    location,
  } = useAppContext();
  const loading = useLoadingStateContext();
  const {
    setStateToInitiateLoading,
    setStateToCompleteLoading,
  } = loading;
  const {
    display_name = '',
    option_codes = '',
    vin = '',
    vehicle_config,
    vehicle_state,
    climate_state,
    drive_state,
    charge_state,
    id_s = '',
  } = currentVehicle || {};
  const { charging_state = 'Complete' } =
    charge_state || {};
  const { latitude = 0, longitude = 0 } = drive_state || {};
  const {
    car_type = '',
    rear_seat_heaters = 0,
    rhd = false,
    sun_roof_installed = null,
  } = vehicle_config || {};
  const {
    odometer = '',
    car_version = '',
    locked = false,
    is_user_present = false,
    rd_window = 0,
    rp_window = 0,
    fd_window = 0,
    fp_window = 0,
    valet_mode = false,
    sentry_mode = false,
    speed_limit_mode = {
      active: false,
      current_limit_mph: 0,
      min_limit_mph: 50,
      max_limit_mph: 90,
      pin_code_set: false,
    },
    sun_roof_state = 'closed',
    homelink_device_count = 0,
    homelink_nearby = false,
  } = vehicle_state || {};
  const {
    inside_temp = 0,
    outside_temp = 0,
    driver_temp_setting = 0,
    passenger_temp_setting = 0,
    max_avail_temp = 30,
    min_avail_temp = 15,
    is_climate_on = false,
    defrost_mode = 0,
    seat_heater_left = 0,
    seat_heater_right = 9,
    seat_heater_rear_center = 0,
    seat_heater_rear_left = 0,
    seat_heater_rear_right = 0,
  } = climate_state || {};
  const climateRef = useRef(null);
  const [refreshing, setRefreshing] = useVehicleRefresh(
    appStatus,
  );
  const [
    showValetPasscode,
    setShowValetPasscode,
  ] = useState<boolean>(false);
  const [
    showSpeedLimitPasscode,
    setShowSpeedLimitPasscode,
  ] = useState<boolean>(false);
  const [valetPin, setValetPin] = useState<string>('');
  const [speedLimitPin, setSpeedLimitPin] = useState<
    string
  >('');
  const windowOpened = useMemo(() => {
    return fd_window || fp_window || rd_window || rp_window;
  }, [fd_window, fp_window, rd_window, rp_window]);

  const onControlPanelItemPressed = useCallback(
    async (vehicleId: string, item: ControlType) => {
      try {
        if (vehicleId) {
          setStateToInitiateLoading(item.command);
          ReactNativeHapticFeedback.trigger('impactLight');
          await sendCommand(vehicleId, item.command, {
            latitude,
            longitude,
          });
        }
      } catch (error) {
        setStateToCompleteLoading(item.command);
        EventManager.emitError({
          body: Error.COMMAND_ERROR,
        });
      }
    },
    [
      latitude,
      longitude,
      setStateToInitiateLoading,
      setStateToCompleteLoading,
    ],
  );

  const renderControlPanelItem = useCallback(
    (item: IControlPanelItem) => {
      if (currentVehicle) {
        const {
          active,
          inactive,
          accessor,
          disabled,
          disabledIf,
        } = item;
        const currentState = accessData(
          accessor,
          currentVehicle,
        );
        const buttonState = accessData(
          disabled,
          currentVehicle,
        );
        const buttonDisabled = buttonState === disabledIf;
        let controlItem: ControlType =
          currentState === active.state ? active : inactive;
        return (
          <ControlPanel.Item
            key={controlItem.label}
            active={currentState === active.state}
            disabled={buttonDisabled}
            icon={controlItem.icon}
            label={controlItem.label}
            loading={loading[controlItem.command]}
            onPress={() =>
              onControlPanelItemPressed(id_s, controlItem)
            }
          />
        );
      }
    },
    [
      currentVehicle,
      id_s,
      onControlPanelItemPressed,
      loading,
    ],
  );

  useEffect(() => {
    getMultipleData([
      StorageKeys.VALET_PIN,
      StorageKeys.SPEED_LIMIT_PIN,
    ]).then((storedPins) => {
      if (storedPins && storedPins.length) {
        const [valet, speedLimit] = storedPins;
        if (valet) {
          setValetPin(valet);
        }
        if (speedLimit) {
          setSpeedLimitPin(speedLimit);
        }
      }
    });
  }, []);

  useEffect(() => {
    if (currentVehicle) {
      setRefreshing(false);
    }
    /* eslint-disable react-hooks/exhaustive-deps */
  }, [currentVehicle]);

  return (
    <View
      testID="home"
      mode={mode}
      shadow
      gradient
      gradientStyle={gradient}
      type="scroll">
      <Header
        title={display_name}
        rightButton={{
          icon: Icons.REFRESH,
          onPress: () => {
            ReactNativeHapticFeedback.trigger(
              'impactLight',
            );
            EventManager.emit(Events.REFRESH_VEHICLES);
          },
          animating: refreshing,
        }}
      />
      <Stack
        alignX="center"
        space="medium"
        zIndexes="equal">
        <CarImage
          url={getCarImage(option_codes)}
          size="normal"
        />
        <Stack hideGutter>
          <Text
            variant="big"
            numberOfLines={1}
            bold="regular"
            alignX="center">
            {getCarNameFromCarType(car_type) +
              ' \u2022 ' +
              getKMFromMiles(Number(odometer)) +
              'km' +
              ' \u2022 ' +
              inside_temp +
              ' \u00B0C'}
          </Text>
          <StatusBar
            locked={locked}
            hvacOn={is_climate_on}
            charging={charging_state === 'Charging'}
          />
        </Stack>
        <ControlPanel>
          {controlPanelItems.map((controlItem) =>
            renderControlPanelItem(controlItem),
          )}
        </ControlPanel>
      </Stack>
      <Stack hideGutter>
        <Column
          hideGutter="vertical"
          alignX="left"
          paddingSize="lg">
          <Text bold="bold">CLIMATE</Text>
          <Text>{`\u2022  Outside ${outside_temp} \u00B0C`}</Text>
        </Column>
      </Stack>
      <Carousel ref={climateRef}>
        <Stack hideGutter="vertical" alignX="center">
          <Box>
            <Column
              alignX="center"
              flexChildren
              hideGutter="horizontal">
              <ClimateControlContainer>
                <Column hideGutter wrap space="small">
                  <CustomButton
                    icon={Icons.CAR_SEAT_HEATERS}
                    width={156}
                    onPress={() => {
                      //@ts-ignore
                      climateRef.current.scrollToPage(1);
                    }}
                  />
                  <CustomButton
                    active={defrost_mode > 0}
                    icon={Icons.DEFROST}
                    loading={loading.TOGGLE_MAX_DEFROST}
                    disabled={loading.TOGGLE_MAX_DEFROST}
                    onPress={async () => {
                      setStateToInitiateLoading(
                        Controls.TOGGLE_MAX_DEFROST,
                      );
                      await sendCommand(
                        id_s,
                        Controls.TOGGLE_MAX_DEFROST,
                        {
                          maxDefrost: !defrost_mode,
                        },
                      );
                    }}
                  />
                  <CustomButton
                    active={!!windowOpened}
                    loading={loading.TOGGLE_WINDOW}
                    disabled={loading.TOGGLE_WINDOW}
                    icon={
                      windowOpened
                        ? Icons.WINDOW_CLOSED
                        : Icons.WINDOW_OPEN
                    }
                    onPress={async () => {
                      if (location) {
                        setStateToInitiateLoading(
                          Controls.TOGGLE_WINDOW,
                        );
                        await sendCommand(
                          id_s,
                          Controls.TOGGLE_WINDOW,
                          {
                            window: windowOpened
                              ? 'close'
                              : 'vent',
                            latitude: location.latitude,
                            longitude: location.longitude,
                          },
                        );
                      }
                    }}
                  />
                </Column>
              </ClimateControlContainer>
              <Slider
                height={158}
                width={140}
                icon={Icons.THERMOMETER}
                loading={loading.SET_TEMPS}
                disabled={loading.SET_TEMPS}
                position="vertical"
                min={min_avail_temp}
                max={max_avail_temp}
                current={driver_temp_setting}
                suffix="&deg;C"
                onSlideReleased={async (temp) => {
                  setStateToInitiateLoading(
                    Controls.SET_TEMPS,
                  );
                  await sendCommand(
                    id_s,
                    Controls.SET_TEMPS,
                    {
                      driverTemperature: temp,
                      passengerTemperature: passenger_temp_setting,
                    },
                  );
                }}
              />
            </Column>
          </Box>
        </Stack>
        <Stack hideGutter="vertical" alignX="center">
          <Box>
            <SeatHeatersControl
              disabled={!is_climate_on && !is_user_present}
              hasRearSeatHeaters={rear_seat_heaters > 0}
              frontLeft={seat_heater_left}
              frontRight={seat_heater_right}
              rearCenter={seat_heater_rear_center}
              rearLeft={seat_heater_rear_left}
              rearRight={seat_heater_rear_right}
              leftHandDrive={!rhd}
              onPress={async (seatId, heatLevel) => {
                await sendCommand(
                  id_s,
                  Controls.REMOTE_SEAT_HEATER_REQUEST,
                  {
                    seatId,
                    heatLevel,
                  },
                );
              }}
            />
          </Box>
        </Stack>
      </Carousel>
      <Stack hideGutter>
        <Column
          hideGutter="vertical"
          alignX="left"
          paddingSize="lg">
          <Text bold="bold">CONTROLS</Text>
        </Column>
      </Stack>
      <Stack
        alignX="center"
        hideGutter="top"
        zIndexes="equal">
        <Box size="medium">
          <Stack space="medium">
            <Switch
              label="Sentry mode"
              loading={loading.SET_SENTRY_MODE}
              value={sentry_mode}
              onValueChange={(val) => {
                setStateToInitiateLoading(
                  Controls.SET_SENTRY_MODE,
                );
                sendCommand(
                  id_s,
                  Controls.SET_SENTRY_MODE,
                  { sentryMode: val },
                );
              }}
            />
            <Switch
              label="Valet mode"
              value={valet_mode}
              loading={loading.SET_VALET_MODE}
              disabled={!valetPin}
              showButton={!loading.SET_VALET_MODE}
              buttonDisabled={loading.SET_VALET_MODE}
              buttonLabel={
                !valetPin ? 'Set PIN' : 'Clear PIN'
              }
              buttonOnPress={async () => {
                if (valetPin) {
                  deleteData(StorageKeys.VALET_PIN);
                  setStateToInitiateLoading(
                    Controls.SET_VALET_MODE,
                  );
                  await sendCommand(
                    id_s,
                    Controls.SET_VALET_MODE,
                    { valetMode: false, valetPin },
                  );
                  await sendCommand(
                    id_s,
                    Controls.RESET_VALET_PIN,
                  );
                  setValetPin('');
                } else {
                  setShowValetPasscode(true);
                }
              }}
              onValueChange={(val) => {
                setStateToInitiateLoading(
                  Controls.SET_VALET_MODE,
                );
                sendCommand(id_s, Controls.SET_VALET_MODE, {
                  valetMode: val,
                  valetPin,
                });
              }}
            />
            <Switch
              label="Speed limit mode"
              loading={loading.SET_SPEED_LIMIT_MODE}
              disabled={!speedLimitPin}
              value={speed_limit_mode.active}
              showButton={!loading.SET_SPEED_LIMIT_MODE}
              buttonDisabled={loading.SET_SPEED_LIMIT_MODE}
              buttonLabel={
                !speedLimitPin ? 'Set PIN' : 'Clear PIN'
              }
              buttonOnPress={async () => {
                if (speedLimitPin) {
                  deleteData(StorageKeys.SPEED_LIMIT_PIN);
                  setStateToInitiateLoading(
                    Controls.SET_SPEED_LIMIT_MODE,
                  );
                  await sendCommand(
                    id_s,
                    Controls.SET_SPEED_LIMIT_MODE,
                    {
                      speedLimitMode: false,
                      speedLimitPin,
                    },
                  );
                  await sendCommand(
                    id_s,
                    Controls.RESET_SPEED_LIMIT_PIN,
                  );
                  setSpeedLimitPin('');
                } else {
                  setShowSpeedLimitPasscode(true);
                }
              }}
              onValueChange={(val) => {
                setStateToInitiateLoading(
                  Controls.SET_SPEED_LIMIT_MODE,
                );
                sendCommand(
                  id_s,
                  Controls.SET_SPEED_LIMIT_MODE,
                  {
                    speedLimitMode: val,
                    speedLimitPin,
                  },
                );
              }}
            />
          </Stack>
        </Box>
        <Box size="small">
          <Stack alignX="center" space="small">
            <Text alignX="center">
              Set your speed limit
            </Text>
            <Slider
              height={60}
              width={300}
              loading={loading.SET_SPEED_LIMIT}
              disabled={
                loading.SET_SPEED_LIMIT ||
                speed_limit_mode.active
              }
              icon={Icons.SPEEDOMETER}
              position="horizontal"
              suffix=" KM/H"
              min={getKMFromMiles(
                speed_limit_mode.min_limit_mph,
              )}
              max={getKMFromMiles(
                speed_limit_mode.max_limit_mph,
              )}
              current={getKMFromMiles(
                speed_limit_mode.current_limit_mph,
              )}
              onSlideReleased={(limit_kmh) => {
                setStateToInitiateLoading(
                  Controls.SET_SPEED_LIMIT,
                );
                sendCommand(
                  id_s,
                  Controls.SET_SPEED_LIMIT,
                  {
                    speedMph: getMilesFromKM(limit_kmh),
                  },
                );
              }}
            />
          </Stack>
        </Box>
        <Box size="xsmall">
          <Column alignX="space-around" space="small">
            <CustomButton
              icon={Icons.HOME}
              label="homelink"
              loading={loading.TRIGGER_HOME_LINK}
              disabled={
                loading.TRIGGER_HOME_LINK ||
                homelink_device_count > 0 ||
                !homelink_nearby
              }
              onPress={() => {
                setStateToInitiateLoading(
                  Controls.TRIGGER_HOME_LINK,
                );
                sendCommand(
                  id_s,
                  Controls.TRIGGER_HOME_LINK,
                  {
                    latitude,
                    longitude,
                  },
                );
              }}
            />
            <CustomButton
              active={sun_roof_state === 'open'}
              icon={Icons.SUNGLASSES}
              label="sunroof"
              loading={loading.TOGGLE_SUNROOF}
              disabled={
                loading.TOGGLE_SUNROOF ||
                !sun_roof_installed
              }
              onPress={() => {
                setStateToInitiateLoading(
                  Controls.TOGGLE_SUNROOF,
                );
                sendCommand(id_s, Controls.TOGGLE_SUNROOF, {
                  sunroof:
                    sun_roof_state === 'closed'
                      ? 'vent'
                      : 'close',
                });
              }}
            />
            <CustomButton
              icon={Icons.ADD}
              label="sunroof"
              disabled
            />
            <CustomButton
              icon={Icons.ADD}
              label="sunroof"
              disabled
            />
          </Column>
        </Box>
      </Stack>
      <Stack hideGutter>
        <Column
          hideGutter="vertical"
          alignX="left"
          paddingSize="lg">
          <Text bold="bold">MEDIA</Text>
        </Column>
      </Stack>
      <Stack
        alignX="center"
        hideGutter="top"
        zIndexes="equal">
        <Box size="xsmall">
          <Stack hideGutter="vertical" alignY="center">
            <MediaControls
              onNextPressed={() =>
                sendCommand(id_s, Controls.PLAY_NEXT_TRACK)
              }
              onPrevPressed={() =>
                sendCommand(
                  id_s,
                  Controls.PLAY_PREVIOUS_TRACK,
                )
              }
              onPlayPausePressed={() =>
                sendCommand(
                  id_s,
                  Controls.TOGGLE_MEDIA_PLAYBACK,
                )
              }
            />
          </Stack>
        </Box>
        <Box size="xxsmall">
          <Column hideGutter flexChildren space="none">
            <CustomButton
              height={50}
              width="full-width"
              icon={Icons.REMOVE}
              inactiveColor="transparent"
              onPress={() =>
                sendCommand(id_s, Controls.TURN_VOLUME_DOWN)
              }
            />
            <CustomButton
              height={50}
              width="full-width"
              icon={Icons.ADD}
              inactiveColor="transparent"
              onPress={() =>
                sendCommand(id_s, Controls.TURN_VOLUME_UP)
              }
            />
          </Column>
        </Box>
      </Stack>
      <Stack hideGutter>
        <Column
          hideGutter="vertical"
          alignX="left"
          paddingSize="lg">
          <Text bold="bold">INFO</Text>
        </Column>
      </Stack>
      <Stack hideGutter="top" alignX="center">
        <Box size="xsmall">
          <Stack alignX="center" alignY="center">
            <Text alignX="center">{`VIN: ${vin}`}</Text>
            <Text alignX="center">{`Version: ${car_version}`}</Text>
          </Stack>
        </Box>
      </Stack>
      <Passcode
        mode={mode}
        visible={
          showValetPasscode || showSpeedLimitPasscode
        }
        onCancel={() => setShowValetPasscode(false)}
        onSuccess={async (passcode) => {
          if (showValetPasscode) {
            setShowValetPasscode(false);
            setStateToInitiateLoading(
              Controls.SET_VALET_MODE,
            );
            storeData(StorageKeys.VALET_PIN, {
              pin: passcode,
            });
            setValetPin(passcode);
            await sendCommand(
              id_s,
              Controls.SET_VALET_MODE,
              {
                valetMode: true,
                valetPin: passcode,
              },
            );
          } else {
            setShowSpeedLimitPasscode(false);
            setStateToInitiateLoading(
              Controls.SET_SPEED_LIMIT_MODE,
            );
            storeData(StorageKeys.SPEED_LIMIT_PIN, {
              pin: passcode,
            });
            setSpeedLimitPin(passcode);
            await sendCommand(
              id_s,
              Controls.SET_SPEED_LIMIT_MODE,
              {
                speedLimitMode: true,
                speedLimitPin: passcode,
              },
            );
          }
        }}
      />
    </View>
  );
};
