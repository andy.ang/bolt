import { DrawerContentComponentProps } from '@react-navigation/drawer';
import React from 'react';
import auth from '@react-native-firebase/auth';
import Animated from 'react-native-reanimated';
import { Screens } from '../../typings/screens.types';
import { Icons, List } from '../../components';
import { useAppContext } from '../../context/app/app-context';
import { Colors } from '../../typings/theme.types';
import { DrawerScreenView } from './drawer.styles';
import { deleteSecureData } from '../../lib/storage';
import { SecureStorageKeys } from '../../typings/storage.types';
import { Events } from '../../typings/event.types';
import { EventManager } from '../../lib/event';

export const DrawerScreen = (
  props: DrawerContentComponentProps,
) => {
  const { progress, navigation } = props;
  const { mode } = useAppContext();
  const iconColor =
    mode === 'dark'
      ? Colors.BABY_POWDER
      : Colors.PURPLE_NAVY;
  return (
    <Animated.View
      style={{
        flex: 1,
        opacity: progress,
      }}>
      <DrawerScreenView>
        <List
          label="Settings"
          icon={Icons.SETTINGS}
          iconColor={iconColor}
          onPress={() => {
            navigation.closeDrawer();
            navigation.navigate(Screens.SETTINGS);
          }}
        />
        <List
          label="Account"
          icon={Icons.CONTACT}
          iconColor={iconColor}
          onPress={() => {}}
        />
        <List
          label="Sign out"
          icon={Icons.SIGN_OUT}
          iconColor={iconColor}
          onPress={async () => {
            await auth().signOut();
            await deleteSecureData(
              SecureStorageKeys.ACCESS_TOKEN,
            );
            await deleteSecureData(
              SecureStorageKeys.PASSWORD,
            );
            EventManager.emit(Events.ACCESS_TOKEN_UPDATED);
          }}
        />
      </DrawerScreenView>
    </Animated.View>
  );
};
