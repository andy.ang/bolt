import styled from 'styled-components/native';
import { CenteredView } from '../../global.styles';

export const DrawerScreenView = styled(CenteredView)`
  background: ${(props) => props.theme.secondaryBackground};
  height: 100%;
`;
