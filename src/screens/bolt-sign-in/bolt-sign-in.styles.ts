import styled from 'styled-components/native';

export const TextContainer = styled.View`
  justify-content: center;
  align-items: center;
  padding-bottom: 5px;
`;

export const TextInputContainer = styled.View`
  width: 300px;
  align-self: center;
`;

export const ButtonContainer = styled.View`
  width: 300px;
  align-self: center;
`;
