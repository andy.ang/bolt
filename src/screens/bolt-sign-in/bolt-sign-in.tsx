import React, { useState } from 'react';
import auth from '@react-native-firebase/auth';
import {
  Button,
  CarImage,
  Icons,
  Stack,
  Text,
  TextIcon,
  TextInput,
  TextLink,
  TimingFade,
  View,
} from '../../components';
import { useAppContext } from '../../context/app/app-context';
import { storeSecureData } from '../../lib/storage';
import { SecureStorageKeys } from '../../typings/storage.types';
import { signInToBolt } from '../../services/bolt/bolt.service';
import { Gradient } from '../../typings/theme.types';
import {
  ButtonContainer,
  TextContainer,
  TextInputContainer,
} from './bolt-sign-in.styles';
import { Error } from '../../typings/error.types';
import { Events } from '../../typings/event.types';
import { config } from '../../config/config';
import { Model } from '../../components/car-image/car-image.types';
import {
  getVehicles,
  registerVehicles,
} from '../../services/vehicle/vehicle-data.service';
import { EventManager } from '../../lib/event';
import {
  getUser,
  registerUser,
  updateUser,
} from '../../services/users/users.service';
import { retrieveUserDetails } from '../../services/auth/auth.service';

export const BoltSignInScreen = () => {
  const {
    mode,
    setVehicles,
    setCurrentVehicle,
  } = useAppContext();
  const [email, setEmail] = useState<string>('');
  const [password, setPassword] = useState<string>('');
  const [loading, setLoading] = useState<boolean>(false);

  const onSignInButtonPressed = async () => {
    try {
      setLoading(true);
      const response = await signInToBolt(email, password);
      if (!response) {
        setLoading(false);
        EventManager.emitError({
          body: Error.INVALID_CREDS,
        });
        return;
      }
      await storeSecureData(
        SecureStorageKeys.ACCESS_TOKEN,
        response,
      );
      await storeSecureData(
        SecureStorageKeys.PASSWORD,
        password,
      );

      const authUser = retrieveUserDetails();

      if (authUser) {
        const user = await getUser(authUser.uid);
        if (user) {
          await updateUser({
            id: user.id,
            phoneNumber: user.phoneNumber,
            accessToken: response.access_token,
            paid: user.paid,
          });
        } else {
          await registerUser({
            id: authUser.uid,
            phoneNumber: authUser.phoneNumber || '',
            accessToken: response.access_token,
            paid: false,
          });
        }
        const vehicles = await getVehicles();
        if (vehicles) {
          setVehicles(vehicles);
          setCurrentVehicle(vehicles[0]);
          await registerVehicles(authUser.uid, vehicles);
          EventManager.emit(Events.ACCESS_TOKEN_UPDATED);
        }
      } else {
        EventManager.emitError({
          body: Error.USER_ERROR,
        });
      }
      setLoading(false);
    } catch (error) {
      setLoading(false);
      EventManager.emitError({
        body: Error.SERVER_ERROR,
      });
    }
  };

  return (
    <View
      testID="bolt-sign-in"
      mode={mode}
      gradient
      gradientStyle={Gradient.GRADIENT_3}
      gradientStopPoint={0}>
      <TextContainer>
        <TextIcon
          fontSize="big"
          icon={Icons.FLASH_FIILED}
          stretch={false}>
          Tron.
        </TextIcon>
      </TextContainer>
      <Stack
        space="medium"
        alignX="center"
        hideGutter
        alignY="center">
        <></>
        <></>
        <></>
        <></>
        <TimingFade duration={2000}>
          <CarImage
            url={`${config.FIREBASE_HOSTING_URL}/tesla-roadster.png`}
            model={Model.TESLA_ROADSTER}
            size="small"
          />
        </TimingFade>
        <Stack alignX="center">
          <TextContainer>
            <Text numberOfLines={1} alignX="center">
              Please sign in
            </Text>
            <Text numberOfLines={1} alignX="center">
              with your Tesla account
            </Text>
          </TextContainer>
          <TextInputContainer>
            <TextInput
              value={email}
              placeholder="username / email"
              onChangeText={setEmail}
            />
          </TextInputContainer>
          <TextInputContainer>
            <TextInput
              value={password}
              placeholder="password"
              maskInput
              onChangeText={setPassword}
            />
          </TextInputContainer>
        </Stack>
        <Stack
          alignX="center"
          space="medium"
          hideGutter="bottom">
          <ButtonContainer>
            <Button
              label="Sign In"
              onPress={onSignInButtonPressed}
              loading={loading}
            />
          </ButtonContainer>
          <TextLink
            message="Sign in with a different phone number"
            disabled={loading}
            onPress={async () => {
              await auth().signOut();
            }}
          />
        </Stack>
      </Stack>
    </View>
  );
};
