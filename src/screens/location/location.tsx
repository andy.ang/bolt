import { StackScreenProps } from '@react-navigation/stack';
import { Dimensions } from 'react-native';
import React, { useEffect, useState, useRef } from 'react';
import {
  AddressInput,
  Map,
  TravelModal,
  View,
} from '../../components/';
import { useAppContext } from '../../context/app/app-context';
import { AddressInputLayout } from './location.styles';
import MapView, { LatLng } from 'react-native-maps';
import { Modalize } from 'react-native-modalize';
import {
  getAddressFromCoordinates,
  getRoutes,
} from '../../services/map/map.service';
import { ISummary } from '../../services/map/map.types';
import { determineBatteryLevel } from '../../lib/util';
import { Error } from '../../typings/error.types';
import { EventManager } from '../../lib/event';

const { width, height } = Dimensions.get('screen');

const DEFAULT_LOCATION: LatLng = {
  latitude: 37.78825,
  longitude: -122.4324,
};

const DEFAULT_PADDING = {
  top: 200,
  right: width / 8,
  bottom: height / 2,
  left: width / 8,
};

export const LocationScreen = ({}: StackScreenProps<{}>) => {
  const mapRef = useRef<MapView>(null);
  const modalRef = useRef<Modalize>(null);
  const {
    mode,
    gradient,
    // location,
    currentVehicle,
  } = useAppContext();
  const {
    charge_state: {
      battery_level = 100,
      est_battery_range = 0,
    } = {},
    drive_state: {
      latitude: vehicleLatitude = DEFAULT_LOCATION.latitude,
      longitude: vehicleLongitude = DEFAULT_LOCATION.longitude,
    } = {},
  } = currentVehicle || {};
  // const userLocation = location || DEFAULT_LOCATION;
  const vehicleLocation = {
    latitude: vehicleLatitude,
    longitude: vehicleLongitude,
  };
  const [destination, setDestination] = useState<LatLng>();
  const [batteryLevel, setBatteryLevel] = useState<number>(
    battery_level,
  );
  const [currentAddress, setCurrentAddress] = useState<
    string
  >('');
  const [destAddress, setDestAddress] = useState<string>(
    '',
  );
  const [calculatedRoutes, setCalculatedRoutes] = useState<
    LatLng[]
  >([]);
  const [routeSummary, setRouteSummary] = useState<
    ISummary
  >();

  useEffect(() => {
    if (vehicleLatitude && vehicleLongitude) {
      refocus({
        latitude: vehicleLatitude,
        longitude: vehicleLongitude,
      });
      getAddressFromCoordinates({
        latitude: vehicleLatitude,
        longitude: vehicleLongitude,
      }).then((formattedAddress) => {
        if (formattedAddress) {
          setCurrentAddress(formattedAddress);
        }
      });
    }
  }, [vehicleLatitude, vehicleLongitude]);

  useEffect(() => {
    if (
      destination &&
      vehicleLatitude &&
      vehicleLongitude
    ) {
      getRoutes({
        origin: {
          latitude: vehicleLatitude,
          longitude: vehicleLongitude,
        },
        destination,
        params: {
          currentChargeInkWh: 43.2,
          maxChargeInkWh: 54,
        },
      }).then((calculated) => {
        if (calculated) {
          const { routes } = calculated;
          const { legs, summary } = routes[0];
          const { points } = legs[0];
          const {
            lengthInMeters,
            batteryConsumptionInkWh,
          } = summary;
          setCalculatedRoutes(points);
          setRouteSummary(summary);
          setBatteryLevel(
            determineBatteryLevel(
              batteryConsumptionInkWh,
              54,
              lengthInMeters / 1000,
              battery_level,
              est_battery_range,
            ),
          );
          refocus([
            {
              latitude: vehicleLatitude,
              longitude: vehicleLongitude,
            },
            destination,
          ]);
          modalRef.current?.open('top');
        } else {
          // Display no route modal
          EventManager.emitError({
            body: Error.NO_ROUTE,
          });
        }
      });
    }
  }, [
    destination,
    vehicleLatitude,
    vehicleLongitude,
    battery_level,
    est_battery_range,
  ]);

  const refocus = (coordinate: LatLng | LatLng[]) => {
    if (Array.isArray(coordinate)) {
      mapRef.current?.fitToCoordinates(coordinate, {
        edgePadding: DEFAULT_PADDING,
        animated: true,
      });
    } else {
      mapRef.current?.animateCamera({
        center: {
          latitude: coordinate.latitude,
          longitude: coordinate.longitude,
        },
        pitch: 10,
        heading: 10,
        altitude: 100,
        zoom: 16,
      });
    }
  };

  return (
    <View
      avoidKeyboard={false}
      testID="location"
      mode={mode}
      gradient
      gradientStyle={gradient}
      topInset={false}
      type="normal">
      <Map
        ref={mapRef}
        locations={
          destination
            ? [vehicleLocation, destination]
            : [vehicleLocation]
        }
        darkMode={mode === 'dark'}
        routes={calculatedRoutes}
        onMarkerPressed={() => refocus(vehicleLocation)}
        onMapPressed={() => {
          if (destination) {
            setDestination(undefined);
            setDestAddress('');
            setCalculatedRoutes([]);
            setRouteSummary(undefined);
            setBatteryLevel(battery_level);
            modalRef.current?.close('alwaysOpen');
            refocus(vehicleLocation);
          }
        }}
      />
      <AddressInputLayout>
        <AddressInput
          location={vehicleLocation}
          onResultSelected={(data, details) => {
            if (details) {
              setDestination({
                latitude: details.geometry.location.lat,
                longitude: details.geometry.location.lng,
              });
              setDestAddress(details.formatted_address);
            }
          }}
        />
      </AddressInputLayout>
      <TravelModal
        ref={modalRef}
        batteryLevel={batteryLevel}
        origin={currentAddress}
        destination={destAddress}
        details={routeSummary}
        onSend={() => {}}
      />
    </View>
  );
};
