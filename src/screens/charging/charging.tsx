import { StackScreenProps } from '@react-navigation/stack';
import React, { useEffect, useRef, useState } from 'react';
import { DateTime } from 'luxon';
import {
  Box,
  Button,
  ChargingRing,
  Column,
  Header,
  Icons,
  ScheduledChargingModal,
  Slider,
  Stack,
  Text,
  View,
} from '../../components/';
import { useAppContext } from '../../context/app/app-context';
import { useLoadingStateContext } from '../../context/loading-state/loading-state-context';
import { useVehicleRefresh } from '../../hooks/vehicle-refresh';
import { EventManager } from '../../lib/event';
import { sendCommand } from '../../services/vehicle/vehicle-command.service';
import { Events } from '../../typings/event.types';
import { Controls } from '../../typings/vehicle-command.types';
import {
  IScheduledTask,
  IVehicleSummary,
} from '../../typings/vehicle-data.types';
import { Modalize } from 'react-native-modalize';
import { getVehicleSummary } from '../../services/vehicle/vehicle-data.service';

export const ChargingScreen = ({}: StackScreenProps<{}>) => {
  const {
    appStatus,
    mode,
    gradient,
    currentVehicle,
  } = useAppContext();
  const modalRef = useRef<Modalize>(null);
  const { charge_state, id_s = '' } = currentVehicle || {};
  const {
    battery_level = 0,
    minutes_to_full_charge = 0,
    charging_state = 'Complete',
    charge_limit_soc = 0,
  } = charge_state || {};
  const loading = useLoadingStateContext();
  const {
    setStateToInitiateLoading,
    setStateToCompleteLoading,
  } = loading;
  const [scheduledDate, setScheduledDate] = useState(
    new Date(),
  );
  const [chargingTask, setChargingTask] = useState<
    IScheduledTask
  >();
  const [
    chargingFrequency,
    setChargingFrequency,
  ] = useState<IScheduledTask['frequency']>('once');
  const [refreshing, setRefreshing] = useVehicleRefresh(
    appStatus,
  );
  const chargingTitle = chargingTask
    ? chargingTask.frequency === 'once'
      ? DateTime.fromMillis(
          chargingTask.timeInSeconds * 1000,
        ).toFormat('fff (ccc)')
      : `Every day at ${DateTime.fromMillis(
          chargingTask.timeInSeconds * 1000,
        ).toFormat('T')}`
    : '';

  useEffect(() => {
    if (currentVehicle) {
      setRefreshing(false);
    }
  }, [currentVehicle, setRefreshing]);

  useEffect(() => {
    if (currentVehicle) {
      getVehicleSummary(currentVehicle.id_s).then(
        (summary) => {
          const { scheduled } = summary as IVehicleSummary;
          if (scheduled) {
            const found = scheduled.find(
              (task) => task.type === 'charging',
            );
            setChargingTask(found);
          }
        },
      );
    }
  }, [currentVehicle]);

  return (
    <View
      testID="charging"
      mode={mode}
      shadow
      gradient
      gradientStyle={gradient}
      type="scroll">
      <Header
        title={'Charging'}
        rightButton={{
          icon: Icons.REFRESH,
          onPress: () => {
            EventManager.emit(Events.REFRESH_VEHICLES);
          },
          animating: refreshing,
        }}
      />
      <Stack alignX="center">
        <ChargingRing
          batteryLevel={battery_level}
          ringSize={300}
          charging={charging_state === 'Charging'}
          minsToFull={minutes_to_full_charge}
        />
        <Text alignX="center">Set your charging limit</Text>
        <Slider
          height={60}
          width={300}
          loading={loading.SET_CHARGE_LIMIT}
          disabled={loading.SET_CHARGE_LIMIT}
          icon={Icons.FLASH_FIILED}
          position="horizontal"
          min={0}
          max={100}
          current={charge_limit_soc}
          suffix="%"
          onSlideReleased={async (limit) => {
            try {
              setStateToInitiateLoading(
                Controls.SET_CHARGE_LIMIT,
              );
              await sendCommand(
                id_s,
                Controls.SET_CHARGE_LIMIT,
                {
                  chargeLimit: limit,
                },
              );
            } catch (error) {
              setStateToCompleteLoading(
                Controls.SET_CHARGE_LIMIT,
              );
              console.error(error);
            }
          }}
        />
      </Stack>
      <Stack hideGutter>
        <Column
          hideGutter="vertical"
          alignX="left"
          paddingSize="lg">
          <Text bold="bold">SCHEDULED</Text>
        </Column>
      </Stack>
      <Stack
        alignX="center"
        hideGutter="top"
        zIndexes="equal">
        <Box size="small">
          <Stack space="small" alignY="bottom">
            {chargingTask ? (
              <Stack hideGutter space="xxsmall">
                <Text numberOfLines={1}>
                  Charging scheduled at
                </Text>
                <Text
                  numberOfLines={2}
                  variant="xsmall"
                  bold="bold">
                  {chargingTitle}
                </Text>
              </Stack>
            ) : (
              <Stack hideGutter space="none">
                <Text>No scheduled charging</Text>
                <Text> </Text>
              </Stack>
            )}
            <Button
              label={
                chargingTask
                  ? 'Cancel schedule'
                  : 'Schedule'
              }
              icon={
                chargingTask
                  ? Icons.CANCEL_TIMER
                  : Icons.CLOCK
              }
              disabled={!scheduledDate}
              loading={
                loading.SCHEDULE_CHARGING ||
                loading.DELETE_SCHEDULED_CHARGING
              }
              onPress={async () => {
                try {
                  if (chargingTask) {
                    setStateToInitiateLoading(
                      Controls.DELETE_SCHEDULED_CHARGING,
                    );
                    await sendCommand(
                      id_s,
                      Controls.DELETE_SCHEDULED_CHARGING,
                      {
                        chargingTask,
                      },
                    );
                  } else {
                    modalRef.current?.open();
                  }
                } catch (error) {
                  setStateToCompleteLoading(
                    Controls.DELETE_SCHEDULED_CHARGING,
                  );
                  console.error(error);
                }
              }}
            />
          </Stack>
        </Box>
      </Stack>
      <Stack hideGutter>
        <Column
          hideGutter="vertical"
          alignX="left"
          paddingSize="lg">
          <Text bold="bold">CHARGING SITES</Text>
        </Column>
      </Stack>
      <Stack
        alignX="center"
        hideGutter="top"
        space="small"
        zIndexes="equal">
        <Box size="xsmall">
          <Stack>
            <Text>CHARGING STATION 1</Text>
          </Stack>
        </Box>
        <Box size="xsmall">
          <Stack>
            <Text>CHARGING STATION 2</Text>
          </Stack>
        </Box>
        <Box size="xsmall">
          <Stack>
            <Text>CHARGING STATION 3</Text>
          </Stack>
        </Box>
      </Stack>
      <ScheduledChargingModal
        ref={modalRef}
        chargingTime={scheduledDate}
        mode={
          chargingFrequency === 'daily'
            ? 'time'
            : 'datetime'
        }
        chargingFrequency={chargingFrequency}
        //@ts-ignore
        onChargingFrequencySelected={setChargingFrequency}
        onDateTimeSelected={(event, selectedDate) => {
          if (selectedDate) {
            setScheduledDate(selectedDate);
          }
        }}
        onCancel={() => setScheduledDate(new Date())}
        loading={loading.SCHEDULE_CHARGING}
        onConfirm={async () => {
          try {
            setStateToInitiateLoading(
              Controls.SCHEDULE_CHARGING,
            );
            await sendCommand(id_s, Controls.STOP_CHARGING);
            const task = await sendCommand(
              id_s,
              Controls.SCHEDULE_CHARGING,
              {
                chargingTime:
                  scheduledDate.getTime() / 1000,
                chargingFrequency,
              },
            );
            setChargingTask(task);
            setStateToCompleteLoading(
              Controls.SCHEDULE_CHARGING,
            );
            modalRef.current?.close();
          } catch (error) {
            setStateToCompleteLoading(
              Controls.SCHEDULE_CHARGING,
            );
            console.error(error);
          }
        }}
      />
    </View>
  );
};
