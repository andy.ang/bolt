import { useNavigation } from '@react-navigation/native';
import ReactNativeHapticFeedback from 'react-native-haptic-feedback';
import React, { useState } from 'react';
import {
  ControlPanelEditable,
  Header,
  Icons,
  Placeholder,
  Stack,
  Text,
  View,
} from '../../components';
import { ControlPanelView } from './settings.styles';
import { useAppContext } from '../../context/app/app-context';
import { IControlPanelItem } from '../../typings/vehicle-command.types';
import { Dimensions } from 'react-native';

const { width } = Dimensions.get('screen');

export const SettingsControlScreen = () => {
  const {
    mode,
    controlPanelItems,
    setControlPanelItems,
  } = useAppContext();
  const navigation = useNavigation();
  const [scrollEnabled, setScrollEnabled] = useState<
    boolean
  >(true);
  const [editMode, setEditMode] = useState<boolean>(false);
  const [controls, setControls] = useState<
    IControlPanelItem[]
  >(controlPanelItems);

  return (
    <View
      testID="settings-control"
      mode={mode}
      scrollEnabled={scrollEnabled}
      type="scroll">
      <Header
        title="Control Panel"
        leftButton={{
          icon: Icons.BACK,
          label: 'back',
          onPress: () => {
            navigation.goBack();
          },
        }}
        rightButton={{
          label: 'save',
          disabled: !editMode,
          onPress: () => {
            setControlPanelItems(controls);
            setEditMode(false);
            navigation.goBack();
          },
        }}
      />
      <Stack>
        <Text alignX="left">
          Please press and hold to enter edit mode
        </Text>
        <Text numberOfLines={5} alignX="left">
          Drag them to configure how you want your control
          panel to look on your home screen
        </Text>
      </Stack>
      <Placeholder height={50} />
      <ControlPanelView>
        <ControlPanelEditable<IControlPanelItem>
          data={controls}
          grid={4}
          width={width}
          onDragStart={() => {
            ReactNativeHapticFeedback.trigger(
              'impactLight',
            );
            setScrollEnabled(false);
          }}
          onDragEnd={() => {
            setScrollEnabled(true);
          }}
          onChange={(data) => {
            setControls(data);
            setEditMode(true);
          }}
        />
      </ControlPanelView>
    </View>
  );
};
