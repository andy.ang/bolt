import React from 'react';
import {
  Button,
  Header,
  Icons,
  Stack,
  View,
} from '../../components';
import { useAppContext } from '../../context/app/app-context';
import { Gradient } from '../../typings/theme.types';
import { Screens } from '../../typings/screens.types';
import { EventManager } from '../../lib/event';
import { useNavigation } from '@react-navigation/native';
import { deleteData } from '../../lib/storage';
import { StorageKeys } from '../../typings/storage.types';

export const SettingsScreen = () => {
  const {
    mode,
    setColorMode,
    setGradient,
  } = useAppContext();
  const navigation = useNavigation();

  return (
    <View testID="settings" mode={mode} type="normal">
      <Header
        title={Screens.SETTINGS}
        leftButton={{
          icon: Icons.BACK,
          onPress: () => navigation.goBack(),
        }}
      />
      <Stack alignX="center" zIndexes="equal">
        <Button
          label="Dark mode"
          onPress={() => {
            setColorMode('dark');
          }}
        />
        <Button
          label="Light mode"
          onPress={() => {
            setColorMode('light');
          }}
        />
        <Button
          label="System mode"
          onPress={() => {
            setColorMode(undefined);
          }}
        />
        <Button
          label="Gradient 1"
          onPress={() => {
            setGradient(Gradient.GRADIENT_1);
          }}
        />
        <Button
          label="Gradient 2"
          onPress={() => {
            setGradient(Gradient.GRADIENT_2);
          }}
        />
        <Button
          label="Unset gradient"
          onPress={() => {
            setGradient(undefined);
          }}
        />
        <Button
          label="Unset control panel"
          onPress={() => {
            deleteData(StorageKeys.CONTROL_PANEL_LAYOUT);
          }}
        />
        <Button
          label="Change control panel layout"
          onPress={() => {
            navigation.navigate(Screens.SETTINGS_CONTROL);
          }}
        />
        <Button
          label="Debug JSON View"
          onPress={() => {
            navigation.navigate(Screens.DEBUG);
          }}
        />
        <Button
          label="Trigger error"
          onPress={() => {
            EventManager.emitError({
              title: 'Happy Holidays',
              body: 'Ho ho ho!',
            });
          }}
        />
      </Stack>
    </View>
  );
};
