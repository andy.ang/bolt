import { useNavigation } from '@react-navigation/native';
import React, { useEffect, useState } from 'react';
import JSONTree from 'react-native-json-tree';
import {
  Header,
  Icons,
  Stack,
  View,
} from '../../components';
import { useAppContext } from '../../context/app/app-context';
import { EventManager } from '../../lib/event';
import { getVehicleSummary } from '../../services/vehicle/vehicle-data.service';
import { Events } from '../../typings/event.types';
import { IVehicleSummary } from '../../typings/vehicle-data.types';

export const DebugScreen = () => {
  const { mode, currentVehicle } = useAppContext();
  const [refreshing, setRefreshing] = useState<boolean>();
  const [vehicleSummary, setVehicleSummary] = useState<
    IVehicleSummary
  >();
  const navigation = useNavigation();

  useEffect(() => {
    setRefreshing(false);
  }, [currentVehicle]);

  useEffect(() => {
    if (currentVehicle) {
      getVehicleSummary(currentVehicle.id_s)
        .then((summary) => {
          setVehicleSummary(summary as IVehicleSummary);
        })
        .catch((error) => {
          console.error(error);
        });
    }
  }, [currentVehicle]);

  return (
    <View
      testID="settings-control"
      mode={mode}
      type="scroll">
      <Header
        title="Current Vehicle"
        leftButton={{
          icon: Icons.BACK,
          label: 'back',
          onPress: () => {
            navigation.goBack();
          },
        }}
        rightButton={{
          icon: Icons.REFRESH,
          animating: refreshing,
          onPress: () => {
            setRefreshing(true);
            EventManager.emit(Events.REFRESH_VEHICLES);
          },
        }}
      />
      <Stack>
        <JSONTree data={currentVehicle} />
        <JSONTree data={vehicleSummary} />
      </Stack>
    </View>
  );
};
