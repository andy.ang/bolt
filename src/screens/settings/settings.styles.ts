import styled from 'styled-components/native';

export const ControlPanelView = styled.View`
  background: ${(props) => props.theme.secondaryBackground};
  width: 100%;
`;
