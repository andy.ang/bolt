import styled from 'styled-components/native';

export const ButtonsLayout = styled.View`
  padding-vertical: 20px;
  padding-horizontal: 20px;
  width: 300px;
  align-self: center;
  z-index: 1;
  margin-top: auto;
`;

export const TextInputContainer = styled.View`
  width: 300px;
  align-self: center;
`;

export const DropDownContainer = styled.View`
  height: 0;
`;
