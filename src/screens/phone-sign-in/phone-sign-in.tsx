import React, { useEffect, useRef, useState } from 'react';
import {
  Button,
  DropDown,
  Stack,
  Text,
  TextInput,
  TextLink,
  View,
} from '../../components';
import { useAppContext } from '../../context/app/app-context';
import { ConfirmationResult } from '../../typings/auth.types';
import {
  Error,
  FirebaseAuthError,
} from '../../typings/error.types';
import {
  ButtonsLayout,
  DropDownContainer,
  TextInputContainer,
} from './phone-sign-in.styles';
import {
  CountryCode,
  CountryDetails,
  getCountryCodes,
} from '../../lib/util';
import { getCountryCodeFromIP } from '../../services/map/map.service';
import { EventManager } from '../../lib/event';
import { signInWithMobile } from '../../services/auth/auth.service';

export const PhoneSignInScreen = () => {
  const { mode } = useAppContext();
  const countryCodes = useRef<CountryCode>(
    getCountryCodes(),
  );
  const defaultCountryCode = countryCodes.current.AU;
  const items = Object.values(countryCodes.current).map(
    (code) => ({
      label: `${code.name} ${code.callingCode}`,
      value: code.code,
    }),
  );
  const [
    confirm,
    setConfirm,
  ] = useState<ConfirmationResult | null>(null);
  const [phoneNumber, setPhoneNumber] = useState<string>(
    '',
  );
  const [code, setCode] = useState<string>('');
  const [country, setCountry] = useState<CountryDetails>(
    defaultCountryCode,
  );
  const [loading, setLoading] = useState<boolean>(false);
  const countryPickerRef = useRef(null);

  const signInWithPhoneNumber = async () => {
    try {
      setLoading(true);
      const confirmation = await signInWithMobile(
        country.callingCode + phoneNumber,
      );
      setConfirm(confirmation);
      setLoading(false);
    } catch (error) {
      setLoading(false);
      if (
        error.code === FirebaseAuthError.TOO_MANY_REQUESTS
      ) {
        EventManager.emitError({
          body: Error.TOO_MANY_ATTEMPTS,
        });
      } else if (
        error.code ===
        FirebaseAuthError.INVALID_PHONE_NUMBER
      ) {
        EventManager.emitError({
          body: Error.INVALID_PHONE_NUMBER,
        });
      } else if (
        error.code === FirebaseAuthError.INTERNAL_ERROR
      ) {
        EventManager.emitError({
          body: Error.GENERIC,
        });
      } else {
        console.error(error);
      }
    }
  };

  const verifyCodeAndSignIn = async () => {
    try {
      if (confirm) {
        setLoading(true);
        await confirm.confirm(code);
        setLoading(false);
      }
    } catch (error) {
      setLoading(false);
      if (
        error.code ===
        FirebaseAuthError.OPERATION_NOT_ALLOWED
      ) {
        EventManager.emitError({
          body: Error.GENERIC,
        });
      } else if (
        error.code === FirebaseAuthError.INVALID_CODE
      ) {
        EventManager.emitError({
          body: Error.INVALID_CODE,
        });
      } else if (
        error.code === FirebaseAuthError.MISSING_CODE
      ) {
        EventManager.emitError({
          body: Error.INVALID_CODE,
        });
      } else {
        console.error(error);
      }
    }
  };

  useEffect(() => {
    getCountryCodeFromIP().then((cc) => {
      if (cc) {
        const retrievedCountry = countryCodes.current[cc];
        setCountry(retrievedCountry);
      }
    });
  }, []);

  return (
    <View
      testID="phone-sign-in"
      mode={mode}
      bottomInset
      type="normal">
      <Stack space="big" alignX="center">
        <Text alignX="center">
          Sign up with your phone number
        </Text>
        <TextInputContainer>
          {!confirm ? (
            <TextInput
              placeholder="Enter phone number here"
              value={phoneNumber}
              onChangeText={setPhoneNumber}
              prefix={country.callingCode}
            />
          ) : (
            <TextInput
              placeholder="Enter code here"
              value={code}
              onChangeText={setCode}
            />
          )}
        </TextInputContainer>
        {!confirm ? (
          <TextLink
            message="Incorrect area code?"
            onPress={() =>
              //@ts-ignore
              //TS definition is already merged. Currently waiting for next release.
              //https://github.com/lawnstarter/react-native-picker-select/issues/357
              countryPickerRef.current.togglePicker(true)
            }
          />
        ) : (
          <TextLink
            message="Didn't get the code? Click here to try again."
            onPress={() => {
              setConfirm(null);
            }}
          />
        )}
        <DropDownContainer>
          <DropDown
            ref={countryPickerRef}
            items={items}
            value={country.code}
            placeholder="Choose a country"
            onItemPicked={(cc) => {
              setCountry(countryCodes.current[cc]);
            }}
          />
        </DropDownContainer>
      </Stack>
      <ButtonsLayout>
        {!confirm ? (
          <Button
            label="Send code"
            loading={loading}
            onPress={signInWithPhoneNumber}
          />
        ) : (
          <Button
            label="Verify code"
            loading={loading}
            onPress={verifyCodeAndSignIn}
          />
        )}
      </ButtonsLayout>
    </View>
  );
};
