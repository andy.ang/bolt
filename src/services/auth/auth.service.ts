import auth, {
  FirebaseAuthTypes,
} from '@react-native-firebase/auth';

export const signInWithMobile = async (
  mobile: string,
): Promise<FirebaseAuthTypes.ConfirmationResult> => {
  try {
    return await auth().signInWithPhoneNumber(mobile);
  } catch (error) {
    throw error;
  }
};

export const retrieveUserDetails = () => {
  return auth().currentUser;
};
