import firestore from '@react-native-firebase/firestore';
import {
  Collection,
  IUser,
} from '../../typings/users.types';

export const registerUser = async (user: IUser) => {
  try {
    await firestore()
      .collection(Collection.USERS)
      .doc(user.id)
      .set(user);
  } catch (error) {
    console.error(
      `[Register user]: Error registering user. ${error}`,
    );
    throw error;
  }
};

export const updateUser = async (user: IUser) => {
  try {
    await firestore()
      .collection(Collection.USERS)
      .doc(user.id)
      .update(user);
  } catch (error) {
    console.error(
      `[Update user]: Error updating user. ${error}`,
    );
    throw error;
  }
};

export const getUser = async (
  id: IUser['id'],
): Promise<IUser> => {
  try {
    const snapshot = await firestore()
      .collection(Collection.USERS)
      .doc(id)
      .get();
    return snapshot.data() as IUser;
  } catch (error) {
    console.error(
      `[Update user]: Error updating user. ${error}`,
    );
    throw error;
  }
};

export const updateToken = async (
  user: Omit<
    IUser,
    'phoneNumber' | 'paid' | 'refreshToken'
  >,
) => {
  try {
    await firestore()
      .collection(Collection.USERS)
      .doc(user.id)
      .update({
        accessToken: user.accessToken,
      });
  } catch (error) {
    throw error;
  }
};
