import { LatLng } from 'react-native-maps';
import publicIP from 'react-native-public-ip';
import { config } from '../../config/config';
import { request } from '../../lib/request';
import { getData, storeData } from '../../lib/storage';
import {
  StorageKeys,
  IStoredAddress,
  IStoredCountryCode,
} from '../../typings/storage.types';
import { truncateNumberToX } from '../../lib/util';
import {
  ITomTomEVRouteResponse,
  IRouteRequestParams,
  IGeocodeResponse,
  IIPStackResponse,
} from './map.types';

const GOOGLE_API_PATH = 'maps/api';
const TOM_TOM_API_PATH = 'routing/1/calculateRoute';
const DEFAULT_DIMENSION = 100;

interface IPhotoRequest {
  photoRef: string;
}

interface IRouteRequest {
  origin: LatLng;
  destination: LatLng;
  params: IRouteRequestParams;
}

interface IRouteRequestError {
  response: {
    data: {
      detailedError: {
        code: string;
      };
    };
  };
}

export const getGooglePhotoURL = (
  req: IPhotoRequest,
): string => {
  return (
    `${config.GOOGLE_MAPS_URL}/${GOOGLE_API_PATH}/place/photo?` +
    `key=${config.GOOGLE_API_KEY}&` +
    `maxheight=${DEFAULT_DIMENSION}&` +
    `photoreference=${req.photoRef}`
  );
};

export const getRoutes = async (
  req: IRouteRequest,
): Promise<ITomTomEVRouteResponse | undefined> => {
  const { origin, destination, params } = req;
  const url =
    `${config.TOM_TOM_API_URL}/${TOM_TOM_API_PATH}/` +
    encodeURIComponent(
      `${origin.latitude},${origin.longitude}:${destination.latitude},${destination.longitude}`,
    ) +
    '/json';

  const defaultParams: Partial<IRouteRequestParams> = {
    traffic: true,
    avoid: 'unpavedRoads',
    travelMode: 'car',
    routeType: 'fastest',
    vehicleEngineType: 'electric',
    vehicleLength: 4.7,
    vehicleWidth: 1.9,
    vehicleHeight: 1.44,
    vehicleWeight: 1609,
    vehicleMaxSpeed: 210,
    // This needs to be in config / tesla api
    constantSpeedConsumptionInkWhPerHundredkm: '100,15',
  };

  const totalParams = {
    ...defaultParams,
    ...params,
    key: config.TOM_TOM_API_KEY,
  };
  try {
    return await request<ITomTomEVRouteResponse>({
      method: 'get',
      url,
      params: totalParams,
    });
  } catch (error) {
    if (
      (error as IRouteRequestError).response.data
        .detailedError.code === 'NO_ROUTE_FOUND'
    ) {
      console.warn('No route found');
      return undefined;
    }
    throw error;
  }
};

const _retrieveCountryCodeFromIP = async (
  ip: string | null,
) => {
  const storedCountryCode = await getData(
    StorageKeys.COUNTRY_CODE,
  );
  if (ip && storedCountryCode) {
    const codeFound = storedCountryCode.find(
      (code) => code.ip === ip,
    );
    return codeFound ? codeFound.countryCode : undefined;
  }
  return undefined;
};

const _retrieveAndMatchCoordinates = async ({
  latitude,
  longitude,
}: LatLng): Promise<string | undefined> => {
  const storedAddresses = await getData(
    StorageKeys.ADDRESS,
  );
  if (storedAddresses) {
    const addressFound = storedAddresses.find(
      (addr) =>
        addr.latLng.latitude ===
          truncateNumberToX(latitude, 2) &&
        addr.latLng.longitude ===
          truncateNumberToX(longitude, 2),
    );
    return addressFound ? addressFound.address : undefined;
  }
  return undefined;
};

const _saveAddress = async (
  address: string,
  coordinate: LatLng,
): Promise<void> => {
  const { latitude, longitude } = coordinate;
  const addressToStore: IStoredAddress = {
    latLng: {
      latitude: truncateNumberToX(latitude, 2),
      longitude: truncateNumberToX(longitude, 2),
    },
    address: address,
  };

  const storedAddresses = await getData(
    StorageKeys.ADDRESS,
  );
  storeData(
    StorageKeys.ADDRESS,
    storedAddresses
      ? [...storedAddresses, addressToStore]
      : [addressToStore],
  );
};

const _saveCountryCode = async (
  ip: string,
  countryCode: string,
) => {
  const codeToStore: IStoredCountryCode = {
    ip,
    countryCode,
  };

  const storedCode = await getData(
    StorageKeys.COUNTRY_CODE,
  );
  storeData(
    StorageKeys.COUNTRY_CODE,
    storedCode
      ? [...storedCode, codeToStore]
      : [codeToStore],
  );
};

export const getAddressFromCoordinates = async ({
  latitude,
  longitude,
}: LatLng): Promise<string> => {
  try {
    const storedAddress = await _retrieveAndMatchCoordinates(
      { latitude, longitude },
    );
    if (storedAddress) {
      console.log('Use stored address');
      return storedAddress;
    }
    console.log(
      'No address found in storage. Proceed with expensive reverse geocoding.',
    );
    const url = `${config.GOOGLE_MAPS_URL}/${GOOGLE_API_PATH}/geocode/json?`;
    const res = await request<IGeocodeResponse>({
      method: 'GET',
      url,
      params: {
        key: config.GOOGLE_API_KEY,
        latlng: `${latitude},${longitude}`,
      },
    });
    const geocodedAddr = res.results[0].formatted_address;
    await _saveAddress(geocodedAddr, {
      latitude,
      longitude,
    });
    return geocodedAddr;
  } catch (error) {
    console.warn('[getAddressFromCoordinates]', error);
    throw error;
  }
};

export const getCountryCodeFromIP = async () => {
  try {
    const ip = await publicIP();
    const storedCountryCode = await _retrieveCountryCodeFromIP(
      ip,
    );
    if (storedCountryCode) {
      console.log('Use stored country code');
      return storedCountryCode;
    }
    console.log(
      'No country code found in storage. Proceed with expensive geocoding.',
    );
    if (ip) {
      const res = await request<IIPStackResponse>({
        method: 'GET',
        url: `${config.IPSTACK_API_URL}/${ip}`,
        params: {
          access_key: config.IPSTACK_API_KEY,
        },
      });
      await _saveCountryCode(ip, res.country_code);
      return res.country_code;
    }
    return undefined;
  } catch (error) {
    console.error(error);
    return undefined;
  }
};
