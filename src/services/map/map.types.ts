import { LatLng } from 'react-native-maps';

export interface ISummary {
  lengthInMeters: number;
  travelTimeInSeconds: number;
  trafficDelayInSeconds: number;
  departureTime: string;
  arrivalTime: string;
  batteryConsumptionInkWh: number;
}

interface ILeg {
  summary: ISummary;
  points: LatLng[];
}

interface IRoute {
  summary: ISummary;
  legs: ILeg[];
}

export interface ITomTomEVRouteResponse {
  formatVersion: string;
  routes: IRoute[];
}

type AvoidType =
  | 'unpavedRoads'
  | 'tollRoads'
  | 'motorways'
  | 'ferries'
  | 'carpools'
  | 'alreadyUsedRoads';

export interface IRouteRequestParams {
  maxAlternatives?: number;
  traffic?: boolean;
  avoid?: AvoidType;
  routeType?: 'fastest' | 'shortest';
  departAt?: string;
  travelMode?: 'car';
  vehicleMaxSpeed?: number;
  vehicleWeight?: number;
  vehicleAxleWeight?: number;
  vehicleLength?: number;
  vehicleWidth?: number;
  vehicleHeight?: number;
  vehicleEngineType?: 'electric';
  accelerationEfficiency?: number;
  decelerationEfficiency?: number;
  uphillEfficiency?: number;
  downhillEfficiency?: number;
  constantSpeedConsumptionInkWhPerHundredkm?: string;
  currentChargeInkWh: number;
  maxChargeInkWh: number;
  auxiliaryPowerInkW?: number;
}

interface IPlusCode {
  compound_code: string;
  global_code: string;
}

type ViewPort = {
  lat: number;
  lng: number;
};
interface IGeometry {
  location: LatLng;
  location_type: string;
  viewport: {
    northeast: ViewPort;
    southwest: ViewPort;
  };
}
interface IGeocodeResult {
  address_components: [];
  formatted_address: string;
  geometry: IGeometry;
  place_id: string;
  types: string[];
}

export interface IGeocodeResponse {
  plus_code: IPlusCode;
  results: IGeocodeResult[];
}

export interface IIPStackResponse {
  country_code: string;
  latitude: number;
  longitude: number;
}
