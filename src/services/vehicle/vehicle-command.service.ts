import { callFunctions } from '../../lib/request';
import { getSecureData } from '../../lib/storage';
import {
  Controls,
  ICommandData,
} from '../../typings/vehicle-command.types';
import { Functions } from '../../typings/functions.types';
import { SecureStorageKeys } from '../../typings/storage.types';
import { EventManager } from '../../lib/event';
import { Events } from '../../typings/event.types';

export const sendCommand = async (
  vehicleId: string,
  command: Controls,
  extraData?: ICommandData,
) => {
  try {
    const data = await getSecureData(
      SecureStorageKeys.ACCESS_TOKEN,
    );
    if (data) {
      const { access_token } = data;
      const {
        latitude,
        longitude,
        window,
        maxDefrost,
        heatLevel,
        seatId,
        chargeLimit,
        driverTemperature,
        passengerTemperature,
        sentryMode,
        valetMode,
        valetPin,
        speedLimitMode,
        speedLimitPin,
        speedMph,
        sunroof,
        chargingTime,
        chargingTask,
        chargingFrequency,
      } = extraData || {};

      switch (command) {
        case Controls.LOCK:
        case Controls.UNLOCK: {
          await callFunctions(
            Functions.LOCK_UNLOCK_VEHICLE,
            {
              vehicleId,
              command,
              access_token,
            },
          );
          break;
        }
        case Controls.OPEN_FRUNK:
        case Controls.OPEN_TRUNK:
          await callFunctions(Functions.OPEN_TRUNK_FRUNK, {
            vehicleId,
            command,
            access_token,
          });
          break;
        case Controls.WAKE:
          await callFunctions(Functions.WAKE_VEHICLE, {
            vehicleId,
            access_token,
          });
          break;
        case Controls.TRIGGER_HOME_LINK:
          if (latitude && longitude) {
            await callFunctions(
              Functions.TRIGGER_HOME_LINK,
              {
                vehicleId,
                access_token,
                latitude,
                longitude,
              },
            );
          }
          break;
        case Controls.START_AC:
        case Controls.STOP_AC:
          await callFunctions(
            Functions.START_STOP_AIR_COND,
            {
              vehicleId,
              access_token,
              command,
            },
          );
          break;
        case Controls.FLASH_LIGHTS:
          await callFunctions(Functions.FLASH_LIGHTS, {
            vehicleId,
            access_token,
          });
          break;
        case Controls.HONK_HORN:
          await callFunctions(Functions.HONK_HORN, {
            vehicleId,
            access_token,
          });
          break;
        case Controls.SET_TEMPS:
          if (driverTemperature && passengerTemperature) {
            await callFunctions(Functions.SET_TEMPS, {
              vehicleId,
              access_token,
              driverTemperature,
              passengerTemperature,
            });
          }
          break;
        case Controls.SET_CHARGE_LIMIT:
          if (chargeLimit) {
            await callFunctions(
              Functions.SET_CHARGE_LIMIT,
              {
                vehicleId,
                access_token,
                limit: chargeLimit,
              },
            );
          }
          break;
        case Controls.REMOTE_SEAT_HEATER_REQUEST:
          if (
            heatLevel !== undefined &&
            seatId !== undefined
          ) {
            await callFunctions(
              Functions.REMOTE_SEAT_HEATER_REQUEST,
              {
                vehicleId,
                access_token,
                heater: seatId,
                level: heatLevel,
              },
            );
          }
          break;
        case Controls.TOGGLE_MAX_DEFROST:
          if (maxDefrost !== undefined) {
            await callFunctions(
              Functions.TOGGLE_MAX_DEFROST,
              {
                vehicleId,
                access_token,
                on: maxDefrost,
              },
            );
          }
          break;
        case Controls.TOGGLE_WINDOW:
          if (
            window !== undefined &&
            latitude &&
            longitude
          ) {
            await callFunctions(Functions.TOGGLE_WINDOW, {
              vehicleId,
              access_token,
              command: window,
              latitude,
              longitude,
            });
          }
          break;
        case Controls.PLAY_NEXT_TRACK:
          await callFunctions(Functions.PLAY_NEXT_TRACK, {
            vehicleId,
            access_token,
          });
          break;
        case Controls.PLAY_PREVIOUS_TRACK:
          await callFunctions(
            Functions.PLAY_PREVIOUS_TRACK,
            {
              vehicleId,
              access_token,
            },
          );
          break;
        case Controls.TOGGLE_MEDIA_PLAYBACK:
          await callFunctions(
            Functions.TOGGLE_MEDIA_PLAYBACK,
            {
              vehicleId,
              access_token,
            },
          );
          break;
        case Controls.TURN_VOLUME_UP:
          await callFunctions(Functions.TURN_VOLUME_UP, {
            vehicleId,
            access_token,
          });
          break;
        case Controls.TURN_VOLUME_DOWN:
          await callFunctions(
            Functions.TOGGLE_MEDIA_PLAYBACK,
            {
              vehicleId,
              access_token,
            },
          );
          break;
        case Controls.SET_SENTRY_MODE:
          if (sentryMode !== undefined) {
            await callFunctions(Functions.SET_SENTRY_MODE, {
              vehicleId,
              access_token,
              on: sentryMode,
            });
          }
          break;
        case Controls.SET_VALET_MODE:
          if (valetMode !== undefined && valetPin) {
            await callFunctions(Functions.SET_VALET_MODE, {
              vehicleId,
              access_token,
              on: valetMode,
              pin: valetPin,
            });
          }
          break;
        case Controls.RESET_VALET_PIN:
          await callFunctions(Functions.RESET_VALET_PIN, {
            vehicleId,
            access_token,
          });
          break;
        case Controls.SET_SPEED_LIMIT_MODE:
          if (
            speedLimitMode !== undefined &&
            speedLimitPin
          ) {
            await callFunctions(
              Functions.SET_SPEED_LIMIT_MODE,
              {
                vehicleId,
                access_token,
                on: speedLimitMode,
                pin: speedLimitPin,
              },
            );
          }
          break;
        case Controls.SET_SPEED_LIMIT:
          if (speedMph) {
            await callFunctions(Functions.SET_SPEED_LIMIT, {
              vehicleId,
              access_token,
              limit_mph: speedMph,
            });
          }
          break;
        case Controls.RESET_SPEED_LIMIT_PIN:
          if (speedLimitPin) {
            await callFunctions(
              Functions.RESET_SPEED_LIMIT_PIN,
              {
                vehicleId,
                access_token,
                pin: speedLimitPin,
              },
            );
          }
          break;
        case Controls.START_REMOTE_DRIVE:
          const password = await getSecureData(
            SecureStorageKeys.PASSWORD,
          );
          if (password) {
            await callFunctions(
              Functions.START_REMOTE_DRIVE,
              {
                vehicleId,
                access_token,
                password: '',
              },
            );
          }
          break;
        case Controls.TOGGLE_SUNROOF:
          if (sunroof) {
            await callFunctions(Functions.TOGGLE_SUNROOF, {
              vehicleId,
              access_token,
              state: sunroof,
            });
          }
          break;
        case Controls.SCHEDULE_CHARGING:
          if (
            chargingTime !== undefined &&
            chargingFrequency !== undefined
          ) {
            return await callFunctions(
              Functions.SCHEDULE_CHARGING,
              {
                vehicleId,
                access_token,
                time: chargingTime,
                frequency: chargingFrequency,
              },
            );
          }
          break;
        case Controls.START_CHARGING:
          await callFunctions(Functions.START_CHARGING, {
            vehicleId,
            access_token,
          });
          break;
        case Controls.STOP_CHARGING:
          await callFunctions(Functions.STOP_CHARGING, {
            vehicleId,
            access_token,
          });
          break;
        case Controls.STANDARD_CHARGING:
          await callFunctions(Functions.STANDARD_CHARGING, {
            vehicleId,
            access_token,
          });
          break;
        case Controls.MAX_CHARGING:
          await callFunctions(Functions.MAX_CHARGING, {
            vehicleId,
            access_token,
          });
          break;
        case Controls.DELETE_SCHEDULED_CHARGING:
          if (chargingTask) {
            await callFunctions(
              Functions.DELETE_SCHEDULED_CHARGING,
              {
                vehicleId,
                access_token,
                task: chargingTask,
              },
            );
            break;
          }
        default:
          break;
      }

      EventManager.emit(Events.REFRESH_VEHICLES, {
        command,
      });
    }
  } catch (error) {
    console.error(error);
    throw error;
  }
};
