import firestore from '@react-native-firebase/firestore';
import { callFunctions, request } from '../../lib/request';
import { getSecureData } from '../../lib/storage';
import { SecureStorageKeys } from '../../typings/storage.types';
import { Functions } from '../../typings/functions.types';
import { config } from '../../config/config';
import {
  deriveImagePath,
  mapVehicleDataToSummary,
  mapVehicleDataToUser,
} from '../../lib/util';
import {
  IUserVehicleData,
  IVehicleData,
} from '../../typings/vehicle-data.types';
import { Collection } from '../../typings/users.types';
import { firebase } from '@react-native-firebase/remote-config';

/**
 * List all vehicles
 */
export const getVehicles = async () => {
  try {
    const data = await getSecureData(
      SecureStorageKeys.ACCESS_TOKEN,
    );
    if (data) {
      return await callFunctions(Functions.GET_VEHICLES, {
        access_token: data.access_token,
      });
    }
    return undefined;
  } catch (error) {
    console.error(error);
    throw error;
  }
};

/**
 * Get all data about the vehicle
 */
export const getVehicleSummary = async (
  vehicleId: IVehicleData['id_s'],
) => {
  const vehicleRef = firestore()
    .collection(Collection.VEHICLES)
    .doc(vehicleId);

  const snapshot = await vehicleRef.get();
  return snapshot.data();
};

export const getCarImage = (optionCodes: string) => {
  return `${config.FIREBASE_HOSTING_URL}/${deriveImagePath(
    optionCodes,
  )}.png`;
};

export const getCarConfig = async () => {
  try {
    return await request({
      method: 'GET',
      url: `${config.FIREBASE_HOSTING_URL}/tesla.json`,
    });
  } catch (error) {
    console.error(error);
    throw error;
  }
};

export const registerVehicles = async (
  userId: string,
  vehicles: IVehicleData[],
) => {
  await Promise.all(
    vehicles.map(async (vehicle) => {
      const userVehicleData = mapVehicleDataToUser(vehicle);
      const summary = mapVehicleDataToSummary(
        userId,
        vehicle,
      );
      const vehicleRef = firestore()
        .collection(Collection.VEHICLES)
        .doc(vehicle.id_s);
      const getVehicleRef = await vehicleRef.get();
      const vehicleExists = getVehicleRef.data();

      if (vehicleExists) {
        await vehicleRef.update({
          userId: firebase.firestore.FieldValue.arrayUnion(
            userId,
          ),
        });
      } else {
        // Create vehicle summary
        await vehicleRef.set(summary);
        // Updte vehicle data
        await vehicleRef
          .collection(Collection.DATA)
          .add(userVehicleData);
      }
    }),
  );
};

export const getStatsData = async (
  vehicleId: IVehicleData['id_s'],
  hours: number = 3,
) => {
  const today = new Date().getTime();
  const xDaysAgo = today - hours * 60 * 60 * 1000;
  const vehicleRef = firestore()
    .collection(Collection.VEHICLES)
    .doc(vehicleId)
    .collection(Collection.DATA)
    .orderBy('createdAt', 'desc')
    .where('createdAt', '>=', new Date(xDaysAgo));

  const snapshotData = await vehicleRef.get();
  const data = snapshotData.docs.map((snap) => snap.data());
  console.log(`Returned ${data.length} datapoints.`);
  return data as IUserVehicleData[];
};
