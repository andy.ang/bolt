import remoteConfig from '@react-native-firebase/remote-config';

export const fetchRemoteConfig = <T>(
  initialState: T,
): Promise<T> => {
  return new Promise((resolve, reject) => {
    remoteConfig()
      .setDefaults(initialState as {})
      .then(() => remoteConfig().fetchAndActivate())
      .then((fetchedRemotely) => {
        if (fetchedRemotely) {
          const parameters = remoteConfig().getAll();
          const config = {};
          Object.entries(parameters).forEach((params) => {
            const [key, entry] = params;
            //@ts-ignore
            config[key] = entry.asString();
          });
          console.log(
            'Configs were retrieved from the backend and activated.',
            config,
          );
          return resolve(config as T);
        } else {
          console.log(
            'No configs were fetched from the backend, and the local configs were already activated',
          );
          return reject(
            new Error('Error fetching remote config'),
          );
        }
      });
  });
};
