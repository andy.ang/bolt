import { hasTokenExpired } from '../../lib/auth';
import functions from '@react-native-firebase/functions';
import {
  getSecureData,
  storeSecureData,
} from '../../lib/storage';
import { SecureStorageKeys } from '../../typings/storage.types';
import { Functions } from '../../typings/functions.types';
import { callFunctions } from '../../lib/request';
import { updateToken } from '../users/users.service';
import { retrieveUserDetails } from '../auth/auth.service';

export const signInToBolt = async (
  email: string,
  password: string,
) => {
  try {
    return await callFunctions(Functions.AUTHENTICATE, {
      email,
      password,
    });
  } catch (error) {
    if (
      error.code ===
      functions.HttpsErrorCode.UNAUTHENTICATED
    ) {
      console.error(
        '[Sign In]: Incorrect sign in details.',
      );
      return undefined;
    }
    throw error;
  }
};

export const refreshToken = async (token: string) => {
  try {
    return await callFunctions(Functions.REFRESH_TOKEN, {
      token,
    });
  } catch (error) {
    console.error(`[Error refresh token]: ${error}`);
    throw error;
  }
};

export const validateAccessToken = async () => {
  try {
    const data = await getSecureData(
      SecureStorageKeys.ACCESS_TOKEN,
    );
    if (data) {
      const {
        created_at,
        expires_in,
        refresh_token,
      } = data;
      const expired = hasTokenExpired(
        created_at,
        expires_in,
      );
      if (expired) {
        console.log('[AccessToken]: Refreshing token.');
        const newToken = await refreshToken(refresh_token);
        await storeSecureData(
          SecureStorageKeys.ACCESS_TOKEN,
          newToken,
        );
        const user = await retrieveUserDetails();
        if (user) {
          await updateToken({
            id: user.uid,
            accessToken: newToken.access_token,
          });
        }
      }
    }
  } catch (error) {
    throw error;
  }
};
