import React from 'react';
import { StyleSheet, Dimensions } from 'react-native';
import {
  NavigationContainer,
  useNavigation,
} from '@react-navigation/native';
import { createDrawerNavigator } from '@react-navigation/drawer';
import { createStackNavigator } from '@react-navigation/stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { useTheme } from 'styled-components/native';
import Ionicons from 'react-native-vector-icons/Ionicons';
import Fontisto from 'react-native-vector-icons/Fontisto';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';

Fontisto.loadFont();
Ionicons.loadFont();
MaterialCommunityIcons.loadFont();

import {
  HomeScreen,
  DrawerScreen,
  SettingsScreen,
  ChargingScreen,
  StatisticsScreen,
  LocationScreen,
  BoltSignInScreen,
  SplashScreen,
  PhoneSignInScreen,
  SettingsControlScreen,
  DebugScreen,
} from './screens';
import { Icon, Icons } from './components/';
import { Screens } from './typings/screens.types';
import { useAppContext } from './context/app/app-context';

const Stack = createStackNavigator();
const Tab = createBottomTabNavigator();
const Drawer = createDrawerNavigator();

const { width } = Dimensions.get('window');

const styles = StyleSheet.create({
  drawerBackground: {
    shadowColor: 'black',
    shadowOpacity: 1,
    shadowRadius: 5,
    elevation: 10,
  },
  drawer: {
    width: width / 2,
    backgroundColor: 'black',
  },
});

const TabStack = () => {
  const {
    tabButtonActiveColor,
    tabButtonInactiveColor,
  } = useTheme();
  return (
    <Tab.Navigator
      tabBarOptions={{
        showLabel: false,
        activeTintColor: tabButtonActiveColor,
        inactiveTintColor: tabButtonInactiveColor,
        style: {
          position: 'absolute',
          backgroundColor: 'transparent',
          borderTopWidth: 0,
          borderTopColor: 'transparent',
          elevation: 0,
        },
      }}>
      <Tab.Screen
        name={Screens.HOME}
        component={HomeScreen}
        options={{
          title: Screens.HOME,
          tabBarTestID: Screens.HOME,
          tabBarIcon: ({ color, size }) => (
            <Icon
              name={Icons.WHEEL}
              size={size}
              color={color}
              scale={1.8}
            />
          ),
        }}
      />
      <Tab.Screen
        name={Screens.CHARGING}
        component={ChargingScreen}
        options={{
          title: Screens.CHARGING,
          tabBarTestID: Screens.CHARGING,
          tabBarIcon: ({ focused, color, size }) => (
            <Icon
              name={
                focused ? Icons.FLASH_FIILED : Icons.FLASH
              }
              size={size}
              color={color}
            />
          ),
        }}
      />
      <Tab.Screen
        name={Screens.LOCATION}
        component={LocationScreen}
        options={{
          title: Screens.LOCATION,
          tabBarTestID: Screens.LOCATION,
          tabBarIcon: ({ focused, color, size }) => (
            <Icon
              name={focused ? Icons.MAP_FILLED : Icons.MAP}
              size={size}
              color={color}
            />
          ),
        }}
      />
      <Tab.Screen
        name={Screens.STATS}
        component={StatisticsScreen}
        options={{
          title: Screens.STATS,
          tabBarTestID: Screens.STATS,
          tabBarIcon: ({ focused, color, size }) => (
            <Icon
              name={
                focused ? Icons.CHART_FILLED : Icons.CHART
              }
              size={size}
              color={color}
            />
          ),
        }}
      />
    </Tab.Navigator>
  );
};

const useCurrentRoute = (): string => {
  const navigation = useNavigation();
  const state = navigation.dangerouslyGetState();

  let currentRoute: any = state.routes[state.index] || '';

  while (currentRoute.state) {
    currentRoute =
      currentRoute.state.routes[currentRoute.state.index];
  }
  return currentRoute.name;
};

const DrawerStack = (): JSX.Element => {
  const currentRoute = useCurrentRoute();

  return (
    <Drawer.Navigator
      overlayColor="transparent"
      drawerType="back"
      keyboardDismissMode="on-drag"
      lazy={true}
      edgeWidth={width / 4}
      drawerContent={(props) => <DrawerScreen {...props} />}
      statusBarAnimation="fade"
      initialRouteName={Screens.DRAWER}
      sceneContainerStyle={styles.drawerBackground}
      drawerStyle={styles.drawer}>
      <Drawer.Screen
        name={'Tab'}
        component={TabStack}
        options={{
          swipeEnabled: currentRoute !== Screens.LOCATION,
        }}
      />
    </Drawer.Navigator>
  );
};

const AcquisitionStack = [
  <Stack.Screen
    key={Screens.BOLT_SIGN_IN}
    name={Screens.BOLT_SIGN_IN}
    component={BoltSignInScreen}
  />,
];

const SignUpStack = [
  <Stack.Screen
    key={Screens.PHONE_SIGN_IN}
    name={Screens.PHONE_SIGN_IN}
    component={PhoneSignInScreen}
  />,
];

const MainStack = [
  <Stack.Screen
    key={Screens.DRAWER}
    name={Screens.DRAWER}
    component={DrawerStack}
  />,
  <Stack.Screen
    key={Screens.SETTINGS}
    name={Screens.SETTINGS}
    component={SettingsScreen}
  />,
  <Stack.Screen
    key={Screens.SETTINGS_CONTROL}
    name={Screens.SETTINGS_CONTROL}
    component={SettingsControlScreen}
  />,
  <Stack.Screen
    key={Screens.DEBUG}
    name={Screens.DEBUG}
    component={DebugScreen}
  />,
];

export const Router = () => {
  const { authentication } = useAppContext();

  return (
    <NavigationContainer>
      <Stack.Navigator
        headerMode="none"
        initialRouteName={Screens.BOLT_SIGN_IN}
        screenOptions={{
          cardStyle: { backgroundColor: 'transparent' },
          cardShadowEnabled: true,
          cardOverlayEnabled: true,
        }}>
        {authentication === 'unauthenticated' &&
          SignUpStack}
        {authentication === 'needsSignIn' &&
          AcquisitionStack}
        {authentication === 'initializing' && (
          <Stack.Screen
            key={Screens.SPLASH}
            name={Screens.SPLASH}
            component={SplashScreen}
          />
        )}
        {authentication === 'authenticated' && MainStack}
      </Stack.Navigator>
    </NavigationContainer>
  );
};
