import CountryCodes from 'country-codes-list';
import { DateTime } from 'luxon';
import {
  Gutter,
  PADDING,
  Padding,
} from '../typings/layout.types';
import {
  IUserVehicleData,
  IVehicleConfig,
  IVehicleData,
  IVehicleSummary,
  OptionCodeColor,
  OptionCodeModel,
  OptionCodeVariant,
} from '../typings/vehicle-data.types';

enum Country {
  CODE = 'countryCode',
  NAME = 'countryNameEn',
  CALLING_CODE = 'countryCallingCode',
}

const MILES_TO_KM = 1.609;
export interface CountryDetails {
  name: string;
  code: string;
  callingCode: string;
}

export type CountryCode = Record<string, CountryDetails>;

export const determineBatteryLevel = (
  batteryConsumptionInkWh: number,
  maxBatteryCapacityInkWh: number,
  travelDistanceInKm: number,
  batteryLevel: number,
  estimatedBatteryRangeInMiles: number,
  batteryDegradation: number = 0,
) => {
  const estimatedBatteryRangeInKm =
    estimatedBatteryRangeInMiles * MILES_TO_KM;
  const estimatedTotalRange =
    estimatedBatteryRangeInKm / (batteryLevel / 100);
  const estimateBasedOnRange =
    (estimatedBatteryRangeInKm - travelDistanceInKm) /
    estimatedTotalRange;
  const estimatedCurrentBatteryCapacityInKWh =
    (batteryLevel / 100) *
    (maxBatteryCapacityInkWh * (1 - batteryDegradation));
  const estimateBaseOnKWh =
    (estimatedCurrentBatteryCapacityInKWh -
      batteryConsumptionInkWh) /
    (maxBatteryCapacityInkWh * (1 - batteryDegradation));

  return Math.round(
    Math.min(
      estimateBaseOnKWh * 100,
      estimateBasedOnRange * 100,
    ),
  );
};

export const truncateNumberToX = (
  num: number,
  decimals: number,
): number => {
  return parseFloat(num.toFixed(decimals));
};

export const getCountryCodes = (): CountryCode => {
  const transformed: CountryCode = {};
  const codes = CountryCodes.customList(
    Country.CODE,
    `{${Country.CODE}} +{${Country.CALLING_CODE}} {${Country.NAME}}`,
  );
  Object.values(codes).forEach((details) => {
    const [
      code,
      callingCode,
      ...name
    ] = (details as string).split(' ');
    transformed[code] = {
      code,
      name: name.join(' '),
      callingCode,
    };
  });
  return transformed;
};

export const deriveImagePath = (optionCodes: string) => {
  const codes = optionCodes.split(',');
  const color = codes.filter((code) =>
    Object.keys(OptionCodeColor).includes(code),
  )[0];
  const model = codes.filter((code) =>
    Object.keys(OptionCodeModel).includes(code),
  )[0];
  const variant = codes.filter((code) =>
    Object.keys(OptionCodeVariant).includes(code),
  )[0];
  //@ts-ignore
  const carColor = OptionCodeColor[color] || 'black';
  //@ts-ignore
  const carModel = OptionCodeModel[model] || 'roadster';
  //@ts-ignore
  const carVariant = OptionCodeVariant[variant] || '';

  let url = `${carModel}_${carColor}`;

  if (variant && variant.length > 0) {
    url = url + carVariant;
  }

  return url;
};

export const mapVehicleDataToSummary = (
  userId: string,
  vehicle: IVehicleData,
): IVehicleSummary => {
  const { vehicle_config, id_s } = vehicle;
  const {
    car_type,
    exterior_color,
    exterior_trim,
    seat_type,
    wheel_type,
    car_special_type,
  } = vehicle_config || {};

  return {
    id: id_s,
    userId: [userId],
    car_type,
    exterior_color,
    exterior_trim,
    seat_type,
    wheel_type,
    car_special_type,
    scheduled: null,
  };
};

export const mapVehicleDataToUser = (
  vehicle: IVehicleData,
): IUserVehicleData => {
  const {
    charge_state,
    drive_state,
    climate_state,
    vehicle_state,
    id_s,
  } = vehicle;
  const {
    battery_level,
    battery_range,
    est_battery_range,
    charge_rate,
  } = charge_state || {};
  const { timestamp, latitude, longitude } =
    drive_state || {};
  const { outside_temp, inside_temp } = climate_state || {};
  const { car_version, odometer } = vehicle_state || {};

  return {
    id: id_s,
    battery_level,
    battery_range,
    est_battery_range,
    charge_rate,
    latitude,
    longitude,
    outside_temp,
    inside_temp,
    car_version,
    timestamp,
    odometer,
    createdAt: timestamp ? new Date(timestamp) : new Date(),
  };
};

export const convertDateToHumanReadable = (
  timestamp: number,
  format: string,
) => {
  return DateTime.fromMillis(timestamp).toFormat(format);
};

export const getCarNameFromCarType = (
  carType: IVehicleConfig['car_type'] | '',
) => {
  return carType.replace(/model/g, 'Tesla Model ');
};

export const getKMFromMiles = (miles: number) => {
  const rate = 1.60934;
  return Math.round(miles * rate);
};

export const getMilesFromKM = (km: number) => {
  const rate = 1.60934;
  return Math.round(km / rate);
};

export const accessData = (
  accessor: string,
  data: Record<string, any>,
): string | boolean | undefined => {
  const keys = accessor.split('.');
  if (keys && keys.length > 0) {
    let accessedData = data[keys[0]];
    if (accessedData) {
      keys.slice(1).forEach((key) => {
        accessedData = (accessedData as Record<
          string,
          any
        >)[key];
      });
      return accessedData;
    }
  }
  return undefined;
};

export const hex2rgba = (
  hex: string,
  alpha: number = 1,
) => {
  const [r, g, b] = hex
    .match(/\w\w/g)!
    .map((x) => parseInt(x, 16));
  return `rgba(${r},${g},${b},${alpha})`;
};

export const getPaddingFromHideGutter = (
  hideGutter: Gutter | Gutter[],
  paddingSize: Padding,
) => {
  if (!Array.isArray(hideGutter)) {
    return getPaddingFromGutter(hideGutter, paddingSize);
  } else {
    const paddings = hideGutter.map((hg) =>
      getPaddingFromGutter(hg, paddingSize),
    );

    const derivedPadding = paddings.reduce((prev, curr) => {
      const currentPadding = curr
        .split(' ')
        .map((str) => Number(str.replace('px', '')));
      const prevPadding = prev
        .split(' ')
        .map((str) => Number(str.replace('px', '')));
      const paddingTop = `${
        currentPadding[0] + prevPadding[0]
      }`;
      const paddingRight = `${
        currentPadding[1] + prevPadding[1]
      }`;
      const paddingBottom = `${
        currentPadding[2] + prevPadding[2]
      }`;
      const paddingLeft = `${
        currentPadding[3] + prevPadding[3]
      }`;

      return `${paddingTop} ${paddingRight} ${paddingBottom} ${paddingLeft}`;
    }, '0 0 0 0');

    return (
      derivedPadding
        .split(' ')
        .map((str) => Number(str))
        .map((num) =>
          hideGutter.length * PADDING[paddingSize] === num
            ? PADDING[paddingSize]
            : 0,
        )
        .join('px ') + 'px'
    );
  }
};

const getPaddingFromGutter = (
  hideGutter: Gutter,
  paddingSize: Padding,
) => {
  /* top right bottom left */
  const padding = PADDING[paddingSize];
  if (hideGutter === true) {
    return '0px 0px 0px 0px';
  } else if (hideGutter === 'horizontal') {
    return `${padding}px 0 ${padding}px 0`;
  } else if (hideGutter === 'vertical') {
    return `0 ${padding}px 0 ${padding}px`;
  } else if (hideGutter === 'left') {
    return `${padding}px ${padding}px ${padding}px 0`;
  } else if (hideGutter === 'right') {
    return `${padding}px 0 ${padding}px ${padding}px`;
  } else if (hideGutter === 'top') {
    return `0 ${padding}px ${padding}px ${padding}px`;
  } else if (hideGutter === 'bottom') {
    return `${padding}px ${padding}px 0 ${padding}px`;
  } else {
    return `${padding}px ${padding}px ${padding}px ${padding}px`;
  }
};

export const getHoursFromMins = (
  timeInMins: number,
): [number, number] => {
  const minsInAnHour = 60;
  const minutes = timeInMins % minsInAnHour;
  const hours = Math.floor(timeInMins / minsInAnHour);
  return [hours, minutes];
};
