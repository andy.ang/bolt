import { EventRegister } from 'react-native-event-listeners';
import { IError } from '../typings/error.types';
import {
  CallbackType,
  EmitDataType,
  Events,
} from '../typings/event.types';

export const EventManager = () => {};
EventManager.listen = <T extends Events>(
  event: T,
  callback: CallbackType<T>,
): string => {
  const listener = EventRegister.addEventListener(
    event,
    callback,
  );
  console.log(
    `[EventManager]: Registered listener with id: ${listener}.`,
  );
  return listener as string;
};

EventManager.remove = (listener: string) => {
  console.log(
    `[EventManager]: Removing listener ${listener}`,
  );
  EventRegister.removeEventListener(listener);
};

EventManager.emit = <T>(
  event: T,
  data?: EmitDataType<T>,
) => {
  console.log(`[EventManager]: Emitting event: ${event}`);
  EventRegister.emit((event as unknown) as string, data);
};

EventManager.emitError = (error: IError) => {
  console.log(
    `[EventManager]: Emitting error ${error?.title || ''}`,
  );
  EventRegister.emit(Events.ERROR, error);
};
