import axios, { AxiosRequestConfig } from 'axios';
import functions from '@react-native-firebase/functions';
import {
  FunctionData,
  FunctionResponse,
  Functions,
} from '../typings/functions.types';

// if(__DEV__) {
//   console.log("DEBUG MODE");
//   functions().useFunctionsEmulator("http://localhost:5001");
// }
interface IResponse {
  data: Record<string, string>;
  status: number;
  headers: Record<string, string>;
}
interface IError {
  response?: IResponse;
  request?: Object;
  message: string;
  config: Record<string, string>;
}

export const request = async <T>(
  config: AxiosRequestConfig,
): Promise<T> => {
  try {
    const res = await axios.request<T>(config);
    return res.data;
  } catch (error) {
    const {
      request: req,
      response,
      message,
    } = error as IError;
    if (response) {
      // The request was made and the server responded with a status code
      // that falls out of the range of 2xx
      console.log(response.data);
      console.log(response.status);
      console.log(response.headers);
    } else if (req) {
      // The request was made but no response was received
      // `error.request` is an instance of XMLHttpRequest in the browser and an instance of
      // http.ClientRequest in node.js
      console.log(req);
    } else {
      // Something happened in setting up the request that triggered an Error
      console.log('Error', message);
    }
    console.log(error.config);
    throw error;
  }
};

export const callFunctions = async <T extends Functions>(
  name: T,
  data: FunctionData<T>,
): Promise<FunctionResponse<T>> => {
  const response = await functions().httpsCallable(name)(
    data,
  );
  return response.data ? response.data : response;
};
