export const hasTokenExpired = (
  createdAt: number,
  expiresIn: number,
  bufferInHours: number = 24,
): boolean => {
  const createdTime = createdAt * 1000;
  const expiryTime = createdTime + expiresIn * 1000;
  const currentTimestamp = new Date().getTime();
  const oneDay = bufferInHours * 60 * 60 * 1000;

  return currentTimestamp + oneDay > expiryTime;
};
