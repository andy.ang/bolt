import AsyncStorage from '@react-native-async-storage/async-storage';
import RNSecureKeyStore, {
  ACCESSIBLE,
} from 'react-native-secure-key-store';
import {
  StorageKey,
  ObjectType,
  SecureStorageKey,
  SecureObjectType,
} from '../typings/storage.types';

export const storeSecureData = async <
  T extends SecureStorageKey
>(
  key: T,
  value: SecureObjectType<T>,
) => {
  const config = {
    accessible: ACCESSIBLE.ALWAYS,
  };
  try {
    const data = value ? JSON.stringify(value) : '';
    await RNSecureKeyStore.set(key, data, config);
  } catch (error) {
    console.error(error);
    throw error;
  }
};

export const getSecureData = async <
  T extends SecureStorageKey
>(
  key: T,
): Promise<SecureObjectType<T> | undefined> => {
  try {
    const data = await RNSecureKeyStore.get(key);
    if (data && data !== null) {
      // value previously stored
      return JSON.parse(data);
    }
    return undefined;
  } catch (error) {
    if (error.message.includes('key does not present')) {
      return undefined;
    }
    console.error(error);
    throw error;
  }
};

export const deleteSecureData = async <
  T extends SecureStorageKey
>(
  key: T,
): Promise<void> => {
  const config = {
    accessible: ACCESSIBLE.ALWAYS,
  };
  try {
    await RNSecureKeyStore.set(key, '', config);
  } catch (error) {
    console.error(error);
    throw error;
  }
};

export const storeData = async <T extends StorageKey>(
  key: T,
  value: ObjectType<T>,
) => {
  try {
    const data = value ? JSON.stringify(value) : '';
    await AsyncStorage.setItem(key, data);
  } catch (e) {
    // saving error
    throw e;
  }
};

export const deleteData = async <T extends StorageKey>(
  key: T,
) => {
  try {
    await AsyncStorage.removeItem(key);
  } catch (e) {
    // saving error
    throw e;
  }
};

export const getData = async <T extends StorageKey>(
  key: T,
): Promise<ObjectType<T> | undefined> => {
  try {
    const value = await AsyncStorage.getItem(key);
    if (value && value !== null) {
      // value previously stored
      return JSON.parse(value);
    }
    return undefined;
  } catch (e) {
    // error reading value
    throw e;
  }
};

export const getMultipleData = async <T extends StorageKey>(
  keys: T[],
) => {
  try {
    const values = await AsyncStorage.multiGet(keys);
    if (values && values.length > 0) {
      // value previously stored
      return values.map(([, value]) =>
        value ? JSON.parse(value) : undefined,
      );
    }
  } catch (e) {
    // error reading value
    throw e;
  }
};
