export interface IUser {
  id: string;
  phoneNumber: string;
  accessToken: string;
  paid: boolean;
}

export enum Collection {
  USERS = 'users',
  VEHICLES = 'vehicles',
  DATA = 'data',
}
