export interface IVehicleConfig {
  can_accept_navigation_requests: boolean;
  can_actuate_trunks: boolean;
  car_special_type: 'base';
  car_type: 'model3' | 'modelS' | 'modelY' | 'modelX';
  charge_port_type: 'CCS';
  default_charge_to_max: boolean;
  ece_restrictions: boolean;
  eu_vehicle: boolean;
  exterior_color: 'SolidBlack';
  exterior_trim: 'Chrome';
  has_air_suspension: boolean;
  has_ludicrous_mode: boolean;
  key_version: number;
  motorized_charge_port: boolean;
  plg: boolean;
  rear_seat_heaters: number;
  rear_seat_type: string | null;
  rhd: boolean;
  roof_color: 'Glass';
  seat_type: null;
  spoiler_type: 'None';
  sun_roof_installed: null | number;
  third_row_seats: '<invalid>';
  timestamp: number;
  use_range_badging: boolean;
  wheel_type: 'Pinwheel18';
}

export interface IDriveState {
  gps_as_of: number;
  heading: number;
  latitude: number;
  longitude: number;
  native_latitude: number;
  native_location_supported: number;
  native_longitude: number;
  native_type: 'wgs';
  power: number;
  shift_state: null;
  speed: number | null;
  timestamp: number;
}

export interface IGuiSettings {
  gui_24_hour_time: boolean;
  gui_charge_rate_units: 'kW' | 'mi/hr';
  gui_distance_units: 'km/hr' | 'mi/hr';
  gui_range_display: 'Rated';
  gui_temperature_units: 'C' | 'F';
  show_range_units: boolean;
  timestamp: number;
}

export interface IChargeState {
  battery_heater_on: boolean;
  battery_level: number;
  battery_range: number;
  charge_current_request: number;
  charge_current_request_max: number;
  charge_enable_request: boolean;
  charge_energy_added: number;
  charge_limit_soc: number;
  charge_limit_soc_max: number;
  charge_limit_soc_min: number;
  charge_limit_soc_std: number;
  charge_miles_added_ideal: number;
  charge_miles_added_rated: number;
  charge_port_cold_weather_mode: boolean;
  charge_port_door_open: boolean;
  charge_port_latch: 'Engaged';
  charge_rate: number;
  charge_to_max_range: boolean;
  charger_actual_current: number;
  charger_phases: number;
  charger_pilot_current: number;
  charger_power: number;
  charger_voltage: number;
  charging_state: 'Complete' | 'Charging';
  conn_charge_cable: 'IEC';
  est_battery_range: number;
  fast_charger_brand: '<invalid>';
  fast_charger_present: boolean;
  fast_charger_type: 'MCSingleWireCAN';
  ideal_battery_range: number;
  managed_charging_active: boolean;
  managed_charging_start_time: number | null;
  managed_charging_user_canceled: boolean;
  max_range_charge_counter: number;
  minutes_to_full_charge: number;
  not_enough_power_to_heat: boolean | null;
  scheduled_charging_pending: boolean;
  scheduled_charging_start_time: number | null;
  time_to_full_charge: number;
  timestamp: number;
  trip_charging: boolean;
  usable_battery_level: number;
  user_charge_enable_request: number | null;
}

export interface IVehicleState {
  api_version: number;
  autopark_state_v2: 'unavailable' | 'available';
  calendar_supported: boolean;
  car_version: string;
  center_display_state: 0 | 2 | 3 | 7 | 8;
  df: 0 | 1;
  dr: 0 | 1;
  fd_window: 0 | 1;
  fp_window: 0 | 1;
  ft: 0 | 1;
  is_user_present: boolean;
  locked: boolean;
  media_state: {
    remote_control_enabled: boolean;
  };
  notifications_supported: boolean;
  odometer: number;
  parsed_calendar_supported: boolean;
  pf: 0 | 1;
  pr: 0 | 1;
  rd_window: 0 | 1;
  remote_start: boolean;
  remote_start_enabled: boolean;
  remote_start_supported: boolean;
  rp_window: 0 | 1;
  rt: 0 | 1;
  sentry_mode: boolean;
  sentry_mode_available: boolean;
  software_update: {
    download_perc: number;
    expected_duration_sec: number;
    install_perc: number;
    status: string;
    version: string;
  };
  speed_limit_mode: {
    active: boolean;
    current_limit_mph: number;
    max_limit_mph: number;
    min_limit_mph: number;
    pin_code_set: boolean;
  };
  sun_roof_state: 'unknown' | 'closed' | 'open';
  homelink_device_count?: number;
  homelink_nearby?: boolean;
  timestamp: number;
  valet_mode: boolean;
  vehicle_name: string;
}

export interface IClimateState {
  battery_heater: boolean;
  battery_heater_no_power: null;
  climate_keeper_mode: 'off' | 'on';
  defrost_mode: 0 | 1 | 2;
  driver_temp_setting: number;
  fan_status: 0 | 1 | 2 | 3 | 4 | 5 | 6 | 7;
  inside_temp: number;
  is_auto_conditioning_on: boolean;
  is_climate_on: boolean;
  is_front_defroster_on: boolean;
  is_preconditioning: boolean;
  is_rear_defroster_on: boolean;
  left_temp_direction: number;
  max_avail_temp: number;
  min_avail_temp: number;
  outside_temp: number;
  passenger_temp_setting: number;
  remote_heater_control_enabled: boolean;
  right_temp_direction: number;
  seat_heater_left: 0 | 1 | 2 | 3;
  seat_heater_right: 0 | 1 | 2 | 3;
  seat_heater_rear_left: 0 | 1 | 2 | 3;
  seat_heater_rear_center: 0 | 1 | 2 | 3;
  seat_heater_rear_right: 0 | 1 | 2 | 3;
  side_mirror_heaters: boolean;
  timestamp: number;
  wiper_blade_heater: boolean;
}
export interface IVehicleData {
  // This is the real vehicle id
  id: number;
  vehicle_id: number;
  vin: string;
  display_name: string;
  option_codes: string;
  color: string | null;
  access_type: 'OWNER';
  tokens: string[];
  state: 'online';
  in_service: boolean;
  id_s: string;
  calendar_enabled: boolean;
  api_version: number;
  backseat_token: string | null;
  backseat_token_updated_at: string | null;
  vehicle_config?: IVehicleConfig;
  drive_state?: IDriveState;
  climate_state?: IClimateState;
  charge_state?: IChargeState;
  vehicle_state?: IVehicleState;
  gui_settings?: IGuiSettings;
}

export interface IVehicleDataResponse {
  response: IVehicleData[];
}

export interface IUserVehicleData {
  battery_level?: number;
  battery_range?: number;
  est_battery_range?: number;
  charge_rate?: number;
  odometer?: number;
  latitude?: number;
  longitude?: number;
  outside_temp?: number;
  inside_temp?: number;
  car_version?: string;
  timestamp?: number;
  createdAt?: Date;
  id: string;
}

export interface IScheduledTask {
  type: 'charging';
  name: string;
  timeInSeconds: number;
  frequency?: 'daily' | 'once';
}

export interface IVehicleSummary {
  id: string;
  userId: string[];
  car_type?: IVehicleConfig['car_type'];
  exterior_color?: IVehicleConfig['exterior_color'];
  exterior_trim?: IVehicleConfig['exterior_trim'];
  seat_type?: IVehicleConfig['seat_type'];
  wheel_type?: IVehicleConfig['wheel_type'];
  car_special_type?: IVehicleConfig['car_special_type'];
  scheduled: IScheduledTask[] | null;
}

export enum OptionCodeColor {
  PBSB = 'black',
  PBCW = 'white',
  PPSB = 'blue',
  PPMR = 'red',
  PPSR = 'red',
  PPSW = 'white',
  PPTI = 'grey',
  PMTG = 'grey',
  PMNG = 'grey',
  PMMB = 'blue',
  PMBL = 'black',
  PMSS = 'grey',
  PMAB = 'black',
  PMSG = 'black',
}

export enum OptionCodeModel {
  MDLS = 'tms',
  MS03 = 'tms',
  MS04 = 'tms',
  MDLX = 'tmx',
  MDL3 = 'tm3',
  MDLY = 'tmy',
}

export enum OptionCodeVariant {
  CPW1 = 'perf_tires',
  MT304 = 'perf_tires',
  MT311 = 'perf_tires',
  MT317 = 'perf_tires',
  WY20P = 'perf_tires',
  W32P = 'perf_tires',
  W32D = 'perf_tires',
  PX01 = 'perf_tires',
  MTS08 = 'perf',
  MTX06 = 'perf',
  MTX08 = 'perf',
  MTY04 = 'perf',
}
