export type Gutter =
  | boolean
  | 'vertical'
  | 'horizontal'
  | 'top'
  | 'bottom'
  | 'left'
  | 'right';

export type Space =
  | 'none'
  | 'xxsmall'
  | 'xsmall'
  | 'small'
  | 'medium'
  | 'big'
  | 'large';

export type Padding = 'lg' | 'md' | 'sm';
export const PADDING: Record<Padding, number> = {
  sm: 10,
  md: 20,
  lg: 30,
};

export const SPACES: Record<Space, number> = {
  none: 0,
  xxsmall: 2,
  xsmall: 4,
  small: 8,
  medium: 12,
  big: 16,
  large: 20,
};
