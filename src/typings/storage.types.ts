import { LatLng } from 'react-native-maps';
import {
  ColorMode,
  IRemoteConfig,
} from '../context/app/app-context.types';
import { IControlPanelItem } from './vehicle-command.types';
import { ISignInResponse } from './functions.types';
import { Gradient } from './theme.types';
import { IVehicleData } from './vehicle-data.types';

export enum StorageKeys {
  VALET_PIN = 'VALET_PIN',
  SPEED_LIMIT_PIN = 'SPEED_LIMIT_PIN',
  REMOTE_CONFIG = 'REMOTE_CONFIG',
  MODE = 'MODE',
  GRADIENT = 'GRADIENT',
  LOCATION = 'LOCATION',
  ADDRESS = 'ADDRESS',
  COUNTRY_CODE = 'COUNTRY_CODE',
  VEHICLES = 'VEHICLES',
  CURRENT_VEHICLE = 'CURRENT_VEHICLE',
  CONTROL_PANEL_LAYOUT = 'CONTROL_PANEL_LAYOUT',
}

export enum SecureStorageKeys {
  ACCESS_TOKEN = 'ACCESS_TOKEN',
  PASSWORD = 'PASSWORD',
}
export interface IStoredAddress {
  latLng: LatLng;
  address: string;
}

export interface IStoredCountryCode {
  ip: string;
  countryCode: string;
}

export interface IValetPin {
  pin: string;
}

export interface ISpeedLimitPin {
  pin: string;
}

export type IPassWord = string;

export type StorageKey = keyof typeof StorageKeys;
export type SecureStorageKey = keyof typeof SecureStorageKeys;

export type ObjectType<T> = T extends StorageKeys.LOCATION
  ? LatLng | undefined
  : T extends StorageKeys.MODE
  ? ColorMode
  : T extends StorageKeys.GRADIENT
  ? Gradient | undefined
  : T extends StorageKeys.REMOTE_CONFIG
  ? IRemoteConfig
  : T extends StorageKeys.ADDRESS
  ? IStoredAddress[]
  : T extends StorageKeys.COUNTRY_CODE
  ? IStoredCountryCode[]
  : T extends StorageKeys.VEHICLES
  ? IVehicleData[]
  : T extends StorageKeys.CURRENT_VEHICLE
  ? IVehicleData
  : T extends StorageKeys.CONTROL_PANEL_LAYOUT
  ? IControlPanelItem[]
  : T extends StorageKeys.VALET_PIN
  ? IValetPin
  : T extends StorageKeys.SPEED_LIMIT_PIN
  ? ISpeedLimitPin
  : never;

export type SecureObjectType<
  T
> = T extends SecureStorageKeys.ACCESS_TOKEN
  ? ISignInResponse
  : T extends SecureStorageKeys.PASSWORD
  ? IPassWord
  : never;
