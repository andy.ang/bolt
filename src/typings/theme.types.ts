export interface ITheme {
  basic: string;
  primaryBackground: string;
  secondaryBackground: string;
  textInputBackground: string;
  buttonBackground: string;
  modalBackground: string;
  chartBackground: string;
  sliderBackground: string;
  sliderTintColor: string;
  chartTitleColor: string;
  modalHandleColor: string;
  headerColor: string;
  listColor: string;
  borderColor: string;
  chargingRingColor: string;
  chargingRingGradientColor: string;
  primaryTextColor: string;
  secondaryTextColor: string;
  placeholderTextColor: string;
  climateStatusColor: string;
  chargingStatusColor: string;
  cursorColor: string;
  markerColor: string;
  routeColor: string;
  errorColor: string;
  activeTabPickerColor: string;
  tabButtonActiveColor: string;
  tabButtonInactiveColor: string;
  controlItemInactiveButtonColor: string;
  controlItemActiveButtonColor: string;
  seatHeaterIndicatorActiveColor: string;
  seatHeaterIndicatorInactiveColor: string;
  graphBackground: string;
  graphAreaColor: string;
  gradient1: [string, string];
  gradient2: [string, string];
  gradient3: [string, string];
}

export enum Gradient {
  DEFAULT = 'gradient1',
  GRADIENT_1 = 'gradient1',
  GRADIENT_2 = 'gradient2',
  GRADIENT_3 = 'gradient3',
}

export enum Colors {
  BEIGE = '#EBEBD3',
  ISABELLINE = '#F5F1ED',
  HUNTER_GREEN = '#2E5339',
  SPACE_CADET = '#25283D',
  BONE = '#ECE2D0',
  COPPER_ROSE = '#A26769',
  BLACK_SHADOWS = '#CEBEBE',
  OLD_MAUVE = '#6D2E46',
  CHARCOAL = '#40434E',
  LEMON_MERINGUE = '#F1E8B8',
  WHITE = '#FFFFFF',
  BLACK = '#000000',
  BABY_POWDER = '#F9FBF2',
  LAVENDER_GRAY = '#B8B3BE',
  LIGHT_BLUE = '#A9BCD0',
  PLATINUM = '#E8E9ED',
  MIDNIGHT_GREEN = '#084C61',
  OLIVE_DRAB = '#423E28',
  RICH_BLACK = '#1A3A3A',
  PURPLE_NAVY = '#4F517D',
  LANGUID_LAVENDER = '#DCCFEC',
  BLACK_CHOCO = '#261C15',
  EERIE_BLACK = '#171614',
  BLACK_OLIVE = '#37423D',
  DARK_PURPLE = '#260F26',
  OLD_BURGUNDY = '#3F292B',
  FOREST_GREEN_CRAYOLA = '#59A96A',
  LAVENDER_BUSH = '#F8E5EE',
  MINT_GREEN = '#9CFC97',
  CULTURED = '#F7F9F9',
  LINEN = '#F2E5D7',
  JET_BLACK = '#2F2D2E',
  JUNE_BUD = '#C7D66D',
  PALE_SPRING_BUD = '#DCEAB2',
  CAMBRIDGE_BLUE = '#B3D6C6',
  DARK_BLUE = '#0A1128',
  VERDIGRIS = '#75B9BE',
  DARK_SKY_BLUE = '#7CAFC4',
  CAMEO_PINK = '#DBABBE',
  QUEEN_PINK = '#EDD2E0',
  SPANISH_PINK = '#EDBBB4',
  SILVER_PINK = '#BAA1A7',
  GRAY_WEB = '#797B84',
  MELON = '#F7AF9D',
  WARM_BLACK = '#004643',
  RUSSET = '#7D451B',
  MAC_AND_CHEESE = '#FCB07E',
  CARDINAL = '#C5283D',
  DAVYS_GRAY = '#595959',
  LIGHT_GRAY = '#CED3DC',
  DARK_SIENNA = '#2C0703',
  SNOW = '#FCF7F8',
  BLACK_CORAL = '#5B616A',
  WILD_BLUE_WONDER = '#ABA9C3',
  ARTICHOKE = '#A2A392',
  SPANISH_GRAY = '#9A998C',
  TIMBERWOLF = '#D5D0CD',
  TIMBERWOLF_DARK = '#D4CDC3',
  EGGSHELL = '#F8F4E3',
  GRAYISH = '#F5F5F5',
  DEBUG = '#F5F5F5',
}

export enum Fonts {
  OXANIUM = 'Oxanium-Regular',
  OXANIUM_EXTRALIGHT = 'Oxanium-ExtraLight',
  OXANIUM_LIGHT = 'Oxanium-Light',
  OXANIUM_MEDIUM = 'Oxanium-Medium',
  OXANIUM_BOLD = 'Oxanium-Bold',
  OXANIUM_SEMIBOLD = 'Oxanium-SemiBold',
  OXANIUM_EXTRABOLD = 'Oxanium-ExtraBold',
}
