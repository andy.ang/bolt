interface IUserMetadata {
  creationTime?: string;
  lastSignInTime?: string;
}

export interface IAuthUser {
  uid: string;
  phoneNumber: string | null;
  metadata: IUserMetadata;
}

export type AuthenticationStatus =
  | 'initializing'
  | 'authenticated'
  | 'needsSignIn'
  | 'unauthenticated';

export interface UserCredential {
  user: IAuthUser;
}

export interface ConfirmationResult {
  verificationId: string | null;
  confirm(
    verificationCode: string,
  ): Promise<UserCredential | null>;
}
