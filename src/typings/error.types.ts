export enum Error {
  INVALID_CODE = 'Invalid code entered. Please try again.',
  INVALID_PHONE_NUMBER = 'Please check your phone number and try again.',
  GENERIC = 'Aww snap. Things did not go as planned. Please try again later. Otherwise, please contact support at bolt@minkco.com.au',
  TOO_MANY_ATTEMPTS = "Woah! You're too excited. Please wait a few minutes for our system to catch up.",
  SERVER_ERROR = "We're so sorry! Looks like our server is experiencing issues. We're working hard to fix it right now.",
  INVALID_CREDS = 'Your sign in details are incorrect. Please try again.',
  MISSING_VEHICLE = "We can't find your tesla vehicle. Please check your login details and try again later.",
  NO_ROUTE = "You can't travel that far! Try somewhere closer.",
  USER_ERROR = 'Failed to create user. Please contact us to resolve the issue.',
  COMMAND_ERROR = 'We have failed to execute command. Please try again.',
}

export enum FirebaseAuthError {
  TOO_MANY_REQUESTS = 'auth/too-many-requests',
  OPERATION_NOT_ALLOWED = 'auth/operation-not-allowed',
  INVALID_CODE = 'auth/invalid-verification-code',
  INVALID_PHONE_NUMBER = 'auth/invalid-phone-number',
  MISSING_CODE = 'auth/missing-verification-code',
  INTERNAL_ERROR = 'auth/internal-error',
}

export enum ErrorTitle {
  GENERIC = 'Oops! Something went wrong',
}

export interface IError {
  title?: string;
  body: string;
  footer?: string;
  showButton?: boolean;
  buttonLabel?: string;
  buttonOnPress?: () => void;
}
