import { Icons } from '../components';
import { IScheduledTask } from './vehicle-data.types';

export enum Controls {
  UNLOCK = 'UNLOCK',
  LOCK = 'LOCK',
  OPEN_FRUNK = 'OPEN_FRUNK',
  OPEN_TRUNK = 'OPEN_TRUNK',
  WAKE = 'WAKE',
  TRIGGER_HOME_LINK = 'TRIGGER_HOME_LINK',
  START_AC = 'START_AC',
  STOP_AC = 'STOP_AC',
  FLASH_LIGHTS = 'FLASH_LIGHTS',
  HONK_HORN = 'HONK_HORN',
  SET_TEMPS = 'SET_TEMPS',
  SET_CHARGE_LIMIT = 'SET_CHARGE_LIMIT',
  REMOTE_SEAT_HEATER_REQUEST = 'REMOTE_SEAT_HEATER_REQUEST',
  TOGGLE_MAX_DEFROST = 'TOGGLE_MAX_DEFROST',
  TOGGLE_WINDOW = 'TOGGLE_WINDOW',
  PLAY_NEXT_TRACK = 'PLAY_NEXT_TRACK',
  PLAY_PREVIOUS_TRACK = 'PLAY_PREVIOUS_TRACK',
  TOGGLE_MEDIA_PLAYBACK = 'TOGGLE_MEDIA_PLAYBACK',
  TURN_VOLUME_UP = 'TURN_VOLUME_UP',
  TURN_VOLUME_DOWN = 'TURN_VOLUME_DOWN',
  SET_SENTRY_MODE = 'SET_SENTRY_MODE',
  SET_VALET_MODE = 'SET_VALET_MODE',
  RESET_VALET_PIN = 'RESET_VALET_PIN',
  SET_SPEED_LIMIT_MODE = 'SET_SPEED_LIMIT_MODE',
  SET_SPEED_LIMIT = 'SET_SPEED_LIMIT',
  RESET_SPEED_LIMIT_PIN = 'RESET_SPEED_LIMIT_PIN',
  START_REMOTE_DRIVE = 'START_REMOTE_DRIVE',
  TOGGLE_SUNROOF = 'TOGGLE_SUNROOF',
  SCHEDULE_CHARGING = 'SCHEDULE_CHARGING',
  START_CHARGING = 'START_CHARGING',
  STOP_CHARGING = 'STOP_CHARGING',
  STANDARD_CHARGING = 'STANDARD_CHARGING',
  MAX_CHARGING = 'MAX_CHARGING',
  DELETE_SCHEDULED_CHARGING = 'DELETE_SCHEDULED_CHARGING',
}

export type Control = keyof typeof Controls;
export type DoorCommand = Controls.LOCK | Controls.UNLOCK;
export type AcCommand =
  | Controls.START_AC
  | Controls.STOP_AC;
export type FrunkTrunkCommand =
  | Controls.OPEN_FRUNK
  | Controls.OPEN_TRUNK;

export type ControlType = {
  label: string;
  state: string | boolean;
  command: Controls;
  icon: Icons;
};
export interface IControlPanelItem {
  index: number;
  active: ControlType;
  inactive: ControlType;
  accessor: string;
  disabled: string;
  disabledIf: boolean;
}

export interface ICommandData {
  latitude?: number;
  longitude?: number;
  driverTemperature?: number;
  passengerTemperature?: number;
  chargeLimit?: number;
  heatLevel?: number;
  seatId?: number;
  maxDefrost?: boolean;
  window?: 'vent' | 'close';
  sunroof?: 'vent' | 'close';
  sentryMode?: boolean;
  valetMode?: boolean;
  valetPin?: string;
  speedMph?: number;
  speedLimitMode?: boolean;
  speedLimitPin?: string;
  chargingTime?: number;
  chargingTask?: IScheduledTask;
  chargingFrequency?: IScheduledTask['frequency'];
}
