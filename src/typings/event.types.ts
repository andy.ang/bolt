import { IError } from './error.types';
import { Controls } from './vehicle-command.types';

export enum Events {
  ACCESS_TOKEN_UPDATED = 'accessTokenUpdated',
  REFRESH_VEHICLES = 'refreshVehicles',
  ERROR = 'error',
}

export type CallbackType<
  T
> = T extends Events.ACCESS_TOKEN_UPDATED
  ? () => void
  : T extends Events.ERROR
  ? (event?: IError) => void
  : T extends Events.REFRESH_VEHICLES
  ? (data?: IRefreshVehicles) => void
  : never;

export interface IRefreshVehicles {
  command: Controls;
}

export type EmitDataType<
  T
> = T extends Events.ACCESS_TOKEN_UPDATED
  ? undefined
  : T extends Events.ERROR
  ? IError | undefined
  : T extends Events.REFRESH_VEHICLES
  ? IRefreshVehicles
  : never;
