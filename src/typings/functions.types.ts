import {
  AcCommand,
  DoorCommand,
  FrunkTrunkCommand,
} from './vehicle-command.types';
import {
  IScheduledTask,
  IVehicleData,
} from './vehicle-data.types';

export interface ISignInResponse {
  access_token: string;
  token_type: 'bearer';
  expires_in: number;
  refresh_token: string;
  created_at: number;
}

export interface ISignInErrorResponse {
  error: string;
  error_description: string;
}

export enum Functions {
  AUTHENTICATE = 'authenticate',
  DELETE_SCHEDULED_CHARGING = 'deleteScheduledCharging',
  REFRESH_TOKEN = 'refreshToken',
  GET_VEHICLES = 'getVehicles',
  GET_VEHICLE_DATA = 'getVehicleData',
  LOCK_UNLOCK_VEHICLE = 'lockUnlockVehicle',
  OPEN_TRUNK_FRUNK = 'openTrunkFrunk',
  WAKE_VEHICLE = 'wakeVehicle',
  TRIGGER_HOME_LINK = 'triggerHomeLink',
  START_STOP_AIR_COND = 'startStopAirCond',
  FLASH_LIGHTS = 'flashLights',
  HONK_HORN = 'honkHorn',
  SET_TEMPS = 'setClimateTemperature',
  SET_CHARGE_LIMIT = 'setChargeLimit',
  REMOTE_SEAT_HEATER_REQUEST = 'setSeatHeaterTemperature',
  TOGGLE_MAX_DEFROST = 'toggleMaxDefrost',
  TOGGLE_WINDOW = 'toggleWindow',
  PLAY_NEXT_TRACK = 'playNextTrack',
  PLAY_PREVIOUS_TRACK = 'playPreviousTrack',
  TOGGLE_MEDIA_PLAYBACK = 'toggleMediaPlayback',
  TURN_VOLUME_UP = 'turnVolumeUp',
  TURN_VOLUME_DOWN = 'turnVolumeDown',
  SET_SENTRY_MODE = 'setSentryMode',
  SET_VALET_MODE = 'setValetMode',
  RESET_VALET_PIN = 'resetValetPin',
  SET_SPEED_LIMIT_MODE = 'setSpeedLimitMode',
  SET_SPEED_LIMIT = 'setSpeedLimit',
  RESET_SPEED_LIMIT_PIN = 'resetSpeedLimitPin',
  START_REMOTE_DRIVE = 'startRemoteDrive',
  TOGGLE_SUNROOF = 'toggleSunroof',
  SCHEDULE_CHARGING = 'scheduleChargingTime',
  START_CHARGING = 'startCharging',
  STOP_CHARGING = 'stopCharging',
  STANDARD_CHARGING = 'startStandardCharging',
  MAX_CHARGING = 'startMaxCharging',
}

interface IAuthenticateData {
  email: string;
  password: string;
}

interface IRefreshTokenData {
  token: string;
}

interface IBaseFunctionData {
  access_token: string;
}

interface IGetVehiclesData extends IBaseFunctionData {}

interface IGetVehicleData extends IBaseFunctionData {
  vehicleId: string;
}

interface IVehicleDoor extends IBaseFunctionData {
  vehicleId: string;
  command: DoorCommand;
}

interface IFrunkTrunk extends IBaseFunctionData {
  vehicleId: string;
  command: FrunkTrunkCommand;
}

interface IWakeVehicle extends IBaseFunctionData {
  vehicleId: string;
}

interface ITriggerHomeLink extends IBaseFunctionData {
  vehicleId: string;
  latitude: number;
  longitude: number;
}

export interface IStartStopAirCond
  extends IBaseFunctionData {
  vehicleId: string;
  command: AcCommand;
}

export interface IFlashLights extends IBaseFunctionData {
  vehicleId: string;
}

export interface IHonkHorn extends IBaseFunctionData {
  vehicleId: string;
}

export interface IResetValetPin extends IBaseFunctionData {
  vehicleId: string;
}

export interface ISetTemps extends IBaseFunctionData {
  vehicleId: string;
  driverTemperature: number;
  passengerTemperature: number;
}
export interface ISetChargeLimit extends IBaseFunctionData {
  vehicleId: string;
  limit: number;
}

export interface IRemoteSeatHeaterRequest
  extends IBaseFunctionData {
  vehicleId: string;
  heater: number;
  level: number;
}

export interface IToggleMaxDefrost
  extends IBaseFunctionData {
  vehicleId: string;
  on: boolean;
}

export interface IToggleWindow extends IBaseFunctionData {
  vehicleId: string;
  command: 'vent' | 'close';
  latitude: number;
  longitude: number;
}

export interface ISetSentryMode extends IBaseFunctionData {
  vehicleId: string;
  on: boolean;
}

export interface ISetValetMode extends IBaseFunctionData {
  vehicleId: string;
  on: boolean;
  pin: string;
}

export interface ISetSpeedLimitMode
  extends IBaseFunctionData {
  vehicleId: string;
  on: boolean;
  pin: string;
}

export interface ISetSpeedLimit extends IBaseFunctionData {
  vehicleId: string;
  limit_mph: number;
}
export interface IResetSpeedLimitPin
  extends IBaseFunctionData {
  vehicleId: string;
  pin: string;
}

export interface IMedia extends IBaseFunctionData {
  vehicleId: string;
}

export interface IStartRemoteDrive
  extends IBaseFunctionData {
  vehicleId: string;
  password: string;
}

export interface IToggleSunroof extends IBaseFunctionData {
  vehicleId: string;
  state: 'vent' | 'close';
}

export interface IScheduleChargingTime
  extends IBaseFunctionData {
  vehicleId: string;
  time: number;
  frequency: IScheduledTask['frequency'];
}

export interface IStartCharging extends IBaseFunctionData {
  vehicleId: string;
}
export interface IStopCharging extends IBaseFunctionData {
  vehicleId: string;
}
export interface IStartStandardCharging
  extends IBaseFunctionData {
  vehicleId: string;
}
export interface IStartMaxCharging
  extends IBaseFunctionData {
  vehicleId: string;
}

export interface IDeleteScheduledCharging
  extends IBaseFunctionData {
  vehicleId: string;
  task: IScheduledTask;
}

export type FunctionData<
  T
> = T extends Functions.AUTHENTICATE
  ? IAuthenticateData
  : T extends Functions.REFRESH_TOKEN
  ? IRefreshTokenData
  : T extends Functions.GET_VEHICLES
  ? IGetVehiclesData
  : T extends Functions.GET_VEHICLE_DATA
  ? IGetVehicleData
  : T extends Functions.LOCK_UNLOCK_VEHICLE
  ? IVehicleDoor
  : T extends Functions.OPEN_TRUNK_FRUNK
  ? IFrunkTrunk
  : T extends Functions.WAKE_VEHICLE
  ? IWakeVehicle
  : T extends Functions.TRIGGER_HOME_LINK
  ? ITriggerHomeLink
  : T extends Functions.START_STOP_AIR_COND
  ? IStartStopAirCond
  : T extends Functions.FLASH_LIGHTS
  ? IFlashLights
  : T extends Functions.HONK_HORN
  ? IHonkHorn
  : T extends Functions.SET_TEMPS
  ? ISetTemps
  : T extends Functions.SET_CHARGE_LIMIT
  ? ISetChargeLimit
  : T extends Functions.REMOTE_SEAT_HEATER_REQUEST
  ? IRemoteSeatHeaterRequest
  : T extends Functions.TOGGLE_MAX_DEFROST
  ? IToggleMaxDefrost
  : T extends Functions.TOGGLE_WINDOW
  ? IToggleWindow
  : T extends Functions.TOGGLE_MEDIA_PLAYBACK
  ? IMedia
  : T extends Functions.PLAY_NEXT_TRACK
  ? IMedia
  : T extends Functions.PLAY_PREVIOUS_TRACK
  ? IMedia
  : T extends Functions.TURN_VOLUME_UP
  ? IMedia
  : T extends Functions.TURN_VOLUME_DOWN
  ? IMedia
  : T extends Functions.SET_SENTRY_MODE
  ? ISetSentryMode
  : T extends Functions.SET_VALET_MODE
  ? ISetValetMode
  : T extends Functions.RESET_VALET_PIN
  ? IResetValetPin
  : T extends Functions.SET_SPEED_LIMIT_MODE
  ? ISetSpeedLimitMode
  : T extends Functions.RESET_SPEED_LIMIT_PIN
  ? IResetSpeedLimitPin
  : T extends Functions.SET_SPEED_LIMIT
  ? ISetSpeedLimit
  : T extends Functions.START_REMOTE_DRIVE
  ? IStartRemoteDrive
  : T extends Functions.TOGGLE_SUNROOF
  ? IToggleSunroof
  : T extends Functions.SCHEDULE_CHARGING
  ? IScheduleChargingTime
  : T extends Functions.START_CHARGING
  ? IStartCharging
  : T extends Functions.STOP_CHARGING
  ? IStopCharging
  : T extends Functions.STANDARD_CHARGING
  ? IStartStandardCharging
  : T extends Functions.MAX_CHARGING
  ? IStartMaxCharging
  : T extends Functions.DELETE_SCHEDULED_CHARGING
  ? IDeleteScheduledCharging
  : never;

export type FunctionResponse<
  T
> = T extends Functions.AUTHENTICATE
  ? ISignInResponse
  : T extends Functions.REFRESH_TOKEN
  ? ISignInResponse
  : T extends Functions.GET_VEHICLES
  ? IVehicleData[]
  : T extends Functions.GET_VEHICLE_DATA
  ? IVehicleData
  : T extends Functions.LOCK_UNLOCK_VEHICLE
  ? void
  : T extends Functions.OPEN_TRUNK_FRUNK
  ? void
  : T extends Functions.WAKE_VEHICLE
  ? void
  : T extends Functions.TRIGGER_HOME_LINK
  ? void
  : T extends Functions.START_STOP_AIR_COND
  ? void
  : T extends Functions.FLASH_LIGHTS
  ? void
  : T extends Functions.HONK_HORN
  ? void
  : T extends Functions.SET_TEMPS
  ? void
  : T extends Functions.SET_CHARGE_LIMIT
  ? void
  : T extends Functions.REMOTE_SEAT_HEATER_REQUEST
  ? void
  : T extends Functions.TOGGLE_MAX_DEFROST
  ? void
  : T extends Functions.TOGGLE_WINDOW
  ? void
  : T extends Functions.TOGGLE_MEDIA_PLAYBACK
  ? void
  : T extends Functions.PLAY_NEXT_TRACK
  ? void
  : T extends Functions.PLAY_PREVIOUS_TRACK
  ? void
  : T extends Functions.TURN_VOLUME_DOWN
  ? void
  : T extends Functions.TURN_VOLUME_UP
  ? void
  : T extends Functions.SET_SENTRY_MODE
  ? void
  : T extends Functions.SET_VALET_MODE
  ? void
  : T extends Functions.RESET_VALET_PIN
  ? void
  : T extends Functions.SET_SPEED_LIMIT_MODE
  ? void
  : T extends Functions.RESET_SPEED_LIMIT_PIN
  ? void
  : T extends Functions.SET_SPEED_LIMIT
  ? void
  : T extends Functions.START_REMOTE_DRIVE
  ? void
  : T extends Functions.TOGGLE_SUNROOF
  ? void
  : T extends Functions.SCHEDULE_CHARGING
  ? IScheduledTask
  : T extends Functions.START_CHARGING
  ? void
  : T extends Functions.STOP_CHARGING
  ? void
  : T extends Functions.STANDARD_CHARGING
  ? void
  : T extends Functions.MAX_CHARGING
  ? void
  : T extends Functions.DELETE_SCHEDULED_CHARGING
  ? void
  : never;
