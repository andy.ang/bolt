import { useCallback, useEffect, useState } from 'react';
import auth from '@react-native-firebase/auth';
import {
  AuthenticationStatus,
  IAuthUser,
} from '../typings/auth.types';
import { getSecureData } from '../lib/storage';
import { SecureStorageKeys } from '../typings/storage.types';
import { Events } from '../typings/event.types';
import { EventManager } from '../lib/event';

export const useAuthentiation = (): [
  AuthenticationStatus,
] => {
  const [initializing, setInitializing] = useState<boolean>(
    true,
  );
  const [hasAccessToken, setHasAccessToken] = useState<
    boolean | null
  >(null);
  const [user, setUser] = useState<IAuthUser | null>(null);

  const determineAuthenticationStatus = () => {
    if (initializing || hasAccessToken === null) {
      return 'initializing';
    } else if (!initializing && hasAccessToken && user) {
      return 'authenticated';
    } else if (!initializing && !hasAccessToken && user) {
      return 'needsSignIn';
    } else {
      return 'unauthenticated';
    }
  };

  const authenticated: AuthenticationStatus = determineAuthenticationStatus();
  const onAuthStateChanged = useCallback(
    (authUser: IAuthUser | null) => {
      setUser(authUser);
      if (initializing) {
        setInitializing(false);
      }
    },
    [initializing],
  );

  const onAccessTokenChanged = useCallback(() => {
    getSecureData(SecureStorageKeys.ACCESS_TOKEN)
      .then((data) => {
        if (data) {
          setHasAccessToken(true);
        } else {
          setHasAccessToken(false);
        }
      })
      .catch(() => {});
  }, []);

  useEffect(() => {
    const subscriber = auth().onAuthStateChanged(
      onAuthStateChanged,
    );
    return subscriber; // unsubscribe on unmount
  }, [onAuthStateChanged]);

  useEffect(() => {
    onAccessTokenChanged();
  }, [onAccessTokenChanged]);

  useEffect((): (() => void) => {
    const accessTokenListener = EventManager.listen(
      Events.ACCESS_TOKEN_UPDATED,
      onAccessTokenChanged,
    );
    return () => EventManager.remove(accessTokenListener);
  }, [onAccessTokenChanged]);

  return [authenticated];
};
