import { useCallback, useEffect, useState } from 'react';
import { useLoadingStateContext } from '../context/loading-state/loading-state-context';
import { EventManager } from '../lib/event';
import { getData } from '../lib/storage';
import { getVehicles } from '../services/vehicle/vehicle-data.service';
import { Events } from '../typings/event.types';
import { StorageKeys } from '../typings/storage.types';
import { IVehicleData } from '../typings/vehicle-data.types';

export const useVehicles = (): [
  IVehicleData[],
  IVehicleData | null,
] => {
  const [vehicles, setVehicles] = useState<IVehicleData[]>(
    [],
  );
  const {
    resetLoadingState,
    setStateToCompleteLoading,
  } = useLoadingStateContext();
  const [
    currentVehicle,
    setCurrentVehicle,
  ] = useState<IVehicleData | null>(null);

  const retrieveVehicles = useCallback(async () => {
    try {
      const selectedVehicle = await getData(
        StorageKeys.CURRENT_VEHICLE,
      );
      const data = await getVehicles();
      if (data && data.length > 0) {
        setVehicles(data);
        if (selectedVehicle) {
          const found = data.find(
            (d) => d.id_s === selectedVehicle.id_s,
          );
          setCurrentVehicle(found || data[0]);
        }
      }
      console.log('[Refresh vehicles]: Refresh complete.');
      return true;
    } catch (error) {
      console.error(`[Refresh vechiles: ${error}]`);
      throw error;
    }
  }, []);

  useEffect(() => {
    const listener = EventManager.listen(
      Events.REFRESH_VEHICLES,
      async (data) => {
        const { command } = data || {};
        console.log(
          `[Refresh vehicles]: Event received from ${
            command || 'SYSTEM'
          }. Refreshing vehicles...`,
        );
        try {
          await retrieveVehicles();
          if (command) {
            setStateToCompleteLoading(command);
          } else {
            resetLoadingState();
          }
        } catch (e) {
          console.error(e);
        }
      },
    );

    return () => EventManager.remove(listener);
  }, [
    retrieveVehicles,
    setStateToCompleteLoading,
    resetLoadingState,
  ]);

  return [vehicles, currentVehicle];
};
