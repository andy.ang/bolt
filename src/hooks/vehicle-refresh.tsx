import { useNavigation } from '@react-navigation/native';
import { useEffect, useState } from 'react';
import { AppStateStatus } from 'react-native';
import { EventManager } from '../lib/event';
import { Events } from '../typings/event.types';

const DEFAULT_REFRESHING_DURATION = 10000;

export const useVehicleRefresh = (
  appStatus: AppStateStatus,
): [
  boolean,
  React.Dispatch<React.SetStateAction<boolean>>,
] => {
  const navigation = useNavigation();
  const [tabFocused, setTabFocused] = useState<boolean>(
    true,
  );
  const [refreshing, setRefreshing] = useState<boolean>(
    false,
  );

  useEffect(() => {
    const focusListener = navigation.addListener(
      'focus',
      () => {
        setTabFocused(true);
      },
    );
    const blurListener = navigation.addListener(
      'blur',
      () => {
        setTabFocused(false);
      },
    );

    return () => {
      focusListener();
      blurListener();
    };
  }, [navigation]);

  useEffect(() => {
    if (appStatus === 'active') {
      EventManager.emit(Events.REFRESH_VEHICLES);
      setRefreshing(true);
    }
  }, [appStatus]);

  useEffect(() => {
    let listener: string;
    let timer: NodeJS.Timeout;

    if (appStatus === 'active' && tabFocused) {
      listener = EventManager.listen(
        Events.REFRESH_VEHICLES,
        () => {
          setRefreshing(true);
        },
      );

      timer = setInterval(() => {
        EventManager.emit(Events.REFRESH_VEHICLES);
      }, DEFAULT_REFRESHING_DURATION);
    }

    return () => {
      if (listener) {
        EventManager.remove(listener);
      }
      if (timer) {
        clearInterval(timer);
      }
    };
  }, [appStatus, tabFocused]);

  return [refreshing, setRefreshing];
};
