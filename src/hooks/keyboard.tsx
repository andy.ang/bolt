import { useEffect, useState } from 'react';
import { Keyboard } from 'react-native';

export const useKeybaord = () => {
  const [keyboardHeight, setKeyboardHeight] = useState<
    number
  >(0);

  const onKeyboardDidShow = (e: KeyboardEvent): void => {
    //@ts-ignore
    setKeyboardHeight(e.endCoordinates.height);
  };

  const onKeyboardDidHide = (): void => {
    setKeyboardHeight(0);
  };

  useEffect(() => {
    Keyboard.addListener(
      'keyboardDidShow',
      onKeyboardDidShow,
    );
    Keyboard.addListener(
      'keyboardDidHide',
      onKeyboardDidHide,
    );
    return (): void => {
      Keyboard.removeListener(
        'keyboardDidShow',
        onKeyboardDidShow,
      );
      Keyboard.removeListener(
        'keyboardDidHide',
        onKeyboardDidHide,
      );
    };
  }, []);

  return [keyboardHeight];
};
