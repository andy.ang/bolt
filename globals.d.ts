declare module '@react-native-async-storage/async-storage/jest/async-storage-mock';
declare module 'react-native-pulse';
declare module 'react-native-secure-storage';
declare module 'country-codes-list';
declare module 'react-native-json-tree';
declare module 'react-native-radial-gradient';
declare module 'react-native-pages' {
  export interface IPagesProps {
    children: React.ReactNode;
    ref: React.ForwardedRef<typeof Pages>;
    containerStyle: Record<string, any>;
    indicatorColor: string;
    scrollEnabled?: boolean;
    bounces?: boolean;
  }
  export function Pages(props: IPagesProps): JSX.Element;
}
